unit DebugThreadDABCommand;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Math,
  dcsHandler,
  FpDbgClasses,
  FpDbgInfo,
  FPDbgVariables,
  FpDbgDwarf,
  FpdMemoryTools,
  FpDbgDwarfDataClasses,
  FpDbgCallContextInfo,
  DbgIntfDebuggerBase,
  DbgIntfBaseTypes,
  FpErrorMessages,
  FpPascalBuilder,
  FPDbgController,
  DebugThread,
  {$ifdef UsePasSrc}
  DebugExpressionParser,
  cnocExpressionEvaluator,
  {$else}
  FpPascalParser,
  {$endif}
  DABMessages,
  DABFpdMessages;

type

  { TFpDebugThreadDABCommand }

  TFpDebugThreadDABCommand = class(TFpDebugThreadCommand)
  protected
    FMessage: TDABProtocolMessage;
    FResponseBody: TObject;
    function GetNotificationCommandEventClass: TDCSNotificationEventClass; override;
    function CreateExecutedCommandEvent(Success: Boolean; ReturnMessage: string; NotificationClass: TDCSNotificationEventClass): TDCSNotificationEvent; override;

    function GetCurrentProcess(AController: TDCSCustomController): TDbgProcess;
    function ComposeDABStackFrameId(AController: TDCSCustomController; AThreadId: Integer; ACallStackIndex: Integer): Integer;
    function RetrieveStackFrameFromDABId(AController: TDCSCustomController; ADABStackFrameID: Integer; out AThreadId, ACallStackIndex: Integer): Boolean;
    procedure FillVariablePresentationHint(AVariable: TDbgVariable; PresentationHint: TDABVariablePresentationHint);
    procedure VariablesToDABVariable(AController: TFpServerDbgController; Variables: TDbgVariableList; DABVariables: TDABVariableList);
  protected
    procedure SendErrorResponse(
      Id: integer;
      Message: shortstring;
      Format: string;
      ShowUser: Boolean);
  public
    constructor Create(ASendByLisId: integer; ADABMessage: TDABProtocolMessage; ADistributor: TDCSDistributor); virtual;
    destructor Destroy; override;
  end;

  { TFpDebugThreadDABInitializeCommand }

  TFpDebugThreadDABInitializeCommand = class(TFpDebugThreadDABCommand)
  public
    procedure PreExecute(AController: TDCSCustomController; out DoQueueCommand: boolean); override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABLaunchCommand }

  TFpDebugThreadDABLaunchCommand = class(TFpDebugThreadDABCommand)
  protected
    function GetLaunchRequest: TDABFpdLaunchRequest;
    procedure FillControllerSettings(ASettings: TFpServerDbgControllerSettings; ALaunchArguments: TDABFpdLaunchRequestArguments);
  public
    procedure PreExecute(AController: TDCSCustomController; out DoQueueCommand: boolean); override;
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABSetBreakpointsCommand }

  TFpDebugThreadDABSetBreakpointsCommand = class(TFpDebugThreadDABCommand)
  protected
    function GetSetBreakpointsRequest: TDABSetBreakpointsRequest;
  public
    procedure PreExecute(AController: TDCSCustomController; out DoQueueCommand: boolean); override;
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABSetExceptionBreakpointsCommand }

  TFpDebugThreadDABSetExceptionBreakpointsCommand = class(TFpDebugThreadDABCommand)
  public
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABConfigurationDoneCommand }

  TFpDebugThreadDABConfigurationDoneCommand = class(TFpDebugThreadDABCommand)
  public
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABThreadsCommand }

  TFpDebugThreadDABThreadsCommand = class(TFpDebugThreadDABCommand)
  public
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABStackTraceCommand }

  TFpDebugThreadDABStackTraceCommand = class(TFpDebugThreadDABCommand)
  protected
    function GetStackTraceRequest: TDABStackTraceRequest;
  public
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABScopesCommand }

  TFpDebugThreadDABScopesCommand = class(TFpDebugThreadDABCommand)
  protected
    function GetScopesRequest: TDABScopesRequest;
  public
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABContinueCommand }

  TFpDebugThreadDABContinueCommand = class(TFpDebugThreadDABCommand)
  public
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABDisconnectCommand }

  TFpDebugThreadDABDisconnectCommand = class(TFpDebugThreadDABCommand)
  private
    FFailed: Boolean;
  public
    procedure PreExecute(AController: TDCSCustomController; out DoQueueCommand: boolean); override;
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABNextRequestCommand }

  TFpDebugThreadDABNextRequestCommand = class(TFpDebugThreadDABCommand)
  public
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABStepInRequestCommand }

  TFpDebugThreadDABStepInRequestCommand = class(TFpDebugThreadDABCommand)
  public
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABStepOutRequestCommand }

  TFpDebugThreadDABStepOutRequestCommand = class(TFpDebugThreadDABCommand)
  public
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABPauseCommand }

  TFpDebugThreadDABPauseCommand = class(TFpDebugThreadDABCommand)
  public
    procedure PreExecute(AController: TDCSCustomController; out DoQueueCommand: boolean); override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABEvaluateCommandBaseclass }

  TFpDebugThreadDABEvaluateCommandBaseclass = class(TFpDebugThreadDABCommand)
  protected
    // These are stored so the DoCallParamsFunc callback can access them
    FController: TFpServerDbgController;
    FScope: TFpDbgSymbolScope;
    FContainsFunctionCalls: Boolean;

    {$ifdef UsePasSrc}
    function ObtainFpValueForEvaluationValue(const AValue: TEvaluationValue; out ReturnMessage: string): TFpValue;

    function DoCallParamsFunc(const Func: TEvaluationValue; const ParamValues: array of TEvaluationValue; out ErrMessage: string): TEvaluationValue;
    function DoCallNotAllowedFunc(const Sender: TEvaluationValue; const ParamValues: array of TEvaluationValue; out ErrMessage: string): TEvaluationValue;
    function DoGetIndexedArrayValue(const Value: TEvaluationValue; IndexValues: TEvaluationValueArray; out ErrMessage: string): TEvaluationValue;
    function DoTypecast(const Value: TEvaluationValue; const TypeDefinition: TEvaluationValue; out ErrMessage: string): TEvaluationValue;
    {$endif}
    function EvaluateExpression(const AnExpression: string; const Scope: TFpDbgSymbolScope; const AllowFunctionCalls: Boolean; out ContainsFunctionCalls: Boolean; out ReturnMessage: string): TFpValue;
    function CallFunction(
      const Controller: TFpServerDbgController;
      const FunctionValue: TFpValue;
      const Parameters: array of TFpValue;
      out ReturnMessage: string;
      const Context: TFpDbgLocationContext): TFpValue;
  end;

  { TFpDebugThreadDABVariablesCommand }

  TFpDebugThreadDABVariablesCommand = class(TFpDebugThreadDABEvaluateCommandBaseclass)
  private
    procedure SortVariables(VariableList: TDbgVariableList; Settings: TFpServerDbgControllerSettings);
    function GetVariablesRequest: TDABVariablesRequest;
    function GetParamVariables(Controller: TFpServerDbgController; ProcSymbol: TFpSymbol; Index: Integer;
      Process: TDbgProcess; Thread: TDbgThread; VariableList: TDABVariableList): Boolean;
  public
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABEvaluateCommand }

  TFpDebugThreadDABEvaluateCommand = class(TFpDebugThreadDABEvaluateCommandBaseclass)
  private
    function GetEvaluateRequest: TDABEvaluateRequest;
  public
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TFpDebugThreadDABSetVariableCommand }

  TFpDebugThreadDABSetVariableCommand = class(TFpDebugThreadDABCommand)
  private
    function GetSetVariableRequest: TDABSetVariableRequest;
  public
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

  { TfpDebugDABNotificationEvent }

  TfpDebugDABNotificationEvent = class(TDCSNotificationEvent)
  private
    FBody: TObject;
  public
    destructor Destroy; override;
    property Body: TObject read FBody write FBody;
  end;

implementation

uses
  DebugQuickPause;

type
  TStackFrameReference = class
  public
    CallStackIndex: Integer;
    ThreadId: Integer;
  end;

  TCachedVariableReference = class
  public
    ChildrenReference: SizeInt
  end;

  TAdditionalInfoReference = class
  public
    AdditionalInfoReference: SizeInt
  end;

  { TFunctionCallReference }

  TFunctionCallReference = class
  private
    FExpression: string;
  public
    // Contains a reference to the TFpValue of the function
    FunctionReference: SizeInt;
    // When the TFpValue is not enough, but a full expression has
    // to be evaluated, Scope and Expression are used.
    Scope: TFpDbgSymbolScope;
    property Expression: string read FExpression write FExpression;
  end;

  // Only used to keep a reference to the context that was the result of
  // a function call. Because the content-cache cannot be user here.

  { TFunctionCallContextReference }

  TFunctionCallContextReference = class
  private
    FExpression: string;
    FContext: TFpDbgAbstractCallContext;
    procedure SetContext(AValue: TFpDbgAbstractCallContext);
  public
    constructor Create(AContext: TFpDbgAbstractCallContext);
    destructor Destroy; override;
    property Context: TFpDbgAbstractCallContext read FContext write SetContext;
  end;

{ TFunctionCallContextReference }

destructor TFunctionCallContextReference.Destroy;
begin
  SetContext(nil);
  inherited Destroy;
end;

procedure TFunctionCallContextReference.SetContext(AValue: TFpDbgAbstractCallContext);
begin
  if FContext = AValue then Exit;
  if Assigned(FContext) then
    FContext.ReleaseReference;
  FContext := AValue;
  if Assigned(FContext) then
    FContext.AddReference;
end;

constructor TFunctionCallContextReference.Create(AContext: TFpDbgAbstractCallContext);
begin
  inherited Create;
  SetContext(AContext);
end;

{ TFpDebugThreadDABEvaluateCommand }

function TFpDebugThreadDABEvaluateCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
var
  CallStackIndex: Integer;
  ResponseBody: TDABEvaluateResponseBody;
  Obj: TObject;
  EvaluateRequest: TDABEvaluateRequest;
  ThreadId: Integer;
  CurrentProcess: TDbgProcess;
  Context: TFpDbgSymbolScope;
  Value: TFpValue;
  Variable: TDbgVariable;
  CachedVariableReference: TCachedVariableReference;
  FunctionCallReference: TFunctionCallReference;
  ContainFunctionCalls: Boolean;
  AdditionalInfoReference: TAdditionalInfoReference;
begin
  Result := False;

  try
    CurrentProcess := GetCurrentProcess(AController);
    if not assigned(CurrentProcess) then
      begin
      ReturnMessage := 'Failed to evaluate expression: No process';
      exit;
      end;

    EvaluateRequest := GetEvaluateRequest;

    if EvaluateRequest.Arguments.FrameId > 0 then
      begin
      Obj := TFpServerDbgController(AController).GetObjectByReference(EvaluateRequest.Arguments.FrameId);

      if not Assigned(Obj) or not (Obj is TStackFrameReference) then
        begin
        ReturnMessage := 'Invalid FrameId';
        Exit;
        end;
      ThreadId := TStackFrameReference(Obj).ThreadId;
      CallStackIndex := TStackFrameReference(Obj).CallStackIndex;
      end
    else
      begin
      ThreadId := CurrentProcess.MainThread.ID;
      CallStackIndex := 0;
      end;

    Context := TFpServerDbgController(AController).FindScope(ThreadId, CallStackIndex);
    if Context = nil then
      begin
      ReturnMessage := 'Unknown context';
      exit;
      end;

    FController:=AController as TFpServerDbgController;

    Value := EvaluateExpression(EvaluateRequest.Arguments.Expression, Context, False, ContainFunctionCalls, ReturnMessage);
    if not Assigned(Value) and not ContainFunctionCalls then
      Exit;

    try
      ResponseBody := TDABEvaluateResponseBody.Create;
      try
        // (Value unassigned also means that ContainFunctionCalls is true)
        if not Assigned(Value) then
          begin
          // When there is a function-call within the expression, or when the result
          // itself is a function, just return 'function' and pass a VariablesReference
          // to give the frontend the ability to retrieve the results including
          // function calls
          ResponseBody.Result := 'function';
          FunctionCallReference := TFunctionCallReference.Create;
          FunctionCallReference.Expression := EvaluateRequest.Arguments.Expression;
          FunctionCallReference.Scope := Context;
          ResponseBody.VariablesReference := TFpServerDbgController(AController).AddObjectReference(FunctionCallReference);
          end
        else
          begin
          Variable := TFpServerDbgController(AController).VariableCache.ObtainVariableForValue(Value, 0, Context);
          try
            ResponseBody.Result := Variable.Value;
            ResponseBody.&Type := Variable.&Type;
            if eifHasChildren in Variable.ExtraInformationFlags then
              begin
              CachedVariableReference := TCachedVariableReference.Create;
              CachedVariableReference.ChildrenReference := Variable.ChildrenReference;
              ResponseBody.VariablesReference := TFpServerDbgController(AController).AddObjectReference(CachedVariableReference);
              end
            // Not possible to combine children with additional-info yet
            else if eifHasAdditionalInfo in Variable.ExtraInformationFlags then
              begin
              AdditionalInfoReference := TAdditionalInfoReference.Create;
              AdditionalInfoReference.AdditionalInfoReference := Variable.AdditionalInfoReference;
              ResponseBody.VariablesReference := TFpServerDbgController(AController).AddObjectReference(AdditionalInfoReference);
              end
            else if eifCallableFunction in Variable.ExtraInformationFlags then
              begin
              FunctionCallReference := TFunctionCallReference.Create;
              FunctionCallReference.FunctionReference := Variable.ChildrenReference;
              ResponseBody.VariablesReference := TFpServerDbgController(AController).AddObjectReference(FunctionCallReference);
              end;
            FillVariablePresentationHint(Variable, ResponseBody.PresentationHint);
          finally
            Variable.Free;
          end;
          end;

        FResponseBody := ResponseBody;
        ResponseBody := nil;
        Result := True;
      finally
        ResponseBody.Free;
      end;
    finally
      value.ReleaseReference;
    end;
  except
    on E: Exception do
      ReturnMessage := 'Exception: ' + e.Message;
  end;
end;

function TFpDebugThreadDABEvaluateCommand.GetEvaluateRequest: TDABEvaluateRequest;
begin
  Result := FMessage as TDABEvaluateRequest;
end;

class function TFpDebugThreadDABEvaluateCommand.TextName: string;
begin
  Result := 'evaluate';
end;

{$ifdef UsePasSrc}
function TFpDebugThreadDABEvaluateCommandBaseclass.EvaluateExpression(
  const AnExpression: string;
  const Scope: TFpDbgSymbolScope;
  const AllowFunctionCalls: Boolean;
  out ContainsFunctionCalls: Boolean;
  out ReturnMessage: string): TFpValue;

var
  Expr: TExpressionEvaluator;
  IdentifierList: TEvaluationIdentifierList;
  i: Integer;
  EvaluationValue: TEvaluationValue;
  EvaluationFunctionValue: TEvaluationValue;
  Identifier: TEvaluationIdentifier;
  IdentifierSymbol: TFpValue;
begin
  Result := Nil;
  ContainsFunctionCalls := False;
  ReturnMessage := '';

  FScope := Scope;

  Expr := TExpressionEvaluator.Create(AnExpression);
  try
    try
      if not Expr.Parse then
        begin
        ReturnMessage := 'Failed to parse expression: ' + Expr.Error;
        Exit;
        end;
    Except
      on E: Exception do
        begin
        ReturnMessage := E.Message;
        Exit;
        end
    end;

    IdentifierList := Expr.IdentifierList;
    for i := 0 to IdentifierList.Count -1 do
      begin
      Identifier := IdentifierList[i];
      IdentifierSymbol := Scope.FindSymbol(Identifier.Name);
      try
        if not Assigned(IdentifierSymbol) then
          begin
          ReturnMessage := 'Identifier not found: '+QuoteText(Identifier.Name);
          Exit;
          end;

        Identifier.Value := TDebugEvaluationValue.Create(IdentifierSymbol);
        if (IdentifierSymbol.Kind = skFunction) then
          ContainsFunctionCalls := True;
      finally
        IdentifierSymbol.ReleaseReference;
      end;
      end;

    if not AllowFunctionCalls and ContainsFunctionCalls then
      Exit;

    if AllowFunctionCalls then
      Expr.OnCallParamsFunc := @DoCallParamsFunc
    else
      begin
      Expr.OnCallParamsFunc := @DoCallNotAllowedFunc;
      end;
    Expr.OnGetIndexedValue := @DoGetIndexedArrayValue;
    Expr.OnTypecast := @DoTypecast;

    EvaluationValue := Expr.Evaluate;
    try
      ContainsFunctionCalls := FContainsFunctionCalls or ContainsFunctionCalls;
      if not Assigned(EvaluationValue) then
        begin
        ReturnMessage := Expr.Error;
        Exit;
        end;

      if (EvaluationValue.Kind=evkFunction) and AllowFunctionCalls then
        begin
        // This only happens when a function/method or procedure call is given
        // without brackets. But this is Pascal, where a function has to be
        // called also in this particular case.
        EvaluationFunctionValue := DoCallParamsFunc(EvaluationValue, [], ReturnMessage);
        try
          if not Assigned(EvaluationFunctionValue) then
            Exit;
          Result := ObtainFpValueForEvaluationValue(EvaluationFunctionValue, ReturnMessage);
        finally
          EvaluationFunctionValue.Free;
        end;
        end
      else
        begin
        Result := ObtainFpValueForEvaluationValue(EvaluationValue, ReturnMessage);
        end;
    finally
      EvaluationValue.Free;
    end;
  finally
    Expr.Free;
  end;
end;
{$else}
function TFpDebugThreadDABEvaluateCommandBaseclass.EvaluateExpression(
  const AnExpression: string;
  const Scope: TFpDbgSymbolScope;
  const AllowFunctionCalls: Boolean;
  out ContainsFunctionCalls: Boolean;
  out ReturnMessage: string): TFpValue;
var
  PasExpr: TFpPascalExpression;
begin
  ContainsFunctionCalls := False;
  ReturnMessage := '';
  FScope := Scope;
  PasExpr := TFpPascalExpression.Create(AnExpression, Scope);
  try
    Result := PasExpr.ResultValue;
    if not PasExpr.Valid then
      ReturnMessage := ErrorHandler.ErrorAsString(PasExpr.Error)
    else
      begin
      ContainsFunctionCalls := Result.Kind=skFunction;
      if ContainsFunctionCalls and AllowFunctionCalls then
        Result := CallFunction(FController, Result, [], ReturnMessage, FScope.LocationContext)
      else
        Result.AddReference;
      end;
  finally
    PasExpr.Free;
  end;
end;
{$endif}

{ TFpDebugThreadDABPauseCommand }

procedure TFpDebugThreadDABPauseCommand.PreExecute(AController: TDCSCustomController; out DoQueueCommand: boolean);
begin
  TFpServerDbgController(AController).DbgController.Pause;
  DoQueueCommand := False;
end;

class function TFpDebugThreadDABPauseCommand.TextName: string;
begin
  Result := 'pause';
end;

{ TFpDebugThreadDABStepOutRequestCommand }

function TFpDebugThreadDABStepOutRequestCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
begin
  TFpServerDbgController(AController).DbgController.StepOut;
  TFpServerDbgController(AController).RequestRunLoop;
  result := true;
end;

class function TFpDebugThreadDABStepOutRequestCommand.TextName: string;
begin
  Result := 'stepOut';
end;

{ TFpDebugThreadDABStepInRequestCommand }

function TFpDebugThreadDABStepInRequestCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
begin
  TFpServerDbgController(AController).DbgController.Step;
  TFpServerDbgController(AController).RequestRunLoop;
  result := true;
end;

class function TFpDebugThreadDABStepInRequestCommand.TextName: string;
begin
  Result := 'stepIn';
end;

{ TFpDebugThreadDABNextRequestCommand }

function TFpDebugThreadDABNextRequestCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
begin
  TFpServerDbgController(AController).DbgController.Next;
  TFpServerDbgController(AController).RequestRunLoop;
  result := true;
end;

class function TFpDebugThreadDABNextRequestCommand.TextName: string;
begin
  Result := 'next';
end;

type

  { TArgumentsReference }

  TArgumentsReference = class
  private
    FProcSymbol: TFpSymbol;
    procedure SetProcSymbol(AValue: TFpSymbol);
  public
    Thread: TDbgThread;
    CallStackIndex: Integer;
    destructor Destroy; override;
    property ProcSymbol: TFpSymbol read FProcSymbol write SetProcSymbol;
  end;

  { TLocalsReference }

  TLocalsReference = class
  public
    Scope: TFpDbgSymbolScope;
  end;

{ TArgumentsReference }

procedure TArgumentsReference.SetProcSymbol(AValue: TFpSymbol);
begin
  if FProcSymbol = AValue then Exit;
  FProcSymbol.ReleaseReference;
  FProcSymbol := AValue;
  if Assigned(FProcSymbol) then
    FProcSymbol.AddReference;
end;

destructor TArgumentsReference.Destroy;
begin
  ProcSymbol := nil;
  inherited Destroy;
end;

{ TFpDebugThreadDABVariablesCommand }

function TFpDebugThreadDABVariablesCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
var
  CallStackIndex: Integer;
  Thread: TDbgThread;
  ResponseBody: TDABVariablesResponseBody;
  Obj: TObject;
  Variables: TDbgVariableList;
  Controller: TFpServerDbgController;
  Value: TFpValue;
  Variable: TDbgVariable;
  ContainsFunctionCalls: Boolean;
  Scope: TFpDbgLocationContext;
begin
  Result := False;

  Controller := TFpServerDbgController(AController);
  FController := Controller;
  Obj := Controller.GetObjectByReference(GetVariablesRequest.Arguments.VariablesReference);

  if not Assigned(Obj) then
    begin
    ReturnMessage := 'Invalid variablesreference';
    Exit;
    end;

  ResponseBody := TDABVariablesResponseBody.Create;
  try
    if (Obj is TArgumentsReference) then
      begin
      Thread := TArgumentsReference(obj).Thread;
      CallStackIndex := TArgumentsReference(obj).CallStackIndex;
      GetParamVariables(
        Controller,
        TArgumentsReference(obj).ProcSymbol, CallStackIndex,
        GetCurrentProcess(AController),
        Thread,
        ResponseBody.Variables);
      end;

    if Obj is TLocalsReference then
      begin
      Variables := TFpServerDbgController(AController).VariableBuilder.ObtainLocalVariablesForScope(TLocalsReference(Obj).Scope, 0);
      try
        SortVariables(Variables, Controller.Settings);
        VariablesToDABVariable(Controller, Variables, ResponseBody.Variables);
      finally
        Variables.Free;
      end;
      end;

    if Obj is TCachedVariableReference then
      begin
      Variables := TFpServerDbgController(AController).VariableCache.ObtainVariablesForReference(
        TCachedVariableReference(Obj).ChildrenReference,
        GetVariablesRequest.Arguments.Start,
        GetVariablesRequest.Arguments.Count,
        0);
      try
        if Assigned(Variables) then
          begin
          if not Variables.ChildrenAreIndexed then
            SortVariables(Variables, Controller.Settings);
          VariablesToDABVariable(TFpServerDbgController(AController), Variables, ResponseBody.Variables);
          end;
      finally
        Variables.Free;
      end;
      end;

    if Obj is TFunctionCallReference then
      begin
      // When a function-call variable is explicitly asked for, the function-
      // has to be executed.
      if TFunctionCallReference(Obj).FunctionReference > 0 then
        begin
        TFpServerDbgController(AController).VariableCache.ObtainValueForReference(TFunctionCallReference(Obj).FunctionReference, Value, Scope);
        Value := CallFunction(Controller, Value, [TFpValueDwarf(Value).StructureValue], ReturnMessage, Scope);
        end
      else
        Value := EvaluateExpression(TFunctionCallReference(Obj).Expression, TFunctionCallReference(Obj).Scope, True, ContainsFunctionCalls, ReturnMessage);

      if not Assigned(Value) then
        Exit;

      Variable := TFpServerDbgController(AController).VariableCache.ObtainVariableForValue(Value, 0, nil);
      try
        Variable.Name := 'Result';
        Variables := TDbgVariableList.Create(False);
        try
          Variables.Add(Variable);
          VariablesToDABVariable(TFpServerDbgController(AController), Variables, ResponseBody.Variables);
        finally
          Variables.Free;
        end;
      finally
        Variable.Free;
      end;
      Value.ReleaseReference;
      end;

    if Obj is TAdditionalInfoReference then
      begin
      Variables := TFpServerDbgController(AController).VariableCache.ObtainAdditionalInfoForReference(
        TCachedVariableReference(Obj).ChildrenReference);
      try
        if Assigned(Variables) then
          VariablesToDABVariable(TFpServerDbgController(AController), Variables, ResponseBody.Variables);
      finally
        Variables.Free;
      end;
      end;

    FResponseBody := ResponseBody;
    ResponseBody := nil;
    Result := True;
  finally
    ResponseBody.Free;
  end;
end;

function TFpDebugThreadDABVariablesCommand.GetVariablesRequest: TDABVariablesRequest;
begin
  Result := FMessage as TDABVariablesRequest;
end;

function TFpDebugThreadDABVariablesCommand.GetParamVariables(
  Controller: TFpServerDbgController;
  ProcSymbol: TFpSymbol;
  Index: Integer;
  Process: TDbgProcess;
  Thread: TDbgThread;
  VariableList: TDABVariableList): Boolean;
var
  ProcVal: TFpValue;
  Context: TFpDbgLocationContext;
  m: TFpValue;
  i: Integer;
  DbgVariable: TDbgVariable;
  DbgVariableList: TDbgVariableList;
begin
  if assigned(ProcSymbol) then
    begin
    ProcVal := ProcSymbol.Value;
    if (ProcVal <> nil) then
      begin
      Context := Process.ContextFromProc(Thread.ID, Index, ProcSymbol);
      if Context <> nil then
        begin
        TFpValueDwarf(ProcVal).Context := Context;
        DbgVariableList := TDbgVariableList.Create(True);
        try
          for i := 0 to ProcVal.MemberCount - 1 do
            begin
            m := ProcVal.Member[i];
            if (m <> nil) and (sfParameter in m.DbgSymbol.Flags) then
              begin
              DbgVariable := Controller.VariableCache.ObtainVariableForValue(m, 0, nil);
              DbgVariableList.Add(DbgVariable);
              end;
            m.ReleaseReference;
            end;
          SortVariables(DbgVariableList, Controller.Settings);
          VariablesToDABVariable(Controller, DbgVariableList, VariableList);
        finally
          DbgVariableList.Free;
        end;
        TFpValueDwarf(ProcVal).Context := nil;
        Context.ReleaseReference;
        end;
      ProcVal.ReleaseReference;
     end;
    end;
  Result := True;
end;

class function TFpDebugThreadDABVariablesCommand.TextName: string;
begin
  Result := 'variables';
end;

procedure TFpDebugThreadDABVariablesCommand.SortVariables(VariableList: TDbgVariableList; Settings: TFpServerDbgControllerSettings);
begin
  VariableList.Sort(Settings.SortBy);
end;

function TFpDebugThreadDABEvaluateCommandBaseclass.CallFunction(
  const Controller: TFpServerDbgController;
  const FunctionValue: TFpValue;
  const Parameters: array of TFpValue;
  out ReturnMessage: string;
  const Context: TFpDbgLocationContext): TFpValue;

  function OutputFunctionName: string;
  begin
    if Assigned(FunctionValue.DbgSymbol) then
      begin
      Result := FunctionValue.DbgSymbol.Name;
      if Result <> '' then
        Result := ' ['+Result+']';
      end
    else
      Result := '';
  end;

var
  FunctionResultDataSize: TFpDbgValueSize;
  val: TFpValue;
  FuncResultValue, ParamSymbol: TFpValue;
  FuncResultVariable: TFpSymbolDwarfFunctionResult;
  ProcAddress: TFpDbgMemLocation;
  FuncResultMemLocation: TFpDbgMemLocation;
  FunctionTypeSymbol: TFpSymbol;
  FunctionResultSymbol: TFpSymbol;
  FunctionResultTypeSymbol, TempSymbol: TFpSymbol;
  CallContext: TFpDbgInfoCallContext;
  DbgController: TDbgController;
  ParameterSymbolArr: array of TFpSymbol;
  i: Integer;
begin
  Result := nil;
  ReturnMessage := '';
  DbgController := Controller.DbgController;
  FunctionResultSymbol := FunctionValue.DbgSymbol;

  FunctionTypeSymbol := FunctionValue.TypeInfo;
  if not Assigned(FunctionTypeSymbol) then
    begin
    ReturnMessage := Format('Unable to call function %s. Function-type unknown. (Is it really a function?)', [OutputFunctionName]);
    Exit;
    end;
  FunctionResultTypeSymbol := FunctionTypeSymbol.TypeInfo;

  // Bind the given parameters to the parameters in the debug-info, and check
  // if the amount of parameters matches.
  ParameterSymbolArr := [];
  try
    for i := 0 to FunctionTypeSymbol.NestedSymbolCount - 1 do
      begin
      // Note that FunctionTypeSymbol.NestedSymbol[i] is more like an iterator.
      // If the reference-count of TempSymbol is not increased, it will return
      // the same symbol over and over again, for each index.
      TempSymbol := FunctionTypeSymbol.NestedSymbol[i];
      if sfParameter in TempSymbol.Flags then
        begin
        if High(Parameters) < length(ParameterSymbolArr) then
          begin
          // The missing space is on purpose!
          ReturnMessage := Format('Unable to call function%s. Not enough parameters supplied.', [OutputFunctionName]);
          Exit;
          end;
        Insert(TempSymbol, ParameterSymbolArr, High(ParameterSymbolArr));
        TempSymbol.AddReference;
        end;
      end;

    if length(ParameterSymbolArr) < Length(Parameters) then
      begin
      ReturnMessage := Format('Unable to call function%s. Too many parameters provided. Expected %d, got %d.', [OutputFunctionName, length(ParameterSymbolArr), Length(Parameters)]);
      Exit;
      end;

    // Retrieve the address of the function and check if it does not contain
    // a bogus value.
    val := FunctionResultSymbol.Value;
    try
      ProcAddress := Val.DataAddress;
    finally
      val.ReleaseReference;
    end;
    if not IsReadableLoc(ProcAddress) then
      begin
      ReturnMessage := Format('Unable to call function%s. Address [0x' + HexStr(LocToAddrOrNil(ProcAddress),
        DbgController.CurrentProcess.PointerSize * 2) + '] is not readable', [OutputFunctionName]);
      Exit;
      end
    else if not (FunctionResultTypeSymbol.Kind in [skInteger, skCurrency, skPointer, skEnum,
      skCardinal, skBoolean, skChar, skClass]) then
      begin
      ReturnMessage := Format('Unable to call function%s. The type of the function result is not supported', [OutputFunctionName]);
      Exit;
      end
    // Final check. If the DataSize does not fit into the rax register, the
    // function result is not returned in it, and all we are doing is just
    // bogus...
    else if not FunctionResultTypeSymbol.ReadSize(nil, FunctionResultDataSize) or (
      FunctionResultDataSize > Controller.MemReader.RegisterSize(0)) then
      begin
      ReturnMessage := Format('Unable to call function%s. The size of the function-result exceeds the content-size of a register.', [OutputFunctionName]);
      Exit;
      end
    else
      begin
      // Call the function
      CallContext := DbgController.Call(ProcAddress, Context, Controller.MemReader, Controller.MemConverter);
      try
        // Set parameter values
        for i := 0 to High(ParameterSymbolArr) do
          begin
          ParamSymbol := CallContext.CreateParamSymbol(i, ParameterSymbolArr[i]);
          try
            ParamSymbol.AsCardinal := Parameters[i].AsCardinal;
          finally
            ParamSymbol.ReleaseReference;
          end;
          end;

        DbgController.ProcessLoop;
        // ToDo: invalidate all cached data. Data might have changed

        if not CallContext.IsValid then
          begin
          ReturnMessage := CallContext.Message;
          Exit;
          end;

        // Add an object-reference to the controller that keeps a reference to the
        // call-context. This way it stays possible to access this context, for example
        // in the case that the TFpValue encapsulated in the TEvaluationValue which
        // is the result of this function is evaluated.
        // Works similar as the content-cache, which we can not use here.
        Controller.AddObjectReference(TFunctionCallContextReference.Create(CallContext));

        // Create a symbol to retrieve the function-result. The debug-info does
        // not contain information on where the function-result is stored. So this
        // info is hard-coded.
        // The assumptions here are only valid on x86_64-linux with the default
        // calling-convention and for ordinal result-types. Checks for this are
        // done earlier.
        FuncResultMemLocation := RegisterLoc(0); // Ordinal values are returned in the rax-register (index = 0);
        FuncResultVariable := TFpSymbolDwarfFunctionResult.Create(FunctionResultSymbol.Name,
          TDbgDwarfSymbolBase(FunctionResultSymbol).InformationEntry, FunctionResultTypeSymbol.Kind,
          FuncResultMemLocation);
        try
          // Obtain the value for the function-result:
          FuncResultValue := FuncResultVariable.Value;
          try
            // Some magic to set the call-context of the function-result. By doing this
            // the FuncResultValue is evaluated in the context of the situation just
            // after the function-call.
            if FuncResultValue is TFpValueDwarf then
              TFpValueDwarf(FuncResultValue).Context := CallContext;

            Result := FuncResultValue;
            FuncResultValue := nil;
          finally
            FuncResultValue.ReleaseReference;
          end;
        finally
          FuncResultVariable.ReleaseReference;
        end;
      finally
        CallContext.ReleaseReference;
      end;
      end;
  finally
    for i := Low(ParameterSymbolArr) to High(ParameterSymbolArr) do
      ParameterSymbolArr[i].ReleaseReference;
  end;
end;

{$ifdef UsePasSrc}
function TFpDebugThreadDABEvaluateCommandBaseclass.DoCallParamsFunc(const Func: TEvaluationValue; const ParamValues: array of TEvaluationValue; out ErrMessage: string): TEvaluationValue;
var
  FunctionValue: TFpValue;
  Value, ParentValue: TFpValue;
  FpParamValueArr: array of TFpValue;
  HiddenParameterAmount: Integer;
  i: Integer;
begin
  FunctionValue := (Func as TDebugEvaluationValue).Value;
  ParentValue := (Func as TDebugEvaluationValue).Parent;
  Result := nil;
  FpParamValueArr := [];

  if Assigned(ParentValue) then
    HiddenParameterAmount := 1
  else
    HiddenParameterAmount := 0;

  SetLength(FpParamValueArr, Length(ParamValues) + HiddenParameterAmount);

  if Assigned(ParentValue) then
    begin
    FpParamValueArr[0] := ParentValue;
    ParentValue.AddReference;
    end;

  try
    // Convert the parameters to a format CallFunction can deal with
    for i := 0 to high(ParamValues) do
      begin
      FpParamValueArr[i+HiddenParameterAmount] := ObtainFpValueForEvaluationValue(ParamValues[i], ErrMessage);
      if not Assigned(FpParamValueArr[i]) then
        Exit;
      end;

    Value := CallFunction(FController, FunctionValue, FpParamValueArr, ErrMessage, FScope.LocationContext);
    try
      // ToDo: double and clumpsy code, due to the ability to compile without the
      // cnocExpressionEvaluator (CalFunction should return a TDebugEvaluationValue)
      // Create a Variable and fill it with the contents of the FuncResultValue
      if Assigned(Value) then
        Result := TDebugEvaluationValue.Create(Value);
    finally
      Value.ReleaseReference;
    end;


  finally
    for i := 0 to high(FpParamValueArr) do
      FpParamValueArr[i].ReleaseReference;
  end;
end;

function TFpDebugThreadDABEvaluateCommandBaseclass.ObtainFpValueForEvaluationValue(const AValue: TEvaluationValue; out ReturnMessage: string): TFpValue;
var
  Str: string;
  Bool: Boolean;
  Int: Int64;
  Float: ValReal;
  Cardinal: QWord;
begin
  Result := nil;
  if AValue is TDebugEvaluationValue then
    begin
    Result := TDebugEvaluationValue(AValue).Value;
    Result.AddReference;
    end
  else
    begin
      case AValue.Kind of
        evkString:
          begin
          if AValue.TryGetAsString(Str, ReturnMessage) then
            Result := TFpValueConstString.Create(Str);
          end;
        evkBoolean:
          begin
          if AValue.TryGetAsBoolean(Bool, ReturnMessage) then
            Result := TFpValueConstBool.Create(Bool);
          end;
        evkInteger:
          begin
          if AValue.TryGetAsInteger(Int, ReturnMessage) then
            Result := TFpValueConstNumber.Create(Int, True);
          end;
        evkCardinal:
          begin
          if AValue.TryGetAsCardinal(Cardinal, ReturnMessage) then
            Result := TFpValueConstNumber.Create(Cardinal, False);
          end;
        evkFloat:
          begin
          if AValue.TryGetAsFloat(Float, ReturnMessage) then
            Result := TFpValueConstFloat.Create(Float);
          end;
      else
        ReturnMessage := 'Failed to convert expression-result to debug-value';
      end
    end;
end;

function TFpDebugThreadDABEvaluateCommandBaseclass.DoCallNotAllowedFunc(const Sender: TEvaluationValue; const ParamValues: array of TEvaluationValue; out ErrMessage: string): TEvaluationValue;
begin
  Result := nil;
  FContainsFunctionCalls := True;
end;

function TFpDebugThreadDABEvaluateCommandBaseclass.DoGetIndexedArrayValue(const Value: TEvaluationValue; IndexValues: TEvaluationValueArray; out ErrMessage: string): TEvaluationValue;
var
  DebugEvaluationValue: TDebugEvaluationValue;
  DebugValue: TFpValue;
  MemberValue: TFpValue;
  Index: Int64;
begin
  Assert(Length(IndexValues)>0);
  Result := nil;
  DebugEvaluationValue := TDebugEvaluationValue(Value);
  DebugValue := DebugEvaluationValue.Value;

  if Assigned(DebugValue) then
    begin
    if Length(IndexValues) > 1 then
      begin
      ErrMessage := 'Multiple indexes are not supported';
      Exit;
      end;

    if IndexValues[0].TryGetAsInteger(Index, ErrMessage) then
      begin
      if DebugValue.HasBounds and (Index > DebugValue.OrdHighBound) or (Index < DebugValue.OrdLowBound) then
        begin
        ErrMessage := Format('Index [%d] out of bounds', [Index]);
        Exit;
        end;
      MemberValue := DebugValue.Member[Index];
      if Assigned(MemberValue) then
        begin
        // DebugValue is added as parent, so that a reference to it is held. Or else
        // the reference-count system will get messed up. (The childs in the index
        // do not have a reference to their parents themselves)
        Result := TDebugEvaluationValue.Create(DebugValue.Member[Index], DebugValue);
        // No idea why...
        DebugValue.AddReference;
        end;
      end;
    end;
end;

function TFpDebugThreadDABEvaluateCommandBaseclass.DoTypecast(const Value: TEvaluationValue; const TypeDefinition: TEvaluationValue; out ErrMessage: string): TEvaluationValue;
var
  FpValue, FpTypeDefinition: TFpValue;
begin
  FpValue := (Value as TDebugEvaluationValue).Value;
  FpTypeDefinition := (TypeDefinition as TDebugEvaluationValue).Value;
  // No idea why... And it seems to leak too.
  FpTypeDefinition.AddReference;

  FpValue := FpTypeDefinition.GetTypeCastedValue(FpValue);
  try
    Result := TDebugEvaluationValue.Create(FpValue, (Value as TDebugEvaluationValue).Parent);
  finally
    FpValue.ReleaseReference;
  end;
end;

{$endif}

{ TFpDebugThreadDABDisconnectCommand }

function TFpDebugThreadDABDisconnectCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
begin
  if FFailed then
    begin
    ReturnMessage := 'No active debug session';
    Result := False;
    exit;
    end;

  Result := True;
  if not assigned(TFpServerDbgController(AController).DbgController.CurrentProcess) then
    begin
    // Nothing to do here
    exit;
    end;

  TFpServerDbgController(AController).RequestRunLoop;
end;

class function TFpDebugThreadDABDisconnectCommand.TextName: string;
begin
  Result := 'disconnect';
end;

procedure TFpDebugThreadDABDisconnectCommand.PreExecute(AController: TDCSCustomController; out DoQueueCommand: boolean);
begin
  DoQueueCommand := True;
  FFailed := False;
  if not assigned(TFpServerDbgController(AController).DbgController.CurrentProcess) then
    begin
    FFailed := True;
    exit;
    end;

  TFpServerDbgController(AController).DbgController.Stop;
end;

{ TFpDebugThreadDABContinueCommand }

function TFpDebugThreadDABContinueCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
begin
  TFpServerDbgController(AController).RequestRunLoop;
  result := true;
end;

class function TFpDebugThreadDABContinueCommand.TextName: string;
begin
  Result := 'continue';
end;

{ TFpDebugThreadDABScopesCommand }

function TFpDebugThreadDABScopesCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
var
  ScopesResponse: TDABScopesResponseBody;
  Scope: TDABScope;
  Thread: TDbgThread;
  CallStackIndex: Integer;
  ThreadId: Integer;
  ProcSymbol: TFpSymbol;
  ArgumentsReference: TArgumentsReference;
  CurrentProcess: TDbgProcess;
  SymbolScope: TFpDbgSymbolScope;
  LocalsReference: TLocalsReference;
  ProcSymbolValue, SymbolScopeProcedureAtAddress: TFpValue;
begin
  result := false;
  CurrentProcess := GetCurrentProcess(AController);
  if not Assigned(CurrentProcess) then
    begin
    ReturnMessage := 'Failed to get scopes: No process';
    exit;
    end;

  if not RetrieveStackFrameFromDABId(AController, GetScopesRequest.Arguments.FrameId, ThreadId, CallStackIndex) then
    begin
    ReturnMessage := 'Invalid FrameId';
    Exit;
    end;

  if not CurrentProcess.GetThread(ThreadId, Thread) then
    begin
    ReturnMessage := 'Thread '+IntToStr(ThreadId) + ' not found.';
    Exit;
    end;

  Thread.PrepareCallStackEntryList;
  if CallStackIndex >= Thread.CallStackEntryList.Count then
    begin
    ReturnMessage := 'Invalid stack-index ' + IntToStr(CallStackIndex);
    Exit;
    end;

  ScopesResponse := TDABScopesResponseBody.Create;
  try
    ProcSymbol := Thread.CallStackEntryList.Items[CallStackIndex].ProcSymbol;
    if Assigned(ProcSymbol) then
      begin
      ProcSymbolValue := ProcSymbol.Value;
      try
        if Assigned(ProcSymbolValue) and (ProcSymbolValue.MemberCount > 0) then
          begin
          Scope := TDABScope.Create;
          Scope.Name := 'Arguments';
          Scope.PresentationHint := 'arguments';
          Scope.Expensive := False;
          ArgumentsReference := TArgumentsReference.Create;
          ArgumentsReference.ProcSymbol := ProcSymbol;
          ArgumentsReference.CallStackIndex := CallStackIndex;
          ArgumentsReference.Thread := Thread;
          Scope.VariablesReference := TFpServerDbgController(AController).AddObjectReference(ArgumentsReference);
          ScopesResponse.Scopes.Add(Scope);
          end;
      finally
        ProcSymbolValue.ReleaseReference;
      end;
      end;

    SymbolScope := TFpServerDbgController(AController).FindScope(ThreadId, CallStackIndex);
    if (SymbolScope <> nil) and (SymbolScope.SymbolAtAddress <> nil) then
      begin
      SymbolScopeProcedureAtAddress := SymbolScope.ProcedureAtAddress;
      try
        // SymbolScopeProcedureAtAddress is nil when there is no debug-information
        // for the given location.
        if Assigned(SymbolScopeProcedureAtAddress) then
          if SymbolScopeProcedureAtAddress.MemberCount > 0 then
            begin
            Scope := TDABScope.Create;
            Scope.Name := 'Locals';
            Scope.PresentationHint := 'locals';
            Scope.Expensive := False;
            LocalsReference := TLocalsReference.Create;
            LocalsReference.Scope := SymbolScope;
            Scope.VariablesReference := TFpServerDbgController(AController).AddObjectReference(LocalsReference);
            ScopesResponse.Scopes.Add(Scope);
            end;
      finally
        SymbolScopeProcedureAtAddress.ReleaseReference;
      end;
      end;

    FResponseBody := ScopesResponse;
    ScopesResponse := nil;
    Result := True;
  finally
    ScopesResponse.Free;
  end;
end;

class function TFpDebugThreadDABScopesCommand.TextName: string;
begin
  Result := 'scopes';
end;

function TFpDebugThreadDABScopesCommand.GetScopesRequest: TDABScopesRequest;
begin
  Result := FMessage as TDABScopesRequest;
end;

{ TFpDebugThreadDABConfigurationDoneCommand }

function TFpDebugThreadDABConfigurationDoneCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
begin
  result := false;
  if not assigned(TFpServerDbgController(AController).DbgController.CurrentProcess) then
    begin
    ReturnMessage := 'Failed to finish configuration: No process';
    exit;
    end;

  TFpServerDbgController(AController).SetPascalExceptionBreakpoint();
  TFpServerDbgController(AController).RequestRunLoop;
  result := true;
end;

class function TFpDebugThreadDABConfigurationDoneCommand.TextName: string;
begin
  Result := 'configurationDone';
end;

{ TFpDebugThreadDABStackTraceCommand }

function TFpDebugThreadDABStackTraceCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
var
  ThreadCallStack: TDbgCallstackEntryList;
  StackResponseBody: TDABStackTraceResponseBody;
  i, StartFrame, EndFrame: Integer;
  StackFrame: TDABStackFrame;
  DbgController: TDbgController;
  CurrentProcess: TDbgProcess;
  Thread: TDbgThread;
  ThreadId: Integer;
begin
  result := false;
  if not assigned(TFpServerDbgController(AController).DbgController.CurrentProcess) then
    begin
    ReturnMessage := 'Failed to get stacktrace: No process';
    exit;
    end;

  StackResponseBody := TDABStackTraceResponseBody.Create;
  try
    DbgController := TFpServerDbgController(AController).DbgController;
    CurrentProcess := DbgController.CurrentProcess;
    ThreadId := GetStackTraceRequest.Arguments.ThreadId;

    if ThreadId>0 then
      begin
      if not CurrentProcess.GetThread(ThreadId, Thread) then
        begin
        ReturnMessage := 'There is no thread with ThreadId ' + IntToStr(ThreadId);
        exit;
        end;
      end
    else
      begin
      Thread := DbgController.CurrentThread;
      ThreadId := Thread.ID;
      end;

    Thread.PrepareCallStackEntryList;
    ThreadCallStack := Thread.CallStackEntryList;
    StackResponseBody.TotalFrames := ThreadCallStack.Count;

    StartFrame := GetStackTraceRequest.Arguments.StartFrame;
    EndFrame := min(StartFrame + GetStackTraceRequest.Arguments.Levels, ThreadCallStack.Count);
    StackResponseBody.StackFrames.Count := EndFrame - StartFrame;

    for i := StartFrame to EndFrame-1 do
      begin
      StackFrame := TDABStackFrame.Create;
      try
        StackFrame.Id:=ComposeDABStackFrameId(AController, ThreadId, ThreadCallStack[i+StartFrame].Index);
        StackFrame.Name:=ThreadCallStack[i+StartFrame].FunctionName;
        StackFrame.Line:=ThreadCallStack[i+StartFrame].Line;
        StackFrame.PresentationHint:='normal';

        if ThreadCallStack[i].SourceFile<>'' then
          begin
          StackFrame.Source := TDABSource.Create;
          StackFrame.Source.Path:=ThreadCallStack[i].SourceFile;
          StackFrame.Source.Name:=ExtractFileName(ThreadCallStack[i].SourceFile);
          end;
        StackResponseBody.StackFrames[i] := StackFrame;
        StackFrame := nil;
      finally
        StackFrame.Free;
      end;
      end;
    // Clear the callstack immediately. Doing this each time the process continous is
    // cumbersome. And the chances that this command is called twice, so that
    // caching the result is usefull, are slim.
    Thread.ClearCallStack;
    FResponseBody := StackResponseBody;
    StackResponseBody := nil;
    result := true;
  finally
    StackResponseBody.Free;
  end;
end;

function TFpDebugThreadDABStackTraceCommand.GetStackTraceRequest: TDABStackTraceRequest;
begin
  Result := FMessage as TDABStackTraceRequest;
end;

class function TFpDebugThreadDABStackTraceCommand.TextName: string;
begin
  Result := 'stackTrace';
end;

{ TFpDebugThreadDABThreadsCommand }

function TFpDebugThreadDABThreadsCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
var
  ThreadArray: TFPDThreadArray;
  Thread: TDABThread;
  i: Integer;
begin
  result := false;
  if not assigned(TFpServerDbgController(AController).DbgController.CurrentProcess) then
    begin
    ReturnMessage := 'Failed to retrieve thread-list: No process';
    exit;
    end;

  ThreadArray := TFpServerDbgController(AController).DbgController.CurrentProcess.GetThreadArray;
  FResponseBody := TDABThreadsResponseBody.Create;
  for i := 0 to High(ThreadArray) do
    begin
    Thread := TDABThread.Create;
    TDABThreadsResponseBody(FResponseBody).Threads.Add(Thread);
    Thread.Id := ThreadArray[i].ID;
    Thread.Name := IntToStr(ThreadArray[i].Handle);
    end;
  Result := True;
end;

class function TFpDebugThreadDABThreadsCommand.TextName: string;
begin
  Result := 'threads';
end;

{ TFpDebugThreadDABSetExceptionBreakpointsCommand }

function TFpDebugThreadDABSetExceptionBreakpointsCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
begin
  // Do nothing. Maybe implement this later
  Result := True;
end;

class function TFpDebugThreadDABSetExceptionBreakpointsCommand.TextName: string;
begin
  Result := 'setExceptionBreakpoints';
end;

{ TFpDebugThreadDABSetBreakpointsCommand }

function TFpDebugThreadDABSetBreakpointsCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
var
  i,j,p: Integer;
  Breakpoint: TFpInternalBreakpoint;
  DABBreakpoint: TDABBreakpoint;
  BreakpointsArr: TFpserverBreakpointArray;
  Lib: TDbgLibrary;
  Source: string;
  Line: Integer;
  PtrArray: TDBGPtrArray;
  BreakpointId: Integer;
  Message, Path: string;
begin
  result := false;
  if not assigned(GetCurrentProcess(AController)) then
    begin
    ReturnMessage := 'Failed to add breakpoints: No process';
    exit;
    end;
  Source := GetSetBreakpointsRequest.Arguments.Source.Path;
  if (Source<>'') then
    begin
    FResponseBody := TDABBreakpointsResponseBody.Create;
    BreakpointsArr := TFpServerDbgController(AController).GetBreakpointsForSource(Source);

    for i := 0 to GetSetBreakpointsRequest.Arguments.Breakpoints.Count -1 do
      begin
      Breakpoint := nil;
      BreakpointId := -1;
      Message := '';
      Line := GetSetBreakpointsRequest.Arguments.Breakpoints[i].Line;

      // See if the breakpoint is already 'known'. For now, a breakpoint is uniquely
      // defined by it's source and line
      for j := 0 to High(BreakpointsArr) do
        begin
        if BreakpointsArr[j].Line=Line then
          begin
          // In case the breakpoint is known, use the existing ID and breakpoint.
          BreakpointId := BreakpointsArr[j].Id;
          Breakpoint := BreakpointsArr[j].Breakpoint;
          // Mark that this breakpoint is requested, so that it should not
          // be deleted.
          BreakpointsArr[j].Id:=-1;
          end;
        end;

      // When the breakpoint is not set, (re)-try to set it
      if not Assigned(Breakpoint) then
        begin
        BreakPoint := TFpServerDbgController(AController).DbgController.CurrentProcess.AddBreak(Source, Line);

        // Update the internal information and return the (new) BreakpointId
        TFpServerDbgController(AController).SetInternalBreakPointInfo(BreakPoint, Source, Line, BreakpointId);

        if not Assigned(Breakpoint) then
          begin
          // Setting the breakpoint failed. Give the user a little bit more information.
          if not GetCurrentProcess(AController).DbgInfo.HasInfo then
            // When the main executable does not include debug-information, we tell
            // the user. In principle it could be that one of the libraries does
            // have debug-info and the executable intentionally does not have
            // debug info, but this is an exception.
            Message := 'No debug-information found for the executable'
          else
            begin
            // Check if there is line-information available, just to inform
            // the user.
            PtrArray := [];
            GetCurrentProcess(AController).GetLineAddresses(Source, Line, PtrArray);
            if Length(PtrArray) = 0 then
              Message := Format('No debug-information found for [%s] line [%d] within the application or any of the currently loaded modules', [Source, Line])
            else
              Message := Format('Debug-information has been found for [%s] line [%d], but setting the breakpoint failed for some other unknown reason', [Source, Line]);
            end;
          end;
        end;

      DABBreakpoint := TDABBreakpoint.Create;
      DABBreakpoint.Message := Message;
      TDABBreakpointsResponseBody(FResponseBody).Breakpoints.Add(DABBreakpoint);
      DABBreakpoint.Line := Line;

      DABBreakpoint.Id := BreakpointId;
      DABBreakpoint.Verified := Assigned(Breakpoint);
      if not DABBreakpoint.Verified then
        DABBreakpoint.Message := Message;
      end;

    // Check if there are any breakpoints in the internal administration, which
    // are not requested anymore. We have to remove those.
    for j := 0 to High(BreakpointsArr) do
      begin
      // Requested breakpoints are marked above by setting their Id to -1
      if BreakpointsArr[j].Id > -1 then
        begin
        BreakpointId := BreakpointsArr[j].Id;
        if Assigned(BreakpointsArr[j].Breakpoint) then
          GetCurrentProcess(AController).RemoveBreak(BreakpointsArr[j].Breakpoint);
        TFpServerDbgController(AController).RemoveInternalBreakPoint(BreakpointId);
        end;
      end;

    Result := true;
    end
  else
    ReturnMessage := 'Failed to add breakpoint: No filename given';
end;

function TFpDebugThreadDABSetBreakpointsCommand.GetSetBreakpointsRequest: TDABSetBreakpointsRequest;
begin
  Result := FMessage as TDABSetBreakpointsRequest;
end;

class function TFpDebugThreadDABSetBreakpointsCommand.TextName: string;
begin
  Result := 'setBreakpoints';
end;

procedure TFpDebugThreadDABSetBreakpointsCommand.PreExecute(AController: TDCSCustomController; out DoQueueCommand: boolean);
begin
  inherited PreExecute(AController, DoQueueCommand);

  // Add to the quickpause list of expected commands. This way the QuickPause
  // thread will pause the debuggee shortlee when this breakpoint-command is not
  // executed within a reasonable time-frame. (The command will only be executed
  // when the debuggee is paused)
  // So it _will_ be executed.
  TFpDebugQuickPause.Instance.AddExpectedCommand(UID);
end;

{ TFpDebugThreadDABLaunchCommand }

procedure TFpDebugThreadDABLaunchCommand.PreExecute(AController: TDCSCustomController; out DoQueueCommand: boolean);
var
  Filename: string;
begin
  Filename := GetLaunchRequest.Arguments.&Program;
  if not FileExists(Filename) then
    begin
    DoQueueCommand := False;
    SendErrorResponse(1, 'exe_not_found', Format('File (program) [%s] not found', [filename]), True);
    end
  else if DirectoryExists(Filename) then
    begin
    DoQueueCommand := False;
    SendErrorResponse(1, 'exe_is_dir', Format('Provided filename (program) [%s] does refer to a directory and is not an executable.', [filename]), True);
    end
  else
    DoQueueCommand := True;
end;

class function TFpDebugThreadDABLaunchCommand.TextName: string;
begin
  Result := 'launch';
end;

function TFpDebugThreadDABLaunchCommand.GetLaunchRequest: TDABFpdLaunchRequest;
begin
  result := FMessage as TDABFpdLaunchRequest;
end;

const cStrategyDABToDbg: array[TDABFpdVariableSortStrategy] of TDbgVariableSortStrategy = (
    vstDefined,
    vstVisibility,
    vstInheritenceDepth,
    vstAlphabetic
  );
const cOrderDABToDbg: array[TDABFpdVariableSortOrder] of TDbgVariableSortOrder = (
    vsoAscending,
    vsoDescending
  );

procedure TFpDebugThreadDABLaunchCommand.FillControllerSettings(ASettings: TFpServerDbgControllerSettings; ALaunchArguments: TDABFpdLaunchRequestArguments);
var
  OrderVariablesBy: TDABFpdOrderByList;
  SortBy: TDbgVariableSortByArr;
  i: Integer;
begin
  OrderVariablesBy := ALaunchArguments.OrderVariablesBy;
  if OrderVariablesBy.Count > 0 then
    begin
    SetLength(SortBy, OrderVariablesBy.Count);
    for i := 0 to OrderVariablesBy.Count -1  do
      begin
      SortBy[i].Strategy := cStrategyDABToDbg[OrderVariablesBy[i].Strategy];
      SortBy[i].Order := cOrderDABToDbg[OrderVariablesBy[i].Order];
      end;
    ASettings.SortBy := SortBy;
    end;
end;

function TFpDebugThreadDABLaunchCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
var
  Params: TStringArray;
  i: Integer;
  Env: TDABLaunchRequestArgumentEnvironmentVariableList;
  Controller: TFpServerDbgController;
  DbgController: TDbgController;
  LaunchArguments: TDABFpdLaunchRequestArguments;
begin
  ReturnMessage := '';

  Controller := AController as TFpServerDbgController;
  DbgController := Controller.DbgController;
  LaunchArguments := GetLaunchRequest.Arguments;

  DbgController.ExecutableFilename:=LaunchArguments.&Program;
  DbgController.WorkingDirectory:=LaunchArguments.WorkingDirectory;

  DbgController.ForceNewConsoleWin:=LaunchArguments.ForceNewConsoleWin;
  DbgController.RedirectConsoleOutput:=not LaunchArguments.ForceNewConsoleWin;

  FillControllerSettings(Controller.Settings, LaunchArguments);

  Params := LaunchArguments.Parameters;
  DbgController.Params.Clear;
  for i := 0 to High(Params) do
    DbgController.Params.Add(Params[i]);

  Env := LaunchArguments.Environment;
  DbgController.Environment.Clear;
  for i := 0 to Env.Count -1 do
    DbgController.Environment.Values[Env[i].Name] := Env[i].Value;

  Result := DbgController.Run;
  if Result then
    Controller.RequestRunLoop
  else
    ReturnMessage := ErrorHandler.ErrorAsString(DbgController.LastError);
end;

{ TFpDebugThreadDABCommand }

constructor TFpDebugThreadDABCommand.Create(ASendByLisId: integer; ADABMessage: TDABProtocolMessage; ADistributor: TDCSDistributor);
begin
  inherited Create(ASendByLisId, ADABMessage.Seq, ADistributor);
  FMessage := ADABMessage;
end;

destructor TFpDebugThreadDABCommand.Destroy;
begin
  FMessage.Free;
  FResponseBody.Free;
  inherited Destroy;
end;

procedure TFpDebugThreadDABCommand.SendErrorResponse(
  Id: integer;
  Message: shortstring;
  Format: string;
  ShowUser: Boolean);
var
  Error: TDABError;
  Event: TfpDebugDABNotificationEvent;
begin
  Event := CreateExecutedCommandEvent(False, 'ReturnMessage', TfpDebugDABNotificationEvent) as TfpDebugDABNotificationEvent;
  try
    Event.Command := self.TextName;
    Event.NotificationType := ntFailedCommand;
    Event.Message := Message;
    Error := TDABError.Create;
    Error.Error.Format := Format;
    Error.Error.Id := Id;
    Error.Error.ShowUser := ShowUser;
    Event.Body := Error;

    FDistributor.SendEvent(Event);
  finally
    Event.Release;
  end;

end;

function TFpDebugThreadDABCommand.GetNotificationCommandEventClass: TDCSNotificationEventClass;
begin
  Result := TfpDebugDABNotificationEvent;
end;

function TFpDebugThreadDABCommand.CreateExecutedCommandEvent(Success: Boolean; ReturnMessage: string; NotificationClass: TDCSNotificationEventClass): TDCSNotificationEvent;
begin
  Result := inherited CreateExecutedCommandEvent(Success, ReturnMessage, NotificationClass);
  TfpDebugDABNotificationEvent(Result).Body := FResponseBody;
  FResponseBody := nil;
end;

function TFpDebugThreadDABCommand.ComposeDABStackFrameId(AController: TDCSCustomController; AThreadId: Integer; ACallStackIndex: Integer): Integer;
var
  Ref: TStackFrameReference;
begin
  Ref := TStackFrameReference.Create;
  Ref.ThreadId := AThreadId;
  ref.CallStackIndex := ACallStackIndex;
  Result := (AController as TFpServerDbgController).AddObjectReference(Ref);
end;

function TFpDebugThreadDABCommand.RetrieveStackFrameFromDABId(AController: TDCSCustomController; ADABStackFrameID: Integer; out AThreadId, ACallStackIndex: Integer): Boolean;
var
  Ref: TStackFrameReference;
  Obj: TObject;
begin
  AThreadId := 0;
  ACallStackIndex := 0;
  Result := False;
  Obj := (AController as TFpServerDbgController).GetObjectByReference(ADABStackFrameID);
  if Assigned(Obj) and (Obj is TStackFrameReference) then
    begin
    Ref := TStackFrameReference(Obj);
    AThreadId := Ref.ThreadId;
    ACallStackIndex := Ref.CallStackIndex;
    Result := True;
    end;
end;

function TFpDebugThreadDABCommand.GetCurrentProcess(AController: TDCSCustomController): TDbgProcess;
begin
  if Assigned(TFpServerDbgController(AController).DbgController) then
    Result := TFpServerDbgController(AController).DbgController.CurrentProcess
  else
    Result := nil;
end;

procedure TFpDebugThreadDABCommand.VariablesToDABVariable(AController: TFpServerDbgController; Variables: TDbgVariableList; DABVariables: TDABVariableList);
var
  i: Integer;
  DABVariable: TDABVariable;
  Variable: TDbgVariable;
  CachedVariableReference: TCachedVariableReference;
  AdditionalInfoReference: TAdditionalInfoReference;
  FunctionCallReference: TFunctionCallReference;
begin
  for i := 0 to Variables.Count -1 do
    begin
    DABVariable := TDABVariable.Create;
    Variable := Variables[i];
    DABVariable.Name := Variable.Name;
    if Variable.StructureName<>'' then
      DABVariable.Name := Variable.StructureName + '.' + DABVariable.Name;
    DABVariable.Value := Variable.Value;
    DABVariable.&Type := Variable.&Type;
    if eifHasChildren in Variable.ExtraInformationFlags then
      begin
      if eifChildrenAreIndexed in Variable.ExtraInformationFlags then
        DABVariable.IndexedVariables := Variable.ChildCount;
      CachedVariableReference := TCachedVariableReference.Create;
      CachedVariableReference.ChildrenReference := Variable.ChildrenReference;
      DABVariable.VariablesReference := AController.AddObjectReference(CachedVariableReference);
      end
    else if eifHasAdditionalInfo in Variable.ExtraInformationFlags then
      begin
      AdditionalInfoReference := TAdditionalInfoReference.Create;
      AdditionalInfoReference.AdditionalInfoReference := Variable.AdditionalInfoReference;
      DABVariable.VariablesReference := AController.AddObjectReference(AdditionalInfoReference);
      end
    else if eifCallableFunction in Variable.ExtraInformationFlags then
      begin
      FunctionCallReference := TFunctionCallReference.Create;
      FunctionCallReference.FunctionReference := Variable.ChildrenReference;
      FunctionCallReference.Scope := AController.FindScope(ThreadId, 0);
      DABVariable.VariablesReference := AController.AddObjectReference(FunctionCallReference);
      end;

    FillVariablePresentationHint(Variable, DABVariable.PresentationHint);
    DABVariables.Add(DABVariable);
    end;
end;

const
  cVisibilityToString: array[TDbgVisibility] of string =
    ('', 'private', 'protected', 'public');

procedure TFpDebugThreadDABCommand.FillVariablePresentationHint(AVariable: TDbgVariable; PresentationHint: TDABVariablePresentationHint);
begin
  if pos('$', AVariable.Name) > 0 then
    // Virtual indicates that this variable is 'artificial'. Not that it is
    // 'virtual' as it is defined in the Pascal-language.
    PresentationHint.Kind := 'virtual';
  if eifAdditionalInfo in AVariable.ExtraInformationFlags then
    PresentationHint.Kind := 'virtual';
  PresentationHint.Visibility := cVisibilityToString[AVariable.Visibility];
end;

{ TfpDebugDABNotificationEvent }

destructor TfpDebugDABNotificationEvent.Destroy;
begin
  FBody.Free;
  inherited Destroy;
end;

{ TFpDebugThreadDABInitializeCommand }

class function TFpDebugThreadDABInitializeCommand.TextName: string;
begin
  Result := 'dabInitialize'
end;

procedure TFpDebugThreadDABInitializeCommand.PreExecute(AController: TDCSCustomController; out DoQueueCommand: boolean);
var
  Event: TfpDebugDABNotificationEvent;
  Capabilities: TDabCapabilities;
begin
  DoQueueCommand := False;

  Event := CreateExecutedCommandEvent(True, 'ReturnMessage', TfpDebugDABNotificationEvent) as TfpDebugDABNotificationEvent;
  try
    Event.Command := 'initialize';
    Capabilities := TDabCapabilities.Create;
    Capabilities.SupportsConfigurationDoneRequest := True;
    Capabilities.SupportsSetVariable := True;
    Event.Body := Capabilities;
    FDistributor.SendEvent(Event);
  finally
    Event.Release;
  end;
end;

{ TFpDebugThreadDABSetVariableCommand }

function TFpDebugThreadDABSetVariableCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
var
  CallStackIndex: Integer;
  Thread: TDbgThread;
  ResponseBody: TDABSetVariableResponse;
  Obj: TObject;
  Variables: TDbgVariableList;
  Controller: TFpServerDbgController;
  Value: TFpValue;
  Variable: TDbgVariable;
  BooleanValue: Boolean;
  Scope: TFpDbgSymbolScope;
  CardinalValue: QWord;
  DWordValue: DWord;
  Int64Value: int64;
  CharValue: Char;
  StrValue: string;
  ValueType, EnumSymbol: TFpSymbol;
begin
  Result := False;

  Controller := TFpServerDbgController(AController);
  Obj := Controller.GetObjectByReference(GetSetVariableRequest.Arguments.VariablesReference);
  if not Assigned(Obj) then
    begin
    ReturnMessage := 'Invalid (unknown) variables-reference.';
    Exit;
    end;

  if Obj is TLocalsReference then
    begin
    Scope := TLocalsReference(Obj).Scope;
    if Scope.LocationContext.StackFrame>0 then
      begin
      ReturnMessage := 'It is only possible to set variables of the current stack frame.';
      Exit;
      end;

    Value := Scope.FindSymbol(GetSetVariableRequest.Arguments.Name);
    try
      if not Assigned(Value) then
        begin
        ReturnMessage := Format('Local variable [%s] not found.', [GetSetVariableRequest.Arguments.Name]);
        Exit;
        end;

      StrValue := GetSetVariableRequest.Arguments.Value;

      case Value.Kind of
        skCardinal:
          begin
          if not TryStrToQWord(StrValue, CardinalValue) then
            begin
            ReturnMessage := Format('[%s] is not a valid cardinal value.', [GetSetVariableRequest.Arguments.Value]);
            Exit
            end;
          Value.AsCardinal := CardinalValue;
          end;
        skInteger:
          begin
          if not TryStrToInt64(StrValue, Int64Value) then
            begin
            ReturnMessage := Format('[%s] is not a valid integer value.', [GetSetVariableRequest.Arguments.Value]);
            Exit
            end;
          Value.AsInteger := Int64Value;
          end;
        skBoolean:
          begin
          if not TryStrToBool(StrValue, BooleanValue) then
            begin
            ReturnMessage := Format('[%s] is not a valid boolean value.', [GetSetVariableRequest.Arguments.Value]);
            Exit
            end;
          Value.AsBool := BooleanValue;
          end;
        skChar:
          begin
          if StrValue='' then
            begin
            ReturnMessage := 'Empty string is not a valid char value';
            Exit
            end;
          if StrValue[1]='#' then
            begin
            if not TryStrToDWord(Copy(StrValue, 2, 10), DWordValue) or (DWordValue>255) then
              begin
              ReturnMessage := Format('[%s] is not a valid char value', [StrValue]);
              Exit;
              end;
            CharValue := Char(DWordValue);
            end
          else
            begin
            StrValue := AnsiDequotedStr(StrValue, '''');
            if length(StrValue)>1 then
              begin
              ReturnMessage := Format('[%s] is not a valid char value',[StrValue]);
              Exit;
              end;
            CharValue := StrValue[1];
            end;
          Value.AsCardinal := Ord(CharValue);
          end;
        skEnum:
          begin
          if TryStrToDWord(StrValue, DWordValue) then
            Value.AsCardinal := DWordValue
          else
            value.AsString := StrValue;
          if IsError(Value.LastError) then
            begin
            ReturnMessage := dbgs(Value.LastError);
            Exit;
            end;
          end;
        skClass:
          begin
          if not TryStrToQWord(StrValue, CardinalValue) then
            begin
            ReturnMessage := Format('[%s] is not a valid class (pointer) value', [StrValue]);
            Exit;
            end;
          Value.AsCardinal := CardinalValue;
          end;
        skPointer:
          begin
          if not TryStrToQWord(StrValue, CardinalValue) then
            begin
            ReturnMessage := Format('[%s] is not a valid pointer value', [StrValue]);
            Exit;
            end;
          Value.AsCardinal := CardinalValue;
          end
      else
        begin
        ReturnMessage := 'Variables of this kind can not be set.';
        Exit
        end;
      end;

      if IsError(Value.LastError) then
        begin
        ReturnMessage := 'Failed to set variable value: ' + Dbgs(Value.LastError);
        Exit;
        end
      else
        begin
        ResponseBody := TDABSetVariableResponse.Create;
        try
          Variable := TFpServerDbgController(AController).VariableCache.ObtainVariableForValue(Value, 0, nil);
          try
            ResponseBody.Value := Variable.Value;
            ResponseBody.&Type := Variable.&Type;
            // ToDo: Fill childs etc? But as long only setting simple values
            // is supported...
          finally
            Variable.Free;
          end;
          FResponseBody := ResponseBody;
          ResponseBody := nil;
          Result := True;
        finally
          ResponseBody.Free;
        end;
        end;
    finally
      Value.ReleaseReference;
    end;
    end;
end;

class function TFpDebugThreadDABSetVariableCommand.TextName: string;
begin
  Result := 'setVariable';
end;

function TFpDebugThreadDABSetVariableCommand.GetSetVariableRequest: TDABSetVariableRequest;
begin
  Result := FMessage as TDABSetVariableRequest;
end;

end.

