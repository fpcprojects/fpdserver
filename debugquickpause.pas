unit DebugQuickPause;

{$mode ObjFPC}{$H+}

interface

uses
  Classes,
  SysUtils,
  dcsHandler,
  dcsListenerThread,
  DebugThread;

type
  // Sometimes there is a debug-command that must be executed immediately, but
  // can only  once the debuggee has stopped. It is however very difficult to
  // stop the debuggee, execute a command and continue again. Without anything
  // happening in between. (First the debuggee is requested to stop, then when a
  // a message is received that the debuggee has been stopped, it is *assumed*
  // that this is because of the request to stop, but there is no guarantee. We
  // do, however, continue the debuggee, which might be incorrect.)
  //
  // It is even more complex when a client sends multiple of these commands in
  // a very short period. (Multiple stop request may be given first, then the
  // debuggee stops. But how many times should we let the debuggee continue
  // again?)
  //
  // This class is here to solve these kind of problems. When a command that
  // needs the debuggee to stop is send, it can be added to a list of expected
  // commands. In the background there is a thread running that removes those
  // commands from the list as soon as they have been executed.
  //
  // When after a time-period an expected command is still not executed, a
  // TForcePauseCommand is issued and the debuggee is paused and immediately
  // thereafter continued again. During this short pause, all already queued
  // commands are executed, and thus removed from the list of expected commands.

  { TFpDebugQuickPause }

  TFpDebugQuickPause = class(TObject)
  private type
    TExpectedCommand = record
      // The UID of an expected command, used to match with ntFailedCommand or
      // ntExecutedCommand events.
      UID: variant;
      // Tickcount at the moment the command was added. Used to determine the
      // timeout.
      Tick: Int64;
    end;
  private const
    // In 'ticks', so milliseconds.
    FCommandTimeout = 100;
  private
    FDistributor: TDCSDistributor;
    FController: TFpServerDbgController;
    FListenerThread: TDCSListenerThread;
    // List of expected commands. May only be accessed from within the
    // FListenerThread
    FExpectedCommands: array of TExpectedCommand;
    // Keeps track of the amount of expected commands at the end of the last
    // call to OnIdle. This so it is possible to detect if commands are being
    // processed. (ie: the debuggee is already paused)
    FPriorAmountOfExpectedCommands: Integer;
    // Listens for new commands to be added to FExpectedCommands or for events
    // that indicate that certain commands have been executed and should be
    // removed from the list. (Runs in the FListenerThread)
    procedure ListenerThreadOnEvent(Event: TDCSEvent);
    // Periodic check to see if the debuggee should be paused. (Runs in the
    // FListenerThread)
    procedure ListenerThreadOnIdle;
    // Sets some basic administration (Runs in the FListenerThread)
    procedure ListenerThreadInitialized(Sender: TDCSListenerThread);
  protected
    class var FInstance: TFpDebugQuickPause;
    class destructor Destroy;
  public
    constructor Create(ADistributor: TDCSDistributor; AController: TFpServerDbgController);
    destructor Destroy; override;

    // Notify that a command has been queued, for which the debuggee has to be
    // paused. When the command is not executed within a timeout, the debuggee
    // is forced to pause so it can execute the command.
    // Is ideally called from within the PreExecute of a command.
    procedure AddExpectedCommand(AnUID: variant);
    class function InitializeInstance(ADistributor: TDCSDistributor; AController: TFpServerDbgController): TFpDebugQuickPause; static;
    class procedure ReleaseInstance;
    class property Instance: TFpDebugQuickPause read FInstance;
  end;

implementation

type

  { TAddExpectedCommandEvent }

  TAddExpectedCommandEvent = class(TDCSEvent)
  private
    FAddTickCount: Int64;
  protected
    function GetEventType: TDCSEventType; override;
  public
    constructor Create(ExpectUID: Variant; ListenerId: integer); overload;
    property AddTickCount: Int64 read FAddTickCount;
  end;

  { TForcePauseCommand }

  TForcePauseCommand = class(TFpDebugThreadCommand)
  public
    procedure PreExecute(AController: TDCSCustomController; out DoQueueCommand: boolean); override;
    procedure Execute(AController: TDCSCustomController); override;
    function DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean; override;
    class function TextName: string; override;
  end;

{ TForcepauseCommand }

procedure TForcePauseCommand.PreExecute(AController: TDCSCustomController; out DoQueueCommand: boolean);
begin
  inherited PreExecute(AController, DoQueueCommand);
  TFpServerDbgController(AController).ForcePaused;
end;

procedure TForcePauseCommand.Execute(AController: TDCSCustomController);
var
  ReturnMessage: string;
begin
  // This way, no events (ntExecutedCommand, ntReceivedCommand) are sent.
  DoExecute(AController, ReturnMessage);
end;

function TForcePauseCommand.DoExecute(AController: TDCSCustomController; out ReturnMessage: string): Boolean;
begin
  // Do nothing special, but do request the debuggee to continue after all
  // commands have been processed. This command is executed after all the 'expected'
  // commands in the queue, because those commands were queued earlier.
  Result:=inherited DoExecute(AController, ReturnMessage);
  TFpServerDbgController(AController).RequestRunLoop;
end;

class function TForcePauseCommand.TextName: string;
begin
  Result:='quickPause';
end;

{ TAddExpectedEventEvent }

function TAddExpectedCommandEvent.GetEventType: TDCSEventType;
begin
  Result := dcsetEvent;
end;

constructor TAddExpectedCommandEvent.Create(ExpectUID: Variant; ListenerId: integer);
begin
  inherited Create;
  UID := ExpectUID;
  LisId := ListenerId;
  FAddTickCount := GetTickCount64;
end;

{ TFpDebugQuickPause }

procedure TFpDebugQuickPause.ListenerThreadOnIdle;
var
  Lowest: Int64;
  i: Integer;
  CurrentTickCount: QWord;
begin
  CurrentTickCount := GetTickCount64;
  Lowest := High(Int64);
  if Length(FExpectedCommands) > 0 then
    begin
    // When there are less expected-commands than after the last OnIdle check,
    // commands are being processed and thus there is no reason pause the
    // debugee
    if Length(FExpectedCommands) >= FPriorAmountOfExpectedCommands then
      begin
      for i := 0 to High(FExpectedCommands) do
        begin
        if FExpectedCommands[i].Tick < Lowest then
          Lowest := FExpectedCommands[i].Tick;
        end;
      if (Lowest + FCommandTimeout) < CurrentTickCount then
        begin
        if TFpServerDbgController(FController).RequestRunLoopRunning then
          begin
          // We have to be very carefull with calling the TForcePauseCommand, because
          // it will resume the debugee in all cases. Also when there was no reason
          // to pause the application. (ie: the application was not running)
          // So to be as sure as possible that the debugee is running, RequestRunLoopRunning
          // is checked.
          FDistributor.QueueCommand(TForcePauseCommand.Create(FListenerThread.GetListenerId, CurrentTickCount, FDistributor));
          // This should always work, so we can clear the list
          FExpectedCommands := [];
          end;
        end;
      end;
    end;
  FPriorAmountOfExpectedCommands := Length(FExpectedCommands);
  // Todo: adapt timeout. When FExpectedCommands is empty, there is no
  // reason to call OnIdle.
end;

class function TFpDebugQuickPause.InitializeInstance(ADistributor: TDCSDistributor; AController: TFpServerDbgController): TFpDebugQuickPause;
begin
  Assert(not Assigned(FInstance));
  FInstance := TFpDebugQuickPause.Create(ADistributor, AController);
end;

class procedure TFpDebugQuickPause.ReleaseInstance;
begin
  FInstance.Free;
  FInstance := nil;
end;

class destructor TFpDebugQuickPause.Destroy;
begin
  FInstance.Free;
end;

procedure TFpDebugQuickPause.ListenerThreadInitialized(Sender: TDCSListenerThread);
begin
  // Limit the events we will get. This is not water-proof, especially not during
  // initialization.
  FDistributor.SetEventsForListener(FListenerThread, reNone);
  FDistributor.SetLogEventsForListener(FListenerThread, reNone, []);
  FDistributor.SetNotificationEventsForListener(FListenerThread, reAll, [ntFailedCommand, ntInvalidCommand, ntExecutedCommand]);
end;

procedure TFpDebugQuickPause.ListenerThreadOnEvent(Event: TDCSEvent);
var
  ExpectedCommand: TExpectedCommand;
  i: Integer;
begin
  if Event is TAddExpectedCommandEvent then
    begin
    ExpectedCommand.Tick := TAddExpectedCommandEvent(Event).AddTickCount;
    ExpectedCommand.UID := Event.UID;
    FExpectedCommands := Concat(FExpectedCommands, [ExpectedCommand]);
    end
  else if (Event is TDCSNotificationEvent) and (TDCSNotificationEvent(Event).NotificationType in [ntExecutedCommand, ntInvalidCommand, ntFailedCommand]) then
    begin
    for i := 0 to High(FExpectedCommands) do
      begin
      if Event.UID=FExpectedCommands[i].UID then
        begin
        Delete(FExpectedCommands, i, 1);
        Break;
        end;
      end;
    end;
end;

constructor TFpDebugQuickPause.Create(ADistributor: TDCSDistributor; AController: TFpServerDbgController);
begin
  FDistributor := ADistributor;
  FController := AController;
  FListenerThread := TDCSListenerThread.Create(FDistributor, 100, 'quickpause', @ListenerThreadOnIdle, @ListenerThreadOnEvent, @ListenerThreadInitialized);
  FDistributor.AddListener(FListenerThread);
  // The only 'normal' events that are received, are not distributed through the
  // distributer (the debugger) but via the side-channel using AddExpectedCommand.
end;

destructor TFpDebugQuickPause.Destroy;
begin
  FListenerThread.ForceTerminate;
  FListenerThread.WaitFor;
  FListenerThread.Free;
  inherited Destroy;
end;

procedure TFpDebugQuickPause.AddExpectedCommand(AnUID: variant);
var
  Event: TAddExpectedCommandEvent;
begin
  Event := TAddExpectedCommandEvent.Create(AnUID, FListenerThread.GetListenerId);
  try
    FListenerThread.SendEvent(Event);
  finally
    Event.Release;
  end;
end;

end.

