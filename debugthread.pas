unit debugthread;

{$mode objfpc}{$H+}
{$INTERFACES CORBA}

interface

uses
  Classes,
  SysUtils,
  Generics.Collections,
  FPDbgController,
  FpDbgUtil,
  FpDbgDwarf,
  FpDbgDwarfFreePascal,
  FpDbgDwarfDataClasses,
  FpDbgInfo,
  FpDbgHardcodedFreepascalInfo,
  FpdMemoryTools,
  DbgIntfBaseTypes,
  LazDebuggerIntf,
  DbgIntfDebuggerBase,
  FpPascalBuilder,
  FPDbgVariables,
  dcsHandler,
  fpjson,
  FpDbgClasses;

type
  TFpDebugEventCallStackEntry = record
    AnAddress: TDBGPtr;
    FrameAdress: TDBGPtr;
    SourceFile: string;
    FunctionName: string;
    Line: integer;
  end;
  TFpDebugEventWatchEntry = record
    TextValue: string;
    Expression: string;
    NumValue: TDBGPtr;
    Size: byte;
  end;

  TFpDebugEventCallStackEntryArray = array of TFpDebugEventCallStackEntry;
  TFpDebugEventDisassemblerEntryArray = array of TDisassemblerEntry;
  TFpDebugEventWatchEntryArray = array of TFpDebugEventWatchEntry;

  // This record is used to pass debugging-events. Not every field is applicable for each type of event.

  { TFpDebugEvent }

  TFpDebugEvent = class(TDCSEvent)
  protected
    function GetEventType: TDCSEventType; override;
    function GetIsThreadSafe: Boolean; override;
  public
    SendByConnectionIdentifier: integer;
    EventName: string;
    Message: shortstring;
    InstructionPointerRegValue: TDBGPtr;
    ExitCode: DWord;
    AnUID: variant;
    BreakpointServerIdr: Integer;
    ThreadID: Integer;
    // Depends on the type of event. For example: ModuleId, BreakpointId
    CustomId: Integer;
    ModuleSymbolStatus: string;
    Verified: Boolean;
    Line: Integer;
  end;

  { TFpDebugNotificationEvent }

  TFpDebugNotificationEvent = class(TDCSNotificationEvent)
    EventName: string;
    BreakpointServerIdr: Integer;
    LocationRec: TDBGLocationRec;
    Validity: TDebuggerDataState;
    Addr1: TDBGPtr;
    Addr2: TDBGPtr;
    Addr3: TDBGPtr;
    StackEntryArray: TFpDebugEventCallStackEntryArray;
    DisassemblerEntryArray: TFpDebugEventDisassemblerEntryArray;
    WatchEntryArray: TFpDebugEventWatchEntryArray;
  end;

  { TFpServerDbgControllerSettings }

  TFpServerDbgControllerSettings = class
  private
    FSortBy: TDbgVariableSortByArr;
  public
    property SortBy: TDbgVariableSortByArr read FSortBy write FSortBy;
  end;

  TFpDebugThread = class;

  TFpServerBreakpointLink = record
    Breakpoint: TFpInternalBreakpoint;
    Line: Integer;
    Id: Integer;
  end;

  TFpserverBreakpointArray = array of TFpServerBreakpointLink;

  { TFpServerDbgController }

  TFpServerDbgController = class(TDCSCustomController)
  private type
    // This record is used to map FpDebug breakpoints (Internal breakpoints or
    // TFpDbgBreakpoints) to breakpoints as how they are defined in FPDServer
    TBreakpoint = record
      Source: string;
      Line: Integer;
      // When Breakpoint is nil, this is a 'pending' breakpoint. As soon as a
      // Module is loaded, the debugger retries to set the pending breakpoints.
      Breakpoint: TFpInternalBreakpoint;
    end;
    TBreakPointIdMap = specialize THashMap<Integer, TBreakpoint>;
    TReferenceMap = specialize TObjectHashMap<Integer, TObject>;
    TFpContextIndex = record
      AThreadId: Integer;
      AStackFrame: Integer;
    end;
    TFpScopeCache = specialize THashMap<TFpContextIndex, TFpDbgSymbolScope>;
  private
    FBreakPointIdCnt: Integer;
    FBreakPointIdMap: TBreakPointIdMap;
    FDbgController: TDbgController;
    FReferenceMap: TReferenceMap;
    FReferenceIndex: Integer;
    FRaiseExceptionBreakPoint: TFpDbgBreakpoint;

    FMemConverter: TFpDbgMemConvertorLittleEndian;
    FMemReader: TDbgMemReader;
    FMemManager: TFpDbgMemManager;

    FRunLoopRequested: Boolean;

    FConsoleOutputThread: TThread;

    FPrettyPrinter: TFpPascalPrettyPrinter;
    FVariableBuilder: TDbgVariableBuilder;
    FVariableCache: TDbgVariableCache;

    FForcePausedRequested: Boolean;

    FProcessLoopRunning: Cardinal;

    FScopeCache: TFpScopeCache;
    FSettings: TFpServerDbgControllerSettings;
    procedure FreeConsoleOutputThread;
    procedure ClearInternalBreakPointList;
    function GetPrettyPrinter: TFpPascalPrettyPrinter;
    function GetVariableBuilder: TDbgVariableBuilder;
    procedure DoScopeCacheValueChanged(ASender: TObject; constref AItem: TFpDbgSymbolScope; AAction: TCollectionNotification);
    function GetRequestRunLoopRunning: Boolean;
    procedure SendLibraryEvent(AnEventName: string; ALibrary: TDbgLibrary);
    function GetSettings: TFpServerDbgControllerSettings;

    // Loop through all breakpoints that are not set yet (pending breakpoints)
    // and re-try to set them. Typically used after a dynamic library has been
    // loaded.
    procedure RetrySettingAllBreakpoints;
    // Unset all breakpoints that are bound to a specific library
    procedure UnsetAllBreakpointsBoundToLibrary(ALibrary: TDbgLibrary);
  public
    constructor Create(ADistributor: TDCSDistributor); override;
    destructor Destroy; override;

    procedure Init; override;
    procedure OnIdle; override;

    function AddObjectReference(const AnObject: TObject): Integer;
    function GetObjectByReference(const AReference: Integer): TObject;
    procedure ClearObjectReferences;

    // Handlers for the FController-events
    procedure FControllerHitBreakpointEvent(var continue: boolean; const Breakpoint: TFpDbgBreakpoint; AnEventType: TFPDEvent; AMoreHitEventsPending: Boolean);
    procedure FControllerProcessExitEvent(ExitCode: DWord);
    procedure FControllerCreateProcessEvent(var continue: boolean);
    procedure FControllerExceptionEvent(var continue: boolean; const ExceptionClass, ExceptionMessage: string);
    procedure FControllerLibraryLoadedEvent(var continue: boolean; ALibraryArray: TDbgLibraryArr);
    procedure FControllerLibraryUnLoadedEvent(var continue: boolean; ALibraryArray: TDbgLibraryArr);

    // Find scopes and cache them.
    function  FindScope(AThreadId, AStackFrame: Integer): TFpDbgSymbolScope;

    // Force the debugee to get into a paused state, but do not send any events
    // out.
    // Usefull when a command has to be executed within the debug-thread
    // immediately. (Note however that the debugee *will be* paused)
    procedure ForcePaused;

    procedure RequestRunLoop;

    procedure SetPascalExceptionBreakpoint;
    procedure ObtainPascalExceptionInformation(out AnExceptionLocation: TDBGPtr; out AnExceptionClass, AnExceptionMessage: string);

    procedure SetInternalBreakPointInfo(ABrkPoint: TFpInternalBreakpoint; ASource: string; ALine: Integer; var AnId: Integer);
    function GetInternalBreakPointFromId(AnId: Integer): TFpDbgBreakpoint;
    function GetIdFromInternalBreakPoint(ABrkPoint: TFpDbgBreakpoint): Integer;
    procedure RemoveInternalBreakPoint(AnId: Integer);
    function GetBreakpointsForSource(ASource: string): TFpserverBreakpointArray;
    //procedure RemoveInternalBreakPoint(ABrkPoint: TFpDbgBreakpoint);
    property DbgController: TDbgController read FDbgController;
    property PrettyPrinter: TFpPascalPrettyPrinter read GetPrettyPrinter;
    property VariableBuilder: TDbgVariableBuilder read GetVariableBuilder;
    property VariableCache: TDbgVariableCache read FVariableCache;
    property MemConverter: TFpDbgMemConvertorLittleEndian read FMemConverter;
    property MemReader: TDbgMemReader read FMemReader;

    // Thread-safe way of determing if ProcessLoop is running. Be carefull, though.
    // ProcessLoop itself is not 'atomic', so use with care.
    property RequestRunLoopRunning: Boolean read GetRequestRunLoopRunning;

    property Settings: TFpServerDbgControllerSettings read GetSettings;
  end;

  { TFpDebugThreadCommand }

  // The base class for all commands that can be send to the debug-thread.

  TFpDebugThreadCommand = class(TDCSThreadCommand)
  protected
    function GetNotificationCommandEventClass: TDCSNotificationEventClass; override;
  public
    constructor Create(ASendByLisId: integer; AnUID: variant; ADistributor: TDCSDistributor); override;
  end;
  TFpDebugThreadCommandClass = class of TFpDebugThreadCommand;

  { TFpDebugThread }

  TFpDebugThread = class(TDCSHandlerThread)
  end;

implementation

uses
  DebugQuickPause;

type

  { TFpDbgMemReader }

  TFpDbgMemReader = class(TDbgMemReader)
    private
      FDbgController: TFpServerDbgController;
    protected
      function GetDbgProcess: TDbgProcess; override;
    public
      constructor create(ADbgcontroller: TFpServerDbgController);
  end;

  { TFpWaitForConsoleOutputThread }

  TFpWaitForConsoleOutputThread = class(TThread)
  private
    FDbgController: TDbgController;
    FDistributor: TDCSDistributor;
  public
    constructor Create(ADbgController: TDbgController; ADistributor: TDCSDistributor);
    procedure Execute; override;
  end;

{ TFpDebugThreadCommand }

function TFpDebugThreadCommand.GetNotificationCommandEventClass: TDCSNotificationEventClass;
begin
  Result := TFpDebugNotificationEvent;
end;

constructor TFpDebugThreadCommand.Create(ASendByLisId: integer; AnUID: variant; ADistributor: TDCSDistributor);
begin
  inherited Create(ASendByLisId, AnUID, ADistributor);
  FCallOnIdle := True;
end;

{ TFpDebugEvent }

function TFpDebugEvent.GetEventType: TDCSEventType;
begin
  Result := dcsetEvent;
end;

function TFpDebugEvent.GetIsThreadSafe: Boolean;
begin
  Result := True;
end;

{ TFpServerDbgController }

constructor TFpServerDbgController.Create(ADistributor: TDCSDistributor);
begin
  FMemReader := TFpDbgMemReader.Create(self);
  FMemConverter := TFpDbgMemConvertorLittleEndian.Create;
  FMemManager := TFpDbgMemManager.Create(FMemReader, FMemConverter);

  FDbgController := TDbgController.Create(FMemManager);
  FBreakPointIdMap := TBreakPointIdMap.Create;
  FReferenceMap := TReferenceMap.Create([doOwnsValues]);
  FVariableBuilder := TDbgVariableBuilder.Create;
  FVariableCache := TDbgVariableCache.Create(FVariableBuilder);
  FScopeCache := TFpScopeCache.Create;
  FScopeCache.OnValueNotify := @DoScopeCacheValueChanged;

  TFpDebugQuickPause.InitializeInstance(ADistributor, Self);

  inherited Create(ADistributor);
end;

destructor TFpServerDbgController.Destroy;
begin
  inherited Destroy;

  FSettings.Free;

  FScopeCache.Free;
  FVariableCache.Free;
  FVariableBuilder.Free;
  FReferenceMap.Free;
  FBreakPointIdMap.Free;
  FMemManager.Free;
  FMemConverter.Free;
  FMemReader.Free;
  FreeConsoleOutputThread;
  TFpDebugQuickPause.ReleaseInstance;
  FPrettyPrinter.Free;
  FDbgController.Free;
end;

procedure TFpServerDbgController.SetInternalBreakPointInfo(ABrkPoint: TFpInternalBreakpoint; ASource: string; ALine: Integer; var AnId: Integer);
var
  BreakpointRec: TBreakpoint;
begin
  if AnId = -1 then
    begin
    Inc(FBreakPointIdCnt);
    AnId := FBreakPointIdCnt;
    end;
  BreakpointRec.Breakpoint := ABrkPoint;
  BreakpointRec.Source := ASource;
  BreakpointRec.Line := ALine;
  FBreakPointIdMap.AddOrSetValue(AnId, BreakpointRec);
end;

function TFpServerDbgController.GetInternalBreakPointFromId(AnId: Integer): TFpDbgBreakpoint;
begin
  if FBreakPointIdMap.ContainsKey(AnId) then
    Result := FBreakPointIdMap.Items[AnId].Breakpoint
  else
    Result := nil;
end;

function TFpServerDbgController.GetIdFromInternalBreakPoint(ABrkPoint: TFpDbgBreakpoint): Integer;
var
  Pair: specialize TPair<Integer, TBreakpoint>;
begin
  for Pair in FBreakPointIdMap do
    if Pair.Value.Breakpoint=ABrkPoint then
      begin
      Result := Pair.Key;
      Exit;
      end;
  Result := 0;
end;

procedure TFpServerDbgController.RemoveInternalBreakPoint(AnId: Integer);
var
  Breakpoint: TFpDbgBreakpoint;
begin
  Breakpoint := FBreakPointIdMap.Items[AnId].Breakpoint;
  if Assigned(Breakpoint) then
    Breakpoint.free;
  FBreakPointIdMap.Remove(AnId);
end;

procedure TFpServerDbgController.ClearInternalBreakPointList;
var
  Pair: specialize TPair<Integer, TBreakpoint>;
begin
  for Pair in FBreakPointIdMap do
    if Assigned(Pair.Value.Breakpoint) then
      Pair.Value.Breakpoint.Free;
  FBreakPointIdMap.Clear;
end;

procedure TFpServerDbgController.Init;
begin
  inherited Init;
  FDbgController.RedirectConsoleOutput:=true;
  FDbgController.OnCreateProcessEvent:=@FControllerCreateProcessEvent;
  FDbgController.OnProcessExitEvent:=@FControllerProcessExitEvent;
  FDbgController.OnHitBreakpointEvent:=@FControllerHitBreakpointEvent;
  FDbgController.OnExceptionEvent := @FControllerExceptionEvent;
  FDbgController.OnLibraryLoadedEvent := @FControllerLibraryLoadedEvent;
  FDbgController.OnLibraryUnloadedEvent := @FControllerLibraryUnLoadedEvent;
end;

constructor TFpWaitForConsoleOutputThread.Create(ADbgController: TDbgController; ADistributor: TDCSDistributor);
begin
  Inherited create(false);
  FDbgController := ADbgController;
  FDistributor := ADistributor;
end;

procedure TFpWaitForConsoleOutputThread.Execute;
var
  res: integer;
  AnEvent: TFpDebugEvent;
  s: string;
begin
  while not terminated do
  begin
    res := FDbgController.CurrentProcess.CheckForConsoleOutput(100);
    if res<0 then
      Terminate
    else if res>0 then
    begin
      s := FDbgController.CurrentProcess.GetConsoleOutput;
      if s <> '' then
      begin
        AnEvent:=TFpDebugEvent.Create;
        try
          AnEvent.LisId := GeneralEventListenerId;
          AnEvent.EventName:='ConsoleOutput';
          AnEvent.Message:=s;
          FDistributor.SendEvent(AnEvent);
        finally
          AnEvent.Release;
        end;
      end;
    end;
  end;
end;

{ TFpDbgMemReader }

function TFpDbgMemReader.GetDbgProcess: TDbgProcess;
begin
  result := FDbgController.FDbgController.CurrentProcess;
end;

constructor TFpDbgMemReader.create(ADbgcontroller: TFpServerDbgController);
begin
  Inherited Create;
  FDbgController:=ADbgcontroller;
end;

{ TFpDebugThread }

procedure TFpServerDbgController.SendLibraryEvent(AnEventName: string; ALibrary: TDbgLibrary);
var
  ADebugEvent: TFpDebugEvent;
begin
  ADebugEvent := TFpDebugEvent.Create;
  try
    ADebugEvent.EventName := AnEventName;
    ADebugEvent.Message := ALibrary.Name;
    ADebugEvent.ThreadID := ALibrary.ModuleHandle;
    if (ALibrary.SymbolTableInfo.HasInfo) and (ALibrary.DbgInfo.HasInfo) then
      ADebugEvent.ModuleSymbolStatus := 'Symbols and debuginfo loaded'
    else if (ALibrary.SymbolTableInfo.HasInfo) then
      ADebugEvent.ModuleSymbolStatus := 'Symbols loaded'
    else if (ALibrary.DbgInfo.HasInfo) then
      ADebugEvent.ModuleSymbolStatus := 'Debuginfo loaded'
    else
      ADebugEvent.ModuleSymbolStatus := 'Symbols and debuginfo not found';
    ADebugEvent.LisId := GeneralEventListenerId;
    FDistributor.SendEvent(ADebugEvent);
  finally
    ADebugEvent.Release;
  end;
end;

procedure TFpServerDbgController.FControllerLibraryLoadedEvent(var continue: boolean; ALibraryArray: TDbgLibraryArr);
var
  i: Integer;
begin
  for i := 0 to High(ALibraryArray) do
    SendLibraryEvent('LibraryLoaded', ALibraryArray[i]);
  continue:=True;

  RetrySettingAllBreakpoints;
end;

procedure TFpServerDbgController.FControllerLibraryUnLoadedEvent(var continue: boolean; ALibraryArray: TDbgLibraryArr);
var
  i: Integer;
begin
  for i := 0 to High(ALibraryArray) do
    begin
    SendLibraryEvent('LibraryUnLoaded', ALibraryArray[i]);
    UnsetAllBreakpointsBoundToLibrary(ALibraryArray[i])
    end;
  continue:=True;
end;

procedure TFpServerDbgController.FreeConsoleOutputThread;
var
  AThread: TFpWaitForConsoleOutputThread;
begin
  if assigned(FConsoleOutputThread) then
    begin
    AThread := TFpWaitForConsoleOutputThread(FConsoleOutputThread);
    FConsoleOutputThread := nil;
    AThread.Terminate;
    AThread.WaitFor;
    AThread.Free;
    end;
end;

procedure TFpServerDbgController.FControllerHitBreakpointEvent(var continue: boolean; const Breakpoint: TFpDbgBreakpoint; AnEventType: TFPDEvent; AMoreHitEventsPending: Boolean);
var
  ADebugEvent: TFpDebugEvent;
  AnId: Integer;
  AnExceptionLocation: TDBGPtr;
  AnExceptionMessage, AnExceptionClass: string;
begin
  if FForcePausedRequested and not Assigned(Breakpoint) and (AnEventType=deInternalContinue) then
    begin
    FForcePausedRequested := False;
    continue := False;
    Exit;
    end;

  ADebugEvent := TFpDebugEvent.Create;
  try
    if Assigned(Breakpoint) and (Breakpoint=FRaiseExceptionBreakPoint) then
      begin
      // This is a Pascal-exception
      ObtainPascalExceptionInformation(AnExceptionLocation, AnExceptionClass, AnExceptionMessage);
      ADebugEvent.EventName := 'Exception';
      ADebugEvent.InstructionPointerRegValue := AnExceptionLocation;
      ADebugEvent.ThreadID := FDbgController.CurrentThreadId;
      ADebugEvent.Message := AnExceptionClass + ': ' + AnExceptionMessage;
      end
    else
      begin
      ADebugEvent.EventName:='BreakPoint';
      ADebugEvent.InstructionPointerRegValue:=FDbgController.CurrentThread.GetInstructionPointerRegisterValue;
      if assigned(Breakpoint) then
        begin
        (* There may be several breakpoints at this address.
           For now sending the IP address allows the IDE to find the same breakpoint(s) as the fpdserver app.
        *)
        AnId := GetIdFromInternalBreakPoint(Breakpoint);
        ADebugEvent.BreakpointServerIdr := AnId;
        end;
      end;
    ADebugEvent.ThreadID := FDbgController.CurrentThreadId;
    ADebugEvent.LisId := GeneralEventListenerId;
    FDistributor.SendEvent(ADebugEvent);
  finally
    ADebugEvent.Release;
  end;
  continue:=false;
end;

procedure TFpServerDbgController.FControllerProcessExitEvent(ExitCode: DWord);
var
  ADebugEvent: TFpDebugEvent;
begin
  FreeAndNil(FRaiseExceptionBreakPoint);
  FreeConsoleOutputThread;
  ClearInternalBreakPointList;

  ADebugEvent := TFpDebugEvent.Create;
  try
    ADebugEvent.LisId := GeneralEventListenerId;
    ADebugEvent.EventName:='ExitProcess';
    ADebugEvent.ExitCode:=ExitCode;
    ADebugEvent.InstructionPointerRegValue:=FDbgController.CurrentThread.GetInstructionPointerRegisterValue;
    FDistributor.SendEvent(ADebugEvent);
  finally
    ADebugEvent.Release;
  end;
end;

procedure TFpServerDbgController.FControllerCreateProcessEvent(var continue: boolean);
var
  ADebugEvent: TFpDebugEvent;
begin
  ADebugEvent := TFpDebugEvent.Create;
  try
    ADebugEvent.EventName:='CreateProcess';
    ADebugEvent.LisId := GeneralEventListenerId;
    ADebugEvent.ThreadID := FDbgController.CurrentThreadId;
    ADebugEvent.InstructionPointerRegValue:=FDbgController.CurrentThread.GetInstructionPointerRegisterValue;

    FDistributor.SendEvent(ADebugEvent);
  finally
    ADebugEvent.Release;
  end;


  if FDbgController.RedirectConsoleOutput then
    FConsoleOutputThread := TFpWaitForConsoleOutputThread.Create(FDbgController, FDistributor);
  continue:=false;
end;

procedure TFpServerDbgController.OnIdle;
begin
  inherited OnIdle;
  while FRunLoopRequested do
    begin
    ClearObjectReferences;
    FVariableCache.InvalidateCache;
    InterLockedExchange(FProcessLoopRunning, 1);
    FDbgController.ProcessLoop;
    InterLockedExchange(FProcessLoopRunning, 0);
    FDbgController.SendEvents(FRunLoopRequested);
    end;
end;

procedure TFpServerDbgController.RequestRunLoop;
begin
  FRunLoopRequested := True;
end;

function TFpServerDbgController.GetPrettyPrinter: TFpPascalPrettyPrinter;
begin
  if not Assigned(FPrettyPrinter) then
    FPrettyPrinter := TFpPascalPrettyPrinter.Create(SizeOf(Pointer));
  Result := FPrettyPrinter;
end;

function TFpServerDbgController.AddObjectReference(const AnObject: TObject): Integer;
begin
  Inc(FReferenceIndex);
  FReferenceMap.Add(FReferenceIndex, AnObject);
  Result := FReferenceIndex;
end;

function TFpServerDbgController.GetObjectByReference(const AReference: Integer): TObject;
begin
  if not FReferenceMap.TryGetValue(AReference, Result) then
    Result := nil;
end;

function TFpServerDbgController.GetVariableBuilder: TDbgVariableBuilder;
begin
  Result := FVariableBuilder;
end;

procedure TFpServerDbgController.ClearObjectReferences;
begin
  FScopeCache.Clear;
  FReferenceMap.Clear;
end;

function TFpServerDbgController.GetBreakpointsForSource(ASource: string): TFpserverBreakpointArray;
var
  Pair: specialize TPair<Integer, TBreakpoint>;
  BreakpointLink: TFpServerBreakpointLink;
begin
  Result := [];
  for Pair in FBreakPointIdMap do
    if Pair.Value.Source=ASource then
      begin
      BreakpointLink.Breakpoint := Pair.Value.Breakpoint;
      BreakpointLink.Line := Pair.Value.Line;
      BreakpointLink.Id := Pair.Key;
      Insert(BreakpointLink, Result, 0);
      end;
end;

procedure TFpServerDbgController.FControllerExceptionEvent(var continue: boolean; const ExceptionClass, ExceptionMessage: string);
var
  ADebugEvent: TFpDebugEvent;
begin
  ADebugEvent := TFpDebugEvent.Create;
  try
    ADebugEvent.EventName := 'Exception';
    ADebugEvent.InstructionPointerRegValue := FDbgController.CurrentThread.GetInstructionPointerRegisterValue;
    ADebugEvent.ThreadID := FDbgController.CurrentThreadId;
    ADebugEvent.Message := ExceptionClass + ':' + ExceptionMessage;
    ADebugEvent.LisId := GeneralEventListenerId;
  Except
    ADebugEvent.Free;
    raise;
  end;
  FDistributor.SendEvent(ADebugEvent);
  continue:=false;
end;

procedure TFpServerDbgController.SetPascalExceptionBreakpoint;
begin
  if Assigned(FRaiseExceptionBreakPoint) then
    Raise Exception.Create('Exception-breakpoint already exists.');
  FRaiseExceptionBreakPoint := DbgController.CurrentProcess.AddBreak('FPC_RAISEEXCEPTION');
end;

procedure TFpServerDbgController.ObtainPascalExceptionInformation(
  out AnExceptionLocation: TDBGPtr;
  out AnExceptionClass, AnExceptionMessage: string);
var
  RegDxDwarfIndex, RegFirstArg: Cardinal;

  ExceptionTypeSymbol: TDbgHardcodedFPCClassTypeSymbol;
  ExceptionVariable: TDbgHardcodedVariableAtMemLocation;
  ExceptionInstance: TDbgHardcodedFPCClassValue;
  Scope: TFpDbgSymbolScope;
  AddressSize: Integer;
  MessageMember: TFpValue;
begin
  // Using regvar:
  // In all their wisdom, people decided to give the (r)dx register dwarf index
  // 1 on for x86_64 and index 2 for i386.
  if FDbgController.CurrentProcess.Mode=dm32 then begin
    RegDxDwarfIndex:=2;
    RegFirstArg := 0; // AX
    AddressSize := 4;
  end else begin
    RegDxDwarfIndex:=1;
    {$IFDEF windows}
    // Must be Win64
    RegFirstArg := 2; // RCX
    {$ELSE}
    RegFirstArg := 5; // RDI
    {$ENDIF}
    AddressSize := 8;
  end;

  // The location of the exception (where it occured)
  AnExceptionLocation:=FDbgController.CurrentThread.RegisterValueList.FindRegisterByDwarfIndex(RegDxDwarfIndex).NumValue;

  // We can not rely on the debug-information. It might be that the debug-information
  // for the exception-class is not included into the executable.
  // Therefor we use a hardcoded version of the debug-information of the Exception-
  // class.

  // Debug-information of the type of the Exception-class
  ExceptionTypeSymbol := TDbgHardcodedFPCExceptionTypeSymbol.Create('Exception', skType, InvalidLoc);
  try
    // Debug-information about the variable that contains the Exception
    ExceptionVariable := TDbgHardcodedVariableAtMemLocation.Create('E', skClass, ExceptionTypeSymbol, RegisterLoc(RegFirstArg));
    try
      // Obtain the value, based on the debug-information
      ExceptionInstance := ExceptionVariable.Value as TDbgHardcodedFPCClassValue;
      try
        // Finally we need the scope to be able to evaluate the value.
        Scope := FDbgController.CurrentProcess.FindSymbolScope(FDbgController.CurrentThreadId, 0);
        if not Assigned(Scope) then
          begin
          // This may never happen, there is always a scope. But atm (20200607) FindScope
          // does not return a scope when there is no debuginformation for the current
          // instruction pointer. We have to deal with this later. For now, use a
          // temporary scope.
          Scope := TFpDbgHardcodedScope.Create(FMemManager, AddressSize, FDbgController.CurrentThreadId);
          end;
        try
          ExceptionInstance.Context := Scope.LocationContext;
        finally
          Scope.ReleaseReference;
        end;

        // Now get the classname of the exception
        AnExceptionClass := ExceptionInstance.GetClassName;
        // And the message.
        MessageMember := ExceptionInstance.MemberByName['Message'];
        try
          AnExceptionMessage := MessageMember.AsString;
        finally
          MessageMember.ReleaseReference;
        end;
      finally
        ExceptionInstance.ReleaseReference;
      end;
    finally
      ExceptionVariable.ReleaseReference;
    end;
  finally
    ExceptionTypeSymbol.ReleaseReference;
  end;
end;

function TFpServerDbgController.FindScope(AThreadId, AStackFrame: Integer): TFpDbgSymbolScope;
var
  Index: TFpContextIndex;
begin
  Index.AThreadId := AThreadId;
  Index.AStackFrame := AStackFrame;
  if FScopeCache.ContainsKey(Index) then
    Result := FScopeCache.Items[Index]
  else
    begin
    Result := DbgController.CurrentProcess.FindSymbolScope(AThreadId, AStackFrame);
    FScopeCache.Add(Index, Result);
    Result.ReleaseReference;
    end;
end;

procedure TFpServerDbgController.DoScopeCacheValueChanged(ASender: TObject; constref AItem: TFpDbgSymbolScope; AAction: TCollectionNotification);
begin
  case AAction of
    cnAdded: AItem.AddReference;
    cnExtracted,
    cnRemoved: AItem.ReleaseReference;
  end;
end;

procedure TFpServerDbgController.ForcePaused;
begin
  // There is some magic in FDbgController.Pause that even if the debugee is
  // already paused, we still receive a HitBreakpointEvent. So that is the
  // location that the FForcePausedRequested is cleared safely.
  FForcePausedRequested := True;
  FDbgController.Pause;
end;

function TFpServerDbgController.GetRequestRunLoopRunning: Boolean;
begin
  Result := Boolean(InterLockedExchangeAdd(FProcessLoopRunning, 0));
end;

function TFpServerDbgController.GetSettings: TFpServerDbgControllerSettings;
begin
  if not Assigned(FSettings) then
    begin
    FSettings := TFpServerDbgControllerSettings.Create;
    FSettings.SortBy := [TDbgVariableSortBy.Init(vstInheritenceDepth, vsoDescending), TDbgVariableSortBy.Init(vstVisibility, vsoDescending), TDbgVariableSortBy.Init(vstDefined)];
    end;
  Result := FSettings;
end;

procedure TFpServerDbgController.RetrySettingAllBreakpoints;
var
  Pair: specialize TPair<Integer, TBreakpoint>;
  BreakPoint: TFpInternalBreakpoint;
  BreakPointId: Integer;
  DebugEvent: TFpDebugEvent;
begin
  for Pair in FBreakPointIdMap do
    begin
    // For now, ony retry to set breakpoints which are not set yet. Breakpoints
    // are not updated or removed.
    if not Assigned(Pair.Value.Breakpoint) then
      begin
      BreakPoint := DbgController.CurrentProcess.AddBreak(Pair.Value.Source, Pair.Value.Line);
      if Assigned(BreakPoint) then
        begin
        // Update the internal information. The returned BreakPointId is ignored.
        // (And should not have been altered)
        BreakPointId := Pair.Key;
        SetInternalBreakPointInfo(BreakPoint, Pair.Value.Source, Pair.Value.Line, BreakPointId);

        // Send an event that the breakpoint has changed
        DebugEvent := TFpDebugEvent.Create;
        try
          DebugEvent.EventName := 'BreakpointChanged';
          DebugEvent.Verified := True;
          DebugEvent.CustomId := BreakPointId;
          DebugEvent.Line := Pair.Value.Line;
          DebugEvent.LisId := GeneralEventListenerId;
          FDistributor.SendEvent(DebugEvent);
        finally
          DebugEvent.Release;
        end;
        end;
      end;
    end;
end;

procedure TFpServerDbgController.UnsetAllBreakpointsBoundToLibrary(ALibrary: TDbgLibrary);
var
  Pair: specialize TPair<Integer, TBreakpoint>;
  BreakPointId: Integer;
  DebugEvent: TFpDebugEvent;
begin
  for Pair in FBreakPointIdMap do
    begin
    if Assigned(Pair.Value.Breakpoint) then
      begin
      BreakPointId := Pair.Key;
      // Check for each set breakpoint if it belongs to the given library
      if Pair.Value.Breakpoint.BelongsToInstance(ALibrary) then
        begin
        // Unset the breakpoint
        DbgController.CurrentProcess.RemoveBreak(Pair.Value.Breakpoint);
        Pair.Value.Breakpoint.Free;

        // Send an event that the breakpoint has changed
        DebugEvent := TFpDebugEvent.Create;
        try
          DebugEvent.EventName := 'BreakpointChanged';
          DebugEvent.Verified := False;
          DebugEvent.CustomId := BreakPointId;
          DebugEvent.Line := Pair.Value.Line;
          DebugEvent.LisId := GeneralEventListenerId;

          FDistributor.SendEvent(DebugEvent);
        finally
          DebugEvent.Release;
        end;

        SetInternalBreakPointInfo(nil, Pair.Value.Source, Pair.Value.Line, BreakPointId);
        end;
      end;
    end;
end;

end.

