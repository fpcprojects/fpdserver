unit DABConsoleServer;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Pipes,
  Math,
  CustApp,
  syncobjs,
  fpjson,
  dcsInOutputProcessor,
  dcsConsoleServer,
  dcsHandler,
  DABInOutputProcessor,
  DABMessages;

type

  { TDABConsoleServer }

  TDABConsoleServer = class(TDCSConsoleServer)
  private
    procedure SendThreadProc();
  protected
    procedure SendCommand(ACommandStr: string);
    function CreateInOutputProcessor: TDCSCustomInOutputProcessor; override;
    function StartSendThread(): TThread; override;
    procedure Execute; override;
  end;

implementation

{ TDABConsoleServer }

procedure TDABConsoleServer.Execute;

  function ParseHeader(const s: string): TDABHeader;
  var
    Strings: TStrings;
  begin
    Strings := TStringList.Create;
    try
      Strings.NameValueSeparator := ':';
      Strings.Text := s;
      Result.ContentLength:=StrToIntDef(Trim(Strings.Values['Content-Length']), -1);
    finally
      Strings.Free;;
    end;
  end;

const
  InputBufferSize = 1024;
var
  s: string;
  i, x: integer;
  InputBuffer: array[0..InputBufferSize-1] of char;
  InputStr: string;
  Header: TDABHeader;
  InputStream: TInputPipeStream;
begin
  try
    FDistributor.SetNotificationEventsForListener(self, reAll, [ntExecutedCommand, ntFailedCommand, ntInvalidCommand, ntConnectionProblem]);

    InputStr := FInitialBuffer;
    InputStream:=TInputPipeStream.Create(StdInputHandle);
    try
      while not terminated do
        begin
        i := InputStream.Read(InputBuffer[0], InputBufferSize);
        if i > 0 then
          begin
          setlength(s,i);
          move(InputBuffer[0],s[1],i);
          InputStr:=InputStr+s;

          // Search for the end of a header.
          i := pos(#13#10#13#10, InputStr);
          while (i > 0) and not Terminated do
            begin
            // Put the full header in s
            s := copy(InputStr, 1, i-1);
            // Remove the header from the InputStr
            delete(InputStr, 1, i+3);
            // Parse the headers
            Header := ParseHeader(S);
            if Header.ContentLength<0 then
              begin
              FDistributor.SendNotification(FListenerId, ntConnectionProblem, null, 'Could not parse DAB-header. Terminating connection', '', []);
              Terminate;
              end;
            // Check if the full message is in the buffer (InputStr) already
            i := length(InputStr);
            if i < Header.ContentLength then
              begin
              // The message is incomplete, wait for the remaining part.
              SetLength(InputStr, Header.ContentLength);
              InputStream.ReadBuffer(InputStr[i+1], Header.ContentLength-i);
              end;
            s := copy(InputStr, 1, Header.ContentLength);
            SendCommand(s);
            Delete(InputStr, 1, Header.ContentLength);
            i := pos(#13#10, InputStr);
            end;
          end
        else if i < 0 then
          begin
          FDistributor.SendNotification(FListenerId, ntConnectionProblem, null, 'Error during read. Terminating connection', '', []);
          Terminate;
          end
        else // i=0
          begin
          // Connection lost
          Terminate;
          end;
        end;
    finally
      InputStream.Free;
    end;
  except
    // In the rare case of an exception, catch it and try to notify some
    // information. Or else this information will be lost when the thread dies.
    on E: exception do
      begin
      FDistributor.SendNotification(FListenerId, ntConnectionProblem, null, 'Unexpected exception. Terminating connection. [%s]', '', [E.Message]);
      end;
  end;
  FDistributor.RemoveListener(self);
  // There is no way that the client that started this DAB-adapter can get into
  // a working state again after this thread has been finished. So stop the
  // application completely. (This is also in line with the DAB-specification)
  // In theory a new TCP-IP connection could connect to restore the debug-
  // functionality. But that is a very unlikely scenario.
  if Assigned(CustomApplication) then
    CustomApplication.Terminate(1);
end;

procedure TDABConsoleServer.SendCommand(ACommandStr: string);
var
  ACommand: TDCSThreadCommand;
begin
  try
    ACommand := FInOutputProcessor.TextToCommand(ACommandStr);
  except
    on E: Exception do
      begin
      // The message will be converted to a shortstring so the exception might
      // get truncated. But something is better then nothing...
      FDistributor.SendNotification(FListenerId, ntInvalidCommand, null, 'Could not extract command from incoming data [%s]: %s', '', [ACommandStr, E.Message]);
      ACommand := nil;
      end;
  end;
  if assigned(ACommand) then
    FDistributor.QueueCommand(ACommand);
end;

procedure TDABConsoleServer.SendThreadProc();
var
  Messages: TStringArray;
  EventStr: string;
  i: Integer;
begin
  while (FEventStrQueue.PopItem(EventStr) = wrSignaled) do
    begin
    Messages := AddDABHeadersToJSonEvents(EventStr);
    for i := Low(Messages) to High(Messages) do
      begin
      write(StdOut, Messages[i]);
      end;
    Flush(StdOut);
    end;
end;

function TDABConsoleServer.CreateInOutputProcessor: TDCSCustomInOutputProcessor;
begin
  Result := TDABInOutputProcessor.create(FListenerId, FDistributor);
end;

function TDABConsoleServer.StartSendThread(): TThread;
begin
  Result := TThread.ExecuteInThread(@SendThreadProc);
end;

end.

