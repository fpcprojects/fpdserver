unit DebugTCPServer;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  ssockets,
  {$IFDEF Windows}
  Windows,
  {$ENDIF Windows}
  DCSTCPServer,
  dcsInOutputProcessor,
  dcsHandler,
  dcsListenerThread,
  DCSSimpleQueueBasedListener,
  syncobjs,
  FpDbgClasses,
  lazCollections,
  fpjson,
  DebugInOutputProcessor,
  debugthread,
  DABMessages,
  DABInOutputProcessor,
  DebugThreadDABCommand,
  DebugThreadCommand;

type

  { TFpDebugTcpServer }

  TFpDebugTcpServer = class(TDCSTcpServer)
  protected
    function CreateTCPConnectionThread(Data: TSocketStream): TDCSTcpConnectionThread; override;
  end;

  { TFpDebugTcpConnectionThread }

  TFpDebugTcpConnectionThread = class(TDCSTcpConnectionThread)
  protected
    function CreateInOutputProcessor: TDCSCustomInOutputProcessor; override;
  end;

  { TFpDebugTcpDABConnectionThread }

  TFpDebugTcpDABConnectionThread = class(TDCSTcpConnectionThread)
  private
    procedure SendThreadProc();
    procedure WhenPossibleStopDebugging;
  protected
    function StartSendThread(): TThread; override;
    procedure Execute; override;
    function CreateInOutputProcessor: TDCSCustomInOutputProcessor; override;
  end;


implementation

{ TFpDebugTcpConnectionThread }

function STrToStr(AStr: string): string;
var i : integer;
begin
  result := '';
  for i := 1 to length(AStr) do
    if ord(AStr[i])<20 then
      result := result +'#'+inttostr(ord(AStr[i]))
    else
      result := result + Astr[i];
end;

{ TFpDebugTcpDABConnectionThread }

procedure TFpDebugTcpDABConnectionThread.Execute;

  function ParseHeader(const s: string): TDABHeader;
  var
    Strings: TStrings;
  begin
    Strings := TStringList.Create;
    try
      Strings.NameValueSeparator := ':';
      Strings.Text := s;
      Result.ContentLength:=StrToIntDef(Trim(Strings.Values['Content-Length']), -1);
    finally
      Strings.Free;;
    end;
  end;

const
  InputBufferSize = 1024;
var
  s: ansistring;
  i: integer;
  InputBuffer: array[0..InputBufferSize-1] of char;
  InputStr: ansistring;
  Header: TDABHeader;
begin
  WhenPossibleStopDebugging;
  s := '';
  FDistributor.SetEventsForListener(Self, reAll);
  FDistributor.SetNotificationEventsForListener(Self, reAll, [ntExecutedCommand, ntInvalidCommand, ntFailedCommand, ntConnectionProblem]);

  try
    // This part has already been 'eaten' by the auto-sense mechanism.
    InputStr := FInitialBuffer;
    while not terminated do
      begin
      i := FData.Read(InputBuffer[0], InputBufferSize);
      if i > 0 then
        begin
        setlength(s,i);
        move(InputBuffer[0],s[1],i);
        InputStr:=InputStr+s;
        i := pos(#13#10#13#10, InputStr);
        while (i > 0) and not Terminated do
          begin
          s := copy(InputStr, 1, i-1);
          delete(InputStr, 1, i+3);
          Header := ParseHeader(S);
          if Header.ContentLength<0 then
            begin
            FResponseQueue.PushItem('Could not parse DAB-header. Terminate connection.'+LineEnding);
            Terminate;
            end;
          i := length(InputStr);
          if i < Header.ContentLength then
            begin
            SetLength(InputStr, Header.ContentLength);
            FData.ReadBuffer(InputStr[i+1], Header.ContentLength-i);
            end;
          s := copy(InputStr, 1, Header.ContentLength);
          SendCommand(s);
          Delete(InputStr, 1, Header.ContentLength);
          i := pos(#13#10, InputStr);
          end;
        end
      else if i < 0 then
        begin
        FDistributor.SendNotification(FListenerId, ntConnectionProblem, null, 'Error during read. Socket-error: %d', '', [FData.LastError]);
        Terminate;
        end
      else if i = 0 then
        begin
        // Zero-count -> Connection closed
        Terminate;
        end;
      end;
  except
    // In the rare case of an exception, catch it and try to notify some
    // information. Or else this information will be lost when the thread dies.
    on E: exception do
      begin
      FDistributor.SendNotification(FListenerId, ntConnectionProblem, null, 'Unexpected exception. Terminating connection. [%s]', '', [E.Message]);
      end;
  end;
  FDebugTcpServer.RemoveConnection(self);
  FResponseQueue.DoShutDown;
  FSendThread.WaitFor;
end;

function TFpDebugTcpDABConnectionThread.CreateInOutputProcessor: TDCSCustomInOutputProcessor;
begin
  Result := TDABInOutputProcessor.create(FListenerId, FDistributor);
end;

function TFpDebugTcpDABConnectionThread.StartSendThread(): TThread;
begin
  Result := TThread.ExecuteInThread(@SendThreadProc);
end;

procedure TFpDebugTcpDABConnectionThread.SendThreadProc();
var
  Res: Boolean;
  AStr: string;
  Messages: TStringArray;
  i: Integer;
begin
  Res := True;
  while Res and (FResponseQueue.PopItem(AStr) = wrSignaled) do
    begin
    Messages := AddDABHeadersToJSonEvents(AStr);
    for i := Low(Messages) to High(Messages) do
      begin
      Res := SendData(Messages[i]);
      if not Res then
        Break;
      end;
    end;
end;

procedure TFpDebugTcpDABConnectionThread.WhenPossibleStopDebugging;
var
  EventQueue: TDCSEventQueue;
  TempListener: TDCSSimpleQueueBasedListener;
  FakeDisconnectRequest: TDABRequest;
  DisconnectCommand: TFpDebugThreadDABDisconnectCommand;
  NoDebugeeRunning: Boolean;
  Event: TDCSEvent;
begin
  // DAB-clients assume that when they make a connection, they have a new 'session'
  // without any debugee running. To make this happen, the current debugee is
  // killed.

  // We have to do all this before the DAB-listener is up and running,
  // or else the DAB-client will receive messages from the prior debugee.

  // Runs within the thread.
  EventQueue := TDCSEventQueue.create(10, INFINITE, 10000);
  try
    //create a fake listener, so we can wait until the command has been executed
    TempListener := TDCSSimpleQueueBasedListener.Create('PreDABConnection', EventQueue);
    try
      FDistributor.AddListener(TempListener);
      FDistributor.SetEventsForListener(TempListener, reOwnAndGeneral);
      FDistributor.SetNotificationEventsForListener(TempListener, reAll, [ntExecutedCommand, ntInvalidCommand, ntFailedCommand]);

      // Send a disconnect command, to stop the debugee, in case it was running.
      FakeDisconnectRequest := TDABRequest.create;
      FakeDisconnectRequest.Seq := 34587343;
      DisconnectCommand := TFpDebugThreadDABDisconnectCommand.Create(TempListener.ListenerId, FakeDisconnectRequest, FDistributor);
      FDistributor.QueueCommand(DisconnectCommand);

      NoDebugeeRunning := False;
      repeat
      // Wait until:
      //  - The disconnect command failed because there was no debugee running
      //  - or the debugee has stopped
      //  - or a timeout of 10 seconds passed.
      if EventQueue.PopItem(Event) <> wrSignaled then
        begin
        // Something went wrong (timeout?) Assume debugee has been stopped.
        NoDebugeeRunning := True;
        end
      else
        begin
        if (Event.EventType=dcsetNotification) then
          begin
          if (TDCSNotificationEvent(Event).NotificationType=ntFailedCommand) and (Event.UID=34587343) then
          // If DisconnectCommand failed, there was probably no debuggee running at all
          NoDebugeeRunning := True;
          end
        else if (Event.EventType=dcsetEvent) and (Event is TFpDebugEvent) and (TFpDebugEvent(Event).EventName='ExitProcess') then
          begin
          // Debugee has been stopped
          NoDebugeeRunning := True;
          end;
        end;
      until NoDebugeeRunning;
    finally
      FDistributor.RemoveListener(TempListener);
      TempListener.Free;
    end;
  finally
    EventQueue.Free;
  end;
end;

function TFpDebugTcpConnectionThread.CreateInOutputProcessor: TDCSCustomInOutputProcessor;
begin
  Result := TJSonInOutputProcessor.create(FListenerId, FDistributor);
end;

{ TFpDebugTcpServer }

function TFpDebugTcpServer.CreateTCPConnectionThread(Data: TSocketStream): TDCSTcpConnectionThread;

var
  Buf: string;
  DAB: Boolean;
begin
  // AutoSense Debug-Adapter-Protocol:
  DAB := AutoSenseDABProtocol(Data, Buf);

  if DAB then
    Result:=TFpDebugTcpDABConnectionThread.create(FDistributor, Self, data, Buf)
  else
    Result:=TFpDebugTcpConnectionThread.create(FDistributor, Self, data, Buf);
end;

end.

