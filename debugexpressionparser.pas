unit DebugExpressionParser;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  cnocExpressionEvaluator,
  FpDbgInfo,
  FpDbgDwarf,
  FpDbgDwarfDataClasses,
  DbgIntfBaseTypes;

type

  { TDebugEvaluationValue }

  TDebugEvaluationValue = class(TEvaluationValue)
  private
    FValue: TFpValue;
    FParent: TFpValue;
  public
    constructor Create(AValue: TFpValue; AParent: TFpValue = nil);
    destructor Destroy; override;
    function Clone: TEvaluationValue; override;
    function TryGetAsInteger(out Int: Int64; out ErrMessage: string): Boolean; override;
    function TryGetAsCardinal(out Cardinal: QWord; out ErrMessage: string): Boolean; override;
    function TryObtainChild(ChildName: string; out EvaluationValue: TEvaluationValue; out ErrMessage: string): Boolean; override;
    property Value: TFpValue read FValue;
    property Parent: TFpValue read FParent;
  end;


implementation

{ TDebugEvaluationValue }

constructor TDebugEvaluationValue.Create(AValue: TFpValue; AParent: TFpValue = nil);

const
  FpValueKindToEvaluationKind: array[TDbgSymbolKind] of TEvaluationValueKind= (
    evkNone,     //  skNone,
    evkType,     //  skType,
    evkNone,     //  skInstance,
    evkNone,     //  skUnit,
    evkNone,     //  skProcedure,
    evkFunction, //  skFunction,
    evkNone,     //  skProcedureRef,
    evkNone,     //  skFunctionRef,
    evkNone,     //  skSimple,
    evkNone,     //  skPointer,
    evkInteger,  //  skInteger,
    evkCardinal, //  skCardinal,
    evkBoolean,  //  skBoolean,
    evkNone,     //  skChar,
    evkFloat,    //  skFloat,
    evkString,   //  skString,
    evkString,   //  skAnsiString,
    evkNone,     //  skCurrency,
    evkNone,     //  skVariant,
    evkNone,     //  skVariantPart,
    evkNone,     //  skWideString,
    evkNone,     //  skEnum,
    evkNone,     //  skEnumValue,
    evkNone,     //  skSet,
    evkNone,     //  skRecord,
    evkNone,     //  skObject,
    evkNone,     //  skClass,
    evkNone,     //  skInterface,
    evkNone,     //  skArray,
    evkNone,     //  skRegister,
    evkNone      //  skAddress
  );

begin
  FValue := AValue;
  FValue.AddReference;
  FParent := AParent;
  if Assigned(FParent) then
    FParent.AddReference;
  FKind := FpValueKindToEvaluationKind[AValue.Kind];
end;

function TDebugEvaluationValue.TryGetAsInteger(out Int: Int64; out ErrMessage: string): Boolean;
begin
  Int := 0;
  ErrMessage := '';
  Result := True;
  if FValue.Kind=skCardinal then
    begin
    if (FValue.AsCardinal > High(Int64)) then
      begin
      Result := False;
      ErrMessage := 'Range check error';
      end
    else
      Int := FValue.AsCardinal;
    end
  else if FValue.Kind=skInteger then
    Int := FValue.AsInteger
  else
    Result := inherited TryGetAsInteger(Int, ErrMessage);
end;

function TDebugEvaluationValue.TryGetAsCardinal(out Cardinal: QWord; out ErrMessage: string): Boolean;
begin
  Cardinal := 0;
  ErrMessage := '';
  Result := True;
  if FValue.Kind=skCardinal then
    Cardinal := FValue.AsCardinal
  else if FValue.Kind=skInteger then
    begin
    if FValue.AsInteger < 0 then
      begin
      Result := False;
      ErrMessage := 'Range check error';
      end
    else
      Cardinal := FValue.AsInteger
    end
  else
    Result := inherited TryGetAsCardinal(Cardinal, ErrMessage);
end;

function TDebugEvaluationValue.Clone: TEvaluationValue;
begin
  Result := TDebugEvaluationValue.Create(FValue, FParent);
end;

destructor TDebugEvaluationValue.Destroy;
begin
  FParent.ReleaseReference;
  FValue.ReleaseReference;
  inherited Destroy;
end;

function TDebugEvaluationValue.TryObtainChild(ChildName: string; out EvaluationValue: TEvaluationValue; out ErrMessage: string): Boolean;
var
  Child: TFpValue;
begin
  Child := FValue.MemberByName[ChildName];
  try
    if not Assigned(Child) then
      begin
      // Property hack: the child could be a property, using a getter
      Child := FValue.MemberByName['get'+ChildName];
      end;

    if not Assigned(Child) and SameText(ChildName,'Items')then
      begin
      // Property hack: the child could be a property, using a getter
      Child := FValue.MemberByName['GetItem'];
      end;

    if not Assigned(Child) then
      begin
      // Property hack: the child could be a property
      Child := FValue.MemberByName['F'+ChildName];
      end;

    if not Assigned(Child) then
      begin
      Result := inherited TryObtainChild(ChildName, EvaluationValue, ErrMessage)
      end
    else
      begin
      Result := True;
      ErrMessage := '';
      EvaluationValue := TDebugEvaluationValue.Create(Child, FValue);
      end;
  finally
    Child.ReleaseReference;
  end;
end;

end.

