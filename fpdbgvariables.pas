unit FPDbgVariables;

{$mode objfpc}{$H+}
{$modeswitch advancedrecords}

interface

uses
  Generics.Defaults,
  Generics.Collections,
  SysUtils,
  Math,
  DbgIntfBaseTypes,
  FpDbgInfo,
  // Should not be here, but is necessary for a hack to get the MemManager
  FpDbgDwarf,
  FpDbgDwarfDataClasses,
  FpDbgDwarfFreePascal,
  FpdMemoryTools,
  FpPascalBuilder;

type
  TDbgVariable = class;
  TDbgCustomVariableList = specialize TObjectList<TDbgVariable>;

  TDbgExtraInformationFlag = (eifHasAdditionalInfo, eifHasChildren, eifChildrenAreIndexed, eifCallableFunction, eifAdditionalInfo);
  TDbgExtraInformationFlags = set of TDbgExtraInformationFlag;
  TDbgVisibility = (vUnknown, vPrivate, vProtected, vPublic);

  TDbgVariableSortStrategy = (
    vstNone,
    vstDefined,          // Sort based on order of definition
    vstVisibility,       // Sort based on visibility
    vstInheritenceDepth, // Sort based on the inheritence-level of the structure a member is defined in
    vstAlphabetic        // Sort alphabetically
  );
  TDbgVariableSortOrder = (
    vsoAscending,
    vsoDescending
  );

  { TDbgVariableSortBy }

  TDbgVariableSortBy = record
  public
    Strategy: TDbgVariableSortStrategy;
    Order: TDbgVariableSortOrder;
    constructor Init(AStrategy: TDbgVariableSortStrategy; AnOrder: TDbgVariableSortOrder=vsoAscending);
  end;
  TDbgVariableSortByArr = array of TDbgVariableSortBy;

  { TDbgVariableList }

  TDbgVariableList = class(TDbgCustomVariableList)
  protected type
    TDelegatedComparer = specialize TDelegatedComparerEvents<TDbgVariable>;
  private
    FOrderByArr: TDbgVariableSortByArr;
  protected
    FChildrenAreIndexed: Boolean;
    function CompareEvent(constref Left, Right: TDbgVariable): Integer;
  public
    procedure Sort(OrderBy: TDbgVariableSortByArr);
    property ChildrenAreIndexed: Boolean read FChildrenAreIndexed;
  end;


  { TDbgVariable }

  TDbgVariable = class
  private
    FName: string;
    FValue: string;
    FType: string;
    FFlags: TStringArray;
    FAdditionalInfo: TDbgVariableList;
    FChildren: TDbgVariableList;
    FExtraInformationFlags: TDbgExtraInformationFlags;
    FAdditionalInfoReference: SizeInt;
    FChildrenReference: SizeInt;
    FChildCount: Integer;
    FStructureName: string;
    FVisibility: TDbgVisibility;
    FInheritenceDepth: integer;
    FDefNr: Integer;
  public
    constructor Create; virtual;
    destructor Destroy; override;
  published
    property Name: string read FName write FName;
    // When the variable is a member this field contains the name of the
    // structure in which this member is defined
    property StructureName: string read FStructureName write FStructureName;
    property InheritenceDepth: integer read FInheritenceDepth write FInheritenceDepth;
    property Value: string read FValue write FValue;
    property &Type: string read FType write FType;
    property Flags: TStringArray read FFlags write FFlags;
    property ExtraInformationFlags: TDbgExtraInformationFlags read FExtraInformationFlags write FExtraInformationFlags;
    property AdditionalInfo: TDbgVariableList read FAdditionalInfo;
    property ChildCount: Integer read FChildCount write FChildCount;
    property Children: TDbgVariableList read FChildren;
    property AdditionalInfoReference: SizeInt read FAdditionalInfoReference write FAdditionalInfoReference;
    property ChildrenReference: SizeInt read FChildrenReference write FChildrenReference;
    property Visibility: TDbgVisibility read FVisibility write FVisibility;
    property DefNr: Integer read FDefNr write FDefNr;
  end;

  TDbgVariableCacheKind = (vckChildren, vckIndexedChildren, vckAdditionalInfo, vckFunctionCall);
  TDbgVariableObtainReferenceProc = function(AValue: TFpValue; AKind: TDbgVariableCacheKind): SizeInt of object;

  { TDbgVariableBuilder }

  TDbgVariableBuilder = class
  private
    FOnObtainReference: TDbgVariableObtainReferenceProc;
    procedure HandleChildren(AValue: TFpValue; AVariableList: TDbgVariableList; AStartIndex, ACount: Integer; Depth: Integer; AContext: TFpDbgLocationContext);
  protected
    function EncodeStringAsRawData(AValue: string): string;
  public
    function ObtainVariableForValue(AValue: TFpValue; Depth: Integer; AContext: TFpDbgLocationContext; AScope: TFpDbgSymbolScope): TDbgVariable;
    function ObtainLocalVariablesForScope(AScope: TFpDbgSymbolScope; Depth: Integer): TDbgVariableList;
    function ObtainChildrenForValue(AValue: TFpValue; AStartIndex, ACount: Integer; Depth: Integer; AContext: TFpDbgLocationContext): TDbgVariableList;
    function ObtainAdditionalInfoForValue(AValue: TFpValue): TDbgVariableList;
    // This handler converts a TFpValue into a reference that can be used in a later
    // state to get more information (children, indexed-children or additional info)
    // about the variable.
    property OnObtainReference: TDbgVariableObtainReferenceProc read FOnObtainReference write FOnObtainReference;
  end;

  { TDbgVariableCache }

  TDbgVariableCacheRecord = record
    Value: TFpValue;
    Context: TFpDbgLocationContext;
    Kind: TDbgVariableCacheKind;
  end;
  TDbgVariableCacheDictionary = specialize TDictionary<SizeInt, TDbgVariableCacheRecord>;

  TDbgVariableCache = class
  private
    FBuilder: TDbgVariableBuilder;
    FVariableCache: TDbgVariableCacheDictionary;
    FLatestCacheReference: Integer;
    function AddNewCacheRecord(ACacheRecord: TDbgVariableCacheRecord): SizeInt;
    function RetrieveCacheRecord(AReference: SizeInt): TDbgVariableCacheRecord;
    function DoObtainReference(AValue: TFpValue; AKind: TDbgVariableCacheKind): SizeInt;
  public
    constructor Create(ABuilder: TDbgVariableBuilder);
    destructor Destroy; override;
    procedure InvalidateCache;
    function ObtainVariableForValue(AValue: TFpValue; Depth: Integer; AScope: TFpDbgSymbolScope): TDbgVariable;
    function ObtainVariablesForReference(AReference: SizeInt; AStartIndex, ACount: Integer; Depth: Integer): TDbgVariableList;
    function ObtainAdditionalInfoForReference(AReference: SizeInt): TDbgVariableList;
    procedure ObtainValueForReference(AReference: SizeInt; out AValue: TFpValue; out AContext: TFpDbgLocationContext);
  end;


implementation

const
  cLazVisibilityToVisibility: array[TDbgSymbolMemberVisibility] of TDbgVisibility =
    (vUnknown, vPrivate, vProtected, vPublic);

{ TDbgVariableSortBy }

constructor TDbgVariableSortBy.Init(AStrategy: TDbgVariableSortStrategy; AnOrder: TDbgVariableSortOrder);
begin
  Strategy:=AStrategy;
  Order:=AnOrder;
end;

{ TDbgVariableList }

procedure TDbgVariableList.Sort(OrderBy: TDbgVariableSortByArr);
var
  Comparer: TDelegatedComparer;
begin
  FOrderByArr := OrderBy;
  Comparer := TDelegatedComparer.Create(@CompareEvent);
  try
    inherited Sort(Comparer);
  finally
    Comparer.Free;
  end;
end;

function TDbgVariableList.CompareEvent(constref Left, Right: TDbgVariable): Integer;

  function Comp(const OrderBy: TDbgVariableSortBy): Integer;
  begin
    case OrderBy.Strategy of
      vstDefined          : Result := Left.DefNr - Right.DefNr;
      vstAlphabetic       : Result := CompareText(Left.Name, Right.Name);
      vstInheritenceDepth : Result := Left.InheritenceDepth - Right.InheritenceDepth;
      vstVisibility       : Result := Ord(Left.Visibility) - Ord(Right.Visibility);
    else
      Result := EqualsValue;
    end;
    if OrderBy.Order=vsoDescending then
      Result := - Result;
  end;

var
  i: Integer;
begin
  if not assigned(Left) then
    Result := -1
  else if not assigned(Right) then
    Result := 1
  else
    begin
    Result := 0;
    for i := 0 to High(FOrderByArr) do
      if (FOrderByArr[i].Strategy <> vstNone) then
        begin
        Result := Comp(FOrderByArr[i]);
        if Result <> 0 then
          Break;
        end;
    end;
end;

{ TDbgVariableCache }

function TDbgVariableCache.ObtainVariableForValue(AValue: TFpValue; Depth: Integer; AScope: TFpDbgSymbolScope): TDbgVariable;
begin
  Result := FBuilder.ObtainVariableForValue(AValue, Depth, nil, AScope);
end;

constructor TDbgVariableCache.Create(ABuilder: TDbgVariableBuilder);
begin
  FVariableCache := TDbgVariableCacheDictionary.Create;
  FBuilder := ABuilder;
  FBuilder.OnObtainReference := @DoObtainReference;
end;

function TDbgVariableCache.AddNewCacheRecord(ACacheRecord: TDbgVariableCacheRecord): SizeInt;
begin
  Inc(FLatestCacheReference);
  FVariableCache.Add(FLatestCacheReference, ACacheRecord);
  Result := FLatestCacheReference;
end;

function TDbgVariableCache.RetrieveCacheRecord(AReference: SizeInt): TDbgVariableCacheRecord;
begin
  Result := FVariableCache.Items[AReference];
end;

destructor TDbgVariableCache.Destroy;
begin
  FVariableCache.Free;
  inherited Destroy;
end;

procedure TDbgVariableCache.InvalidateCache;
var
  Reference: SizeInt;
begin
  for Reference in FVariableCache.Keys do
    begin
    FVariableCache.Items[Reference].Value.ReleaseReference;
    if Assigned(FVariableCache.Items[Reference].Context) then
      FVariableCache.Items[Reference].Context.ReleaseReference;
    FVariableCache.Remove(Reference);
    end;
end;

function TDbgVariableCache.ObtainVariablesForReference(AReference: SizeInt; AStartIndex, ACount: Integer; Depth: Integer): TDbgVariableList;
var
  CacheRecord: TDbgVariableCacheRecord;
begin
  Result := nil;
  CacheRecord := RetrieveCacheRecord(AReference);
  Result := FBuilder.ObtainChildrenForValue(CacheRecord.Value, AStartIndex, ACount, Depth, CacheRecord.Context);
  if CacheRecord.Kind=vckIndexedChildren then
    Result.FChildrenAreIndexed:=True;
end;

function TDbgVariableCache.ObtainAdditionalInfoForReference(AReference: SizeInt): TDbgVariableList;
var
  CacheRecord: TDbgVariableCacheRecord;
begin
  CacheRecord := RetrieveCacheRecord(AReference);
  Result := FBuilder.ObtainAdditionalInfoForValue(CacheRecord.Value);
end;

procedure TDbgVariableCache.ObtainValueForReference(AReference: SizeInt; out AValue: TFpValue; out AContext: TFpDbgLocationContext);
var
  CacheRecord: TDbgVariableCacheRecord;
begin
  CacheRecord := RetrieveCacheRecord(AReference);
  AValue := CacheRecord.Value;
  AContext := CacheRecord.Context;
end;

function TDbgVariableCache.DoObtainReference(AValue: TFpValue; AKind: TDbgVariableCacheKind): SizeInt;
var
  CacheRecord: TDbgVariableCacheRecord;
begin
  CacheRecord.Value := AValue;
  if AValue is TFpValueDwarf then
    begin
    CacheRecord.Context := TFpValueDwarf(AValue).Context;
    if Assigned(CacheRecord.Context) then
      CacheRecord.Context.AddReference;
    end;
  AValue.AddReference;
  CacheRecord.Kind := AKind;
  Result := AddNewCacheRecord(CacheRecord);
end;

{ TDbgVariable }

constructor TDbgVariable.Create;
begin
  FChildren := TDbgVariableList.Create;
end;

destructor TDbgVariable.Destroy;
begin
  FChildren.Free;
  inherited Destroy;
end;

{ TDbgVariableBuilder }

function TDbgVariableBuilder.ObtainLocalVariablesForScope(AScope: TFpDbgSymbolScope; Depth: Integer): TDbgVariableList;
var
  ProcVal: TFpValue;
  i: Integer;
  m: TFpValue;
  VariableList: TDbgVariableList;
begin
  result := nil;

  if (AScope = nil) or (AScope.SymbolAtAddress = nil) then
    exit;

  ProcVal := AScope.ProcedureAtAddress;

  if (ProcVal = nil) or (ProcVal.MemberCount < 1) then
    exit;

  VariableList := TDbgVariableList.Create(True);
  try
    for i := 0 to ProcVal.MemberCount - 1 do
      begin
      m := ProcVal.Member[i];
      if m <> nil then
        begin
        VariableList.Add(ObtainVariableForValue(m, Depth, nil, AScope));
        m.ReleaseReference;
        end;
      end;
    Result := VariableList;
    VariableList := nil;
  finally
    VariableList.Free;
  end;

  ProcVal.ReleaseReference;
end;

function TDbgVariableBuilder.ObtainVariableForValue(AValue: TFpValue; Depth: Integer; AContext: TFpDbgLocationContext; AScope: TFpDbgSymbolScope): TDbgVariable;

  function PrettyPrintValue: string;
  var
    PrettyPrinter: TFpPascalPrettyPrinter;
  begin
    PrettyPrinter := TFpPascalPrettyPrinter.Create(sizeof(pointer));
    try
      PrettyPrinter.Context := AContext;
      PrettyPrinter.PrintValue(Result, AValue);
    finally
      PrettyPrinter.Free;
    end;
  end;

var
  n: string;
  ValueClassName: string;
  AddrSize: TFpDbgValueSize;
  Variable: TDbgVariable;
  ProcessChildren: Boolean;
  Symbol: TFpSymbol;
  ClassTypeDef: TFpValue;
begin
  Result := nil;
  if not Assigned(AValue) then
    Exit;
  ProcessChildren := False;

  Symbol := AValue.DbgSymbol;
  if Symbol <> nil then
    n := Symbol.Name
  else
    n := '';

  Variable := TDbgVariable.Create;
  try
    Variable.Name := n;

    if Assigned(Symbol) then
      Variable.Visibility := cLazVisibilityToVisibility[Symbol.MemberVisibility];

    if Assigned(AValue.TypeInfo) then
      Variable.&Type := AValue.TypeInfo.Name;

    case AValue.Kind of
      skClass:
        begin
        if Assigned(AValue.TypeInfo) then
          begin
          AValue.TypeInfo.GetInstanceClassName(AValue, ValueClassName);
          if Assigned(AScope) then
            begin
            ClassTypeDef := AScope.FindSymbol(ValueClassName);
            if Assigned(ClassTypeDef) then
              begin
              AValue := ClassTypeDef.GetTypeCastedValue(AValue);
              end;
            end;
          end
        else
          ValueClassName := 'class';

        if (AValue.AsCardinal = 0) then
          begin
          Variable.Value := 'nil';
          end
        else
          begin
          AValue.GetSize(AddrSize);
          Variable.Value := ValueClassName+' ($' + HexStr(LocToAddrOrNil(AValue.DataAddress), SizeToFullBytes(AddrSize)*2) + ')';
          ProcessChildren := True;
          end;
        end;
      skRecord:
        begin
        Variable.Value := 'record';
        ProcessChildren := True;
        end;
      skArray:
        begin
        Variable.Value := 'array';

        if AValue.HasBounds then
          Variable.Value := Variable.Value + '['+IntToStr(AValue.OrdLowBound)+'..'+IntToStr(AValue.OrdHighBound)+']'
        else
          Variable.Value := Variable.Value + '['+IntToStr(AValue.MemberCount)+']';
        Variable.ExtraInformationFlags := Variable.ExtraInformationFlags + [eifChildrenAreIndexed];
        ProcessChildren := True;
        end;
      skString:
        begin
        Variable.Value := PrettyPrintValue;

        Variable.ExtraInformationFlags := Variable.ExtraInformationFlags + [eifHasAdditionalInfo];
        if Assigned(FOnObtainReference) then
          Variable.AdditionalInfoReference := OnObtainReference(AValue, vckAdditionalInfo);
        end;
      skFunction:
        begin
        Variable.ExtraInformationFlags := Variable.ExtraInformationFlags + [eifCallableFunction];

        if Assigned(Symbol) and (sfpropGet in Symbol.Flags) then
          Variable.Value := 'property ('+AValue.TypeInfo.Name+')'
        else
          Variable.Value := PrettyPrintValue;

        if Assigned(FOnObtainReference) then
          Variable.ChildrenReference := OnObtainReference(AValue, vckFunctionCall);
        end
    else
      Variable.Value := PrettyPrintValue;
    end;

    if ProcessChildren and (AValue.MemberCount > 0) then
      begin
      Variable.ExtraInformationFlags := Variable.ExtraInformationFlags + [eifHasChildren];
      Variable.ChildCount := AValue.MemberCount;
      if Depth > 0 then
        HandleChildren(AValue, Variable.Children, 0, -1, Depth -1, AContext)
      else if Assigned(FOnObtainReference) then
        begin
        if eifChildrenAreIndexed in Variable.ExtraInformationFlags then
          Variable.ChildrenReference := OnObtainReference(AValue, vckIndexedChildren)
        else
          Variable.ChildrenReference := OnObtainReference(AValue, vckChildren);
        end;
      end;

    Result := Variable;
    Variable := nil;
  finally
    Variable.Free;
  end;
end;

procedure TDbgVariableBuilder.HandleChildren(AValue: TFpValue; AVariableList: TDbgVariableList; AStartIndex, ACount: Integer; Depth: Integer; AContext: TFpDbgLocationContext);
var
  i: PtrInt;
  Variable: TDbgVariable;
  MemberValue: TFpValue;
  LowBound, HighBound: PtrInt;
  InheritenceDepth: Integer;
  CurrentParent: TFpSymbol;
begin
  if AValue.HasBounds then
    begin
    LowBound := AValue.OrdLowBound;
    HighBound := AValue.OrdHighBound;
    end
  else
    begin
    LowBound := 0;
    HighBound := AValue.MemberCount -1;
    end;
  if AStartIndex > 0 then
    LowBound := LowBound + AStartIndex;
  if ACount > 0 then
    HighBound := Min(LowBound+ACount, HighBound);
  InheritenceDepth := 0;
  CurrentParent := nil;
  for i := LowBound to HighBound do
    begin
    MemberValue := AValue.Member[i];
    try
      // Omit functions and procedures from the list of members, unless they
      // are in fact a property-getter.
      if not ((MemberValue = nil) or ((MemberValue.Kind in [skFunction, skProcedure]) and not (sfpropGet in MemberValue.DbgSymbol.Flags))) then
        begin
        Variable := ObtainVariableForValue(MemberValue, Depth, AContext, nil);
        // Add 1, to be able to distinguish variables which do not have their
        // DefNr set.
        Variable.DefNr:=i+1;
        if Variable.Name='' then
          Variable.Name := '['+IntToStr(i)+']';
        if assigned(MemberValue.ParentTypeInfo) then
          begin
          if CurrentParent<>MemberValue.ParentTypeInfo then
            begin
            CurrentParent := MemberValue.ParentTypeInfo;
            Inc(InheritenceDepth);
            end;
          Variable.InheritenceDepth := InheritenceDepth;
          Variable.StructureName := MemberValue.ParentTypeInfo.Name;
          end;
        AVariableList.Add(Variable);
        end;
    finally
      MemberValue.ReleaseReference;
    end;
    end;
end;

function TDbgVariableBuilder.EncodeStringAsRawData(AValue: string): string;

var
  InStr: Boolean;

  procedure SetQuoted(const Quoted: boolean);
  begin
    if InStr<>Quoted then
      begin
      Result := Result + '''';
      InStr := not InStr;
      end;
  end;

var
  i: Integer;
  c: Char;
begin
  if AValue='' then
    Exit('''''');
  Result := '';
  InStr := False;
  // Not that fast, but this function should not be called that often.
  for i := 1 to Length(AValue) do
    begin
    c := AValue[i];
    if c in ['$'..'_', 'a'..'~', ' ', '!', '"'] then
      begin
      SetQuoted(True);
      Result := Result + c;
      end
    else
      begin
      SetQuoted(False);
      Result := Result + '#'+IntToStr(Ord(c));
      end;
    end;
  SetQuoted(False);
end;

function TDbgVariableBuilder.ObtainChildrenForValue(AValue: TFpValue; AStartIndex, ACount: Integer; Depth: Integer; AContext: TFpDbgLocationContext): TDbgVariableList;
var
  List: TDbgVariableList;
begin
  List := TDbgVariableList.Create;
  try
    HandleChildren(AValue, List, AStartIndex, ACount, Depth, AContext);
    Result := List;
    List := nil;
  finally
    List.Free;
  end;
end;

function TDbgVariableBuilder.ObtainAdditionalInfoForValue(AValue: TFpValue): TDbgVariableList;
var
  List: TDbgVariableList;
  Variable: TDbgVariable;
begin
  List := TDbgVariableList.Create;
  try
    case AValue.Kind of
      skString:
        begin
        if AValue is TFpValueDwarfV3FreePascalString then
          begin
          Variable := TDbgVariable.Create;
          List.Add(Variable);
          Variable.Name:='Dynamic codepage';
          case TFpValueDwarfV3FreePascalString(AValue).DynamicCodePage of
            CP_ACP:   Variable.Value:='CP_ACP';
            CP_OEMCP: Variable.Value:='CP_OEM';
            CP_UTF7:  Variable.Value:='UTF-7';
            CP_UTF8:  Variable.Value:='UTF-8';
            CP_NONE:  Variable.Value:='NONE';
          else
            Variable.Value:=IntToStr(TFpValueDwarfV3FreePascalString(AValue).DynamicCodePage);
          end;
          Variable.ExtraInformationFlags:=[eifAdditionalInfo];
          end;

        Variable := TDbgVariable.Create;
        List.Add(Variable);
        Variable.Name:='Rawdata';
        Variable.Value:=EncodeStringAsRawData(AValue.AsString);
        Variable.ExtraInformationFlags:=[eifAdditionalInfo];
        end;
    end;
    Result := List;
    List := nil;
  finally
    List.Free;
  end;
end;

end.

