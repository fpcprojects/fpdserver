unit DebugScriptServer;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  dcsHandler,
  debugthread,
  DebugInOutputProcessor;

type

  { TFpDebugScriptServer }

  TFpDebugScriptServer = class(TThread, IDCSListener)
  private
    FListenerId: Integer;
    FDistributor: TDCSDistributor;
    FConnectionIdentifier: integer;
    FFileContents: TStringList;
    FInOutputProcessor: TJSonInOutputProcessor;
  protected
    procedure Execute; override;
    function GetListenerId: Integer;
    procedure InitListener(AListenerId: Integer);
  public
    constructor create(ADistributor: TDCSDistributor; AFileName: string);
    function GetOrigin: string;
    procedure SendEvent(AnEvent: TDCSEvent);
    destructor Destroy; override;
  end;

implementation

{ TFpDebugScriptServer }

procedure TFpDebugScriptServer.Execute;
var
  ACommand: TFpDebugThreadCommand;
  i: Integer;
begin
  FInOutputProcessor := TJSonInOutputProcessor.create(FConnectionIdentifier, FDistributor);
  try
    for i := 0 to FFileContents.Count-1 do
      begin
      ACommand := FInOutputProcessor.TextToCommand(FFileContents.Strings[i]);
      if assigned(ACommand) then
        FDistributor.QueueCommand(ACommand);
      if Terminated then
        Break;
      end;
  finally
    FInOutputProcessor.Free;
  end;
  Terminate;
end;

constructor TFpDebugScriptServer.create(ADistributor: TDCSDistributor; AFileName: string);
begin
  inherited Create(false);
  FDistributor:=ADistributor;
  FConnectionIdentifier := FDistributor.AddListener(self);
  FFileContents := TStringList.Create;
  FFileContents.LoadFromFile(AFileName);
end;

function TFpDebugScriptServer.GetOrigin: string;
begin
  result := 'File input';
end;

procedure TFpDebugScriptServer.SendEvent(AnEvent: TDCSEvent);
begin
  // Ignore
end;

destructor TFpDebugScriptServer.Destroy;
begin
  FFileContents.Free;
  inherited Destroy;
end;

function TFpDebugScriptServer.GetListenerId: Integer;
begin
  Result := FListenerId;
end;

procedure TFpDebugScriptServer.InitListener(AListenerId: Integer);
begin
  FListenerId := AListenerId;
end;

end.

