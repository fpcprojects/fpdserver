FROM registry.fedoraproject.org/fedora-minimal

RUN microdnf -y update
RUN microdnf install -y git subversion fpc glibc-devel make
RUN microdnf clean all

RUN /usr/lib64/fpc/3.2.2/samplecfg /usr/lib64/fpc/3.2.2
