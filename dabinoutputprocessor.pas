unit DABInOutputProcessor;

{$mode objfpc}{$H+}

interface

uses
  {$IFDEF UNIX}
  BaseUnix,
  termio,
  {$ENDIF}
  {$IFDEF Windows}
  Windows,
  WinSock2,
  ssockets,
  {$ENDIF}
  Classes,
  SysUtils,
  Variants,
  fpjson,
  csModel,
  csJSONRttiStreamHelper,
  dcsHandler,
  dcsInOutputProcessor,
  debugthread,
  DABMessages,
  DABFpdMessages,
  DebugThreadDABCommand;

type
  TDABHeader = record
    ContentLength: Integer;
  end;

  { TDABInOutputProcessor }

  TDABInOutputProcessor = class(TDCSCustomInOutputProcessor)
  private
    FSerializer: TJSONRttiStreamClass;
    function GetDynamicBodyDescription(AnDescription: TcsStreamDescription; AnInstance: TObject): TcsStreamDescription;
  public
    constructor create(ALisId: integer; ADistributor: TDCSDistributor); override;
    destructor Destroy; override;
    function TextToCommand(const ACommandText: string): TFpDebugThreadCommand; override;
    function EventToText(AnEvent: TDCSEvent): string; override;
  end;

  function AddDABHeadersToJSonEvents(AnEventStr: string): TStringArray;
  function AutoSenseDABProtocol(const Stream: TStream; out InitialBuffer: string): Boolean;

implementation

function AddDABHeadersToJSonEvents(AnEventStr: string): TStringArray;

 function AddHeader(const AStr: string): string;
 begin
   Result := 'Content-Length: ' + IntToStr(Length(AStr)) + #13#10#13#10 + AStr;
 end;

var
  JSArray: TJSONArray;
  j: Integer;

begin
  Result := [];
  // The DAB-protocol does not allow an array of events. So send them separately.
  // (In theory AnEventStr is never '', but in case of a mistake it is better
  //  to transfer the empty string, then to raise an AV.)
  if (AnEventStr<>'') and (AnEventStr[1]='[') then
    begin
    JSArray := GetJSON(AnEventStr) as TJSONArray;
    for j := 0 to JSArray.Count -1 do
      Result := Concat(Result, [AddHeader(JSArray[j].AsJSON)]);
    end
  else
    Result := [AddHeader(AnEventStr)];
end;

function AutoSenseDABProtocol(const Stream: TStream; out InitialBuffer: string): Boolean;

  function NumBytesAvailable: DWord;
  begin
    {$IFDEF Unix}
    if fpioctl((Stream as THandleStream).Handle, FIONREAD, @Result)<0 then
      Result := 0;
    {$ENDIF Unix}
    {$IFDEF Windows}
    if Stream is TSocketStream then
      begin
      if ioctlsocket(TSocketStream(Stream).Handle, FIONREAD, @Result)<0 then
        Result := 0;
      end
    else if Stream is THandleStream then
      begin
      if not PeekNamedPipe(THandleStream(Stream).Handle, nil, 0, nil, @Result, nil) then
        Result := 0;
      end
    else
      Result := 0;
    {$ENDIF}
  end;

var
  Buf: string;
  i,l: LongInt;
begin
  Result := False;
  InitialBuffer := '';

  // TInputPipeStream could not be used, because it closes the handle when it
  // gets destroyed.

  // Wait at the most 20*10 milliseconds for enough input on the console to detect
  // the DAB-protocol.
  for i := 0 to 20 do
    begin
    if NumBytesAvailable > 15 then
      begin
      // There are at least 16 bytes in the buffer. Check if these bytes look
      // like a DAB-header.
      Buf:='';
      SetLength(Buf,16);
      l := Stream.Read(Buf[1], 16);
      SetLength(Buf, l);
      Result := Buf = 'Content-Length: ';

      // Fill the initial-buffer with the data that we 'peaked' from stdin
      InitialBuffer := Buf;
      break;
      end;
    sleep(10);
    end;
end;

{ TDABInOutputProcessor }

function TDABInOutputProcessor.EventToText(AnEvent: TDCSEvent): string;

  function GetOutputEvent(const Category, Output: string): string;
  var
    Event: TDABOutputEvent;
  begin
    Event := TDABOutputEvent.create;
    try
      Event.InitSequence;
      TDABOutputEventBody(Event.Body).Category := Category;
      TDABOutputEventBody(Event.Body).Output := Output;
      Result := FSerializer.ObjectToJSONString(Event);
    finally
      Event.Free;
    end;
  end;

var
  Response: TDABResponse;
  Event: TDABEvent;
  NotificationEvent: TDCSNotificationEvent;
  DebugEvent: TFpDebugEvent;
  LogEvent: TDCSLogEvent;
begin
  Result := '';
  if AnEvent is TDCSNotificationEvent then
    begin
    NotificationEvent := AnEvent as TDCSNotificationEvent;
    if NotificationEvent.NotificationType=ntConnectionProblem then
      begin
      // Difficult to decide what to do here. But at least do an attempt to
      // tell the client what happened.
      Result := GetOutputEvent('important', NotificationEvent.Message);
      end
    else if (NotificationEvent.NotificationType=ntInvalidCommand) and (VarIsNull(NotificationEvent.UID)) then
      begin
      // The command is invalid but we do not even have the request_seq number,
      // so we can not respond with a 'Response'. Send an output-event
      // instead.
      Result := GetOutputEvent('important', 'Invalid DAB-command. ' + NotificationEvent.Message);
      end
    else
      begin
      Response := TDABResponse.create;
      try
        Response.InitSequence;
        Response.Command := NotificationEvent.Command;
        if NotificationEvent is TfpDebugDABNotificationEvent then
          Response.Body := TfpDebugDABNotificationEvent(NotificationEvent).Body;
        Response.Success := NotificationEvent.NotificationType = ntExecutedCommand;
        if not Response.Success then
          Response.Message := NotificationEvent.Message;
        Response.Request_seq := NotificationEvent.UID;
        Result := FSerializer.ObjectToJSONString(Response);
      finally
         Response.Free;
      end;
      end;
    end
  else if AnEvent is TFpDebugEvent then
    begin
    DebugEvent := AnEvent as TFpDebugEvent;
    if DebugEvent.EventName='CreateProcess' then
      begin
      Event := TDabInitializedEvent.create;
      try
        Event.InitSequence;
        Result := FSerializer.ObjectToJSONString(Event);
      finally
        Event.Free;
      end;
      end
    else if DebugEvent.EventName='ExitProcess' then
      begin
      Event := TDABTerminatedEvent.create;
      try
        Event.InitSequence;
        Result := FSerializer.ObjectToJSONString(Event);
      finally
        Event.Free;
      end;
      Event := TDABExitedEvent.create;
      try
        Event.InitSequence;
        TDABExitedEventBody(Event.Body).ExitCode:=DebugEvent.ExitCode;
        Result := '[' + Result + ',' + FSerializer.ObjectToJSONString(Event) + ']';
      finally
        Event.Free;
      end;
      end
    else if DebugEvent.EventName='BreakPoint' then
      begin
      Event := TDABStoppedEvent.create;
      try
        Event.InitSequence;
        if DebugEvent.BreakpointServerIdr > 0 then
          begin
          TDABStoppedEventBody(Event.Body).Reason := 'breakpoint';
          TDABStoppedEventBody(Event.Body).Description := 'Paused on breakpoint';
          TDABStoppedEventBody(Event.Body).HitBreakpointIds := [DebugEvent.BreakpointServerIdr];

          end
        else
          begin
          TDABStoppedEventBody(Event.Body).Reason := 'step';
          TDABStoppedEventBody(Event.Body).Description := 'Single stepping';
          end;
        TDABStoppedEventBody(Event.Body).ThreadId := DebugEvent.ThreadID;
        Result := FSerializer.ObjectToJSONString(Event);
      finally
        Event.Free;
      end;
      end
    else if DebugEvent.EventName='Exception' then
      begin
      Event := TDABStoppedEvent.create;
      try
        Event.InitSequence;
        TDABStoppedEventBody(Event.Body).Reason := 'exception';
        TDABStoppedEventBody(Event.Body).Description := 'Paused on exception';
        TDABStoppedEventBody(Event.Body).Text := DebugEvent.Message;
        TDABStoppedEventBody(Event.Body).ThreadId := DebugEvent.ThreadID;
        Result := FSerializer.ObjectToJSONString(Event);
      finally
        Event.Free;
      end;
      end
    else if DebugEvent.EventName='ConsoleOutput' then
      Result := GetOutputEvent('stdout', DebugEvent.Message)
    else if (DebugEvent.EventName = 'LibraryLoaded') or (DebugEvent.EventName = 'LibraryUnLoaded') then
      begin
      Event := TDABModuleEvent.create;
      try
        Event.InitSequence;
        if DebugEvent.EventName = 'LibraryLoaded' then
          TDABModuleEventBody(Event.Body).Reason := 'new'
        else
          TDABModuleEventBody(Event.Body).Reason := 'removed';
        TDABModuleEventBody(Event.Body).Module.Id := DebugEvent.CustomId;
        TDABModuleEventBody(Event.Body).Module.Name := ExtractFileName(DebugEvent.Message);
        TDABModuleEventBody(Event.Body).Module.Path := DebugEvent.Message;
        TDABModuleEventBody(Event.Body).Module.SymbolStatus := DebugEvent.ModuleSymbolStatus;
        Result := FSerializer.ObjectToJSONString(Event);
      finally
        Event.Free;
      end;
      end
    else if DebugEvent.EventName='BreakpointChanged' then
      begin
      Event := TDABBreakpointEvent.create;
      try
        Event.InitSequence;
        TDABBreakpointEventBody(Event.Body).Reason := 'changed';
        TDABBreakpointEventBody(Event.Body).Breakpoint.Verified := DebugEvent.Verified;
        TDABBreakpointEventBody(Event.Body).Breakpoint.Id := DebugEvent.CustomId;
        TDABBreakpointEventBody(Event.Body).Breakpoint.Line := DebugEvent.Line;
        Result := FSerializer.ObjectToJSONString(Event);
      finally
        Event.Free;
      end;
      end
    else
      raise Exception.CreateFmt('Received unsupported debug-event: [%s]', [DebugEvent.EventName]);
    end
  else if AnEvent is TDCSLogEvent then
    begin
    LogEvent := AnEvent as TDCSLogEvent;
    Event := TDABOutputEvent.create;
    try
      Event.InitSequence;
      if LogEvent.LogLevel in [etWarning, etError] then
        // Set the category to 'important' so that the client may decide to show
        // the message in it's UI as a popup.
        TDABOutputEventBody(Event.Body).Category := 'important'
      else
        // With the category 'console' the client will show the event as a
        // message from the debugger. (A 'debug console')
        TDABOutputEventBody(Event.Body).Category := 'console';
      TDABOutputEventBody(Event.Body).Output := LogEvent.Message;
      Result := FSerializer.ObjectToJSONString(Event);
    finally
      Event.Free;
    end;
    end
  else
    raise Exception.CreateFmt('Could not convert event to DAB message: [%s]', [AnEvent.Classname]);
end;

function TDABInOutputProcessor.TextToCommand(const ACommandText: string): TFpDebugThreadCommand;
var
  ARequest: TDABRequest;
  LaunchRequest: TDABFpdLaunchRequest;
  ScopeRequest: TDABScopesRequest;
  StackTraceRequest: TDABStackTraceRequest;
  SetBreakpointsRequest: TDABSetBreakpointsRequest;
  VariablesRequest: TDABVariablesRequest;
  SetVariableRequest: TDABSetVariableRequest;
  EvaluateRequest: TDABEvaluateRequest;
  Event: TDCSNotificationEvent;
begin
  ARequest := TDABRequest.Create;
  try
    FSerializer.JSONStringToObject(ACommandText, ARequest);
    Assert(ARequest.&Type = 'request');
    case ARequest.Command of
      'initialize':
        begin
        Result := TFpDebugThreadDABInitializeCommand.Create(FLisId, ARequest, FDistributor);
        ARequest := nil;
        end;
      'launch':
        begin
        LaunchRequest := FSerializer.specialize CreateObjectFromJSONString<TDABFpdLaunchRequest>(ACommandText);
        Result := TFpDebugThreadDABLaunchCommand.Create(FLisId, LaunchRequest, FDistributor);
        end;
      'setBreakpoints':
        begin
        SetBreakpointsRequest := FSerializer.specialize CreateObjectFromJSONString<TDABSetBreakpointsRequest>(ACommandText);
        Result := TFpDebugThreadDABSetBreakpointsCommand.Create(FLisId, SetBreakpointsRequest, FDistributor);
        end;
      'setExceptionBreakpoints':
        begin
        Result := TFpDebugThreadDABSetExceptionBreakpointsCommand.Create(FLisId, ARequest, FDistributor);
        ARequest := nil;
        end;
      'threads':
        begin
        Result := TFpDebugThreadDABThreadsCommand.Create(FLisId, ARequest, FDistributor);
        ARequest := nil;
        end;
      'configurationDone':
        begin
        Result := TFpDebugThreadDABConfigurationDoneCommand.Create(FLisId, ARequest, FDistributor);
        ARequest := nil;
        end;
      'continue':
        begin
        Result := TFpDebugThreadDABContinueCommand.Create(FLisId, ARequest, FDistributor);
        ARequest := nil;
        end;
      'next':
        begin
        Result := TFpDebugThreadDABNextRequestCommand.Create(FLisId, ARequest, FDistributor);
        ARequest := nil;
        end;
      'stepIn':
        begin
        Result := TFpDebugThreadDABStepInRequestCommand.Create(FLisId, ARequest, FDistributor);
        ARequest := nil;
        end;
      'stepOut':
        begin
        Result := TFpDebugThreadDABStepOutRequestCommand.Create(FLisId, ARequest, FDistributor);
        ARequest := nil;
        end;
      'scopes':
        begin
        ScopeRequest := FSerializer.specialize CreateObjectFromJSONString<TDABScopesRequest>(ACommandText);
        Result := TFpDebugThreadDABScopesCommand.Create(FLisId, ScopeRequest, FDistributor);
        end;
      'disconnect':
        begin
        Result := TFpDebugThreadDABDisconnectCommand.Create(FLisId, ARequest, FDistributor);
        ARequest := nil;
        end;
      'stackTrace':
        begin
        StackTraceRequest := FSerializer.specialize CreateObjectFromJSONString<TDABStackTraceRequest>(ACommandText);
        Result := TFpDebugThreadDABStackTraceCommand.Create(FLisId, StackTraceRequest, FDistributor);
        end;
      'variables':
        begin
        VariablesRequest := FSerializer.specialize CreateObjectFromJSONString<TDABVariablesRequest>(ACommandText);
        Result := TFpDebugThreadDABVariablesCommand.Create(FLisId, VariablesRequest, FDistributor);
        end;
      'setVariable':
        begin
        SetVariableRequest := FSerializer.specialize CreateObjectFromJSONString<TDABSetVariableRequest>(ACommandText);
        Result := TFpDebugThreadDABSetVariableCommand.Create(FLisId, SetVariableRequest, FDistributor);
        end;
      'pause':
        begin
        Result := TFpDebugThreadDABPauseCommand.Create(FLisId, ARequest, FDistributor);
        ARequest := nil;
        end;
      'evaluate':
        begin
        EvaluateRequest := FSerializer.specialize CreateObjectFromJSONString<TDABEvaluateRequest>(ACommandText);
        Result := TFpDebugThreadDABEvaluateCommand.Create(FLisId, EvaluateRequest, FDistributor);
        end
      else
        begin
        Event := TDCSNotificationEvent.Create;
        try
          Event.Command := ARequest.Command;
          Event.UID := ARequest.Seq;
          Event.Message := Format('DAB %s requests are not supported', [ARequest.Command]);
          Event.NotificationType := ntInvalidCommand;
          FDistributor.SendEvent(Event);
        finally
          Event.Release;
        end;
        Result := nil;
        end;
    end;
  finally
    ARequest.Free;
  end;
end;

constructor TDABInOutputProcessor.create(ALisId: integer; ADistributor: TDCSDistributor);
begin
  inherited create(ALisId, ADistributor);
  FSerializer := TJSONRttiStreamClass.Create();
  FSerializer.Describer.DefaultImportNameStyle := tcsinsCaseInsensitive;
  FSerializer.Describer.DefaultExportNameStyle := tcsensLowerCaseFirstChar;
  FSerializer.Describer.Flags := [tcsdfCreateClassInstances, tcsdfDynamicClassDescriptions];
  FSerializer.DescriptionStore.GetDescription(TDABResponse).Properties.FindByPropertyName('Body').OnGetSubjectDescription := @GetDynamicBodyDescription;
end;

destructor TDABInOutputProcessor.Destroy;
begin
  FSerializer.Free;
  inherited Destroy;
end;

function TDABInOutputProcessor.GetDynamicBodyDescription(AnDescription: TcsStreamDescription;
  AnInstance: TObject): TcsStreamDescription;
begin
  Result := FSerializer.DescriptionStore.GetDescription(AnInstance.ClassType);
end;

end.

