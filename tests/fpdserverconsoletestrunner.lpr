program fpdserverconsoletestrunner;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  consoletestrunner,
  dcshandler,
  DebugThreadDABCommand,
  // junittestreport is part of fpc 3.3.1 and above, but included here so it is
  // possible to generate JUnit-style output with fpc 3.2.2
  junittestreport, fpcunitreport,
{$ifdef UsePasSrc}
  debugexpressionparser,
{$endif}
  DABTests,
  FunctionCallTests,
  DebuggingLibrariesTests,
  expressiontests;

type

  { TJUnitTestRunner }

  TJUnitTestRunner = class(TTestRunner)
  protected
    function GetResultsWriter: TCustomResultsWriter; override;
  end;

{ TJUnitTestRunner }

function TJUnitTestRunner.GetResultsWriter: TCustomResultsWriter;
begin
  Result := TJUnitResultsWriter.Create(nil);
end;

var
  Application: TTestRunner;

begin
  Application := TJUnitTestRunner.Create(nil);
  Application.Initialize;
  Application.Title := 'FPCUnit Console test runner';
  Application.Run;
  Application.Free;
end.
