program fpdserverguitestrunner;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Interfaces,
  Forms,
  GuiTestRunner,
  DABTests,
{$ifdef UsePasSrc}
  debugexpressionparser,
{$endif}
  FunctionCallTests,
  expressiontests,
  DebuggingLibrariesTests;

{$R *.res}

begin
  Application.Title:='FPDServerGuiTestRunner';
  Application.Initialize;
  Application.CreateForm(TGuiTestRunner, TestRunner);
  Application.Run;
end.

