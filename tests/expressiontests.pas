unit ExpressionTests;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  fpcunit,
  testregistry,
  FileUtil,
  DABTests;

type

  { TDABTestExpressions }

  TDABTestExpressions = class(TDABAbstractTestCase)
  published
    procedure TestBasicOperators;
    procedure TestExpressionsWithIdentifiers;
  end;

implementation

procedure TDABTestExpressions.TestBasicOperators;
var
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testbasictypes') + GetExeExt + '"},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set a breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testbasictypes.pp","path":"' + ExpandTestApplicationsPath('testbasictypes.pp') + '"},"breakpoints":[{"line":16}]},"type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', 3, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":4}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 4, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Evaluation/expression tests
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":5, "arguments": {"expression": "1+2"}}');
  CheckReceiveEvaluateResponse(5000, 5, '3', '');

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":5, "arguments": {"expression": "1-2"}}');
  CheckReceiveEvaluateResponse(5000, 5, '-1', '');

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":5, "arguments": {"expression": "3*5"}}');
  CheckReceiveEvaluateResponse(5000, 5, '15', '');

  //FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":5, "arguments": {"expression": "9 div 2"}}');
  //CheckReceiveEvaluateResponse(5000, 5, '4', '');

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":5, "arguments": {"expression": "''hallo'' + 15"}}');
  CheckReceiveBasicResponse(5000, 'evaluate', 5, False, 'Incompatible types');

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":7, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 7, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestExpressions.TestExpressionsWithIdentifiers;
var
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testbasictypes') + GetExeExt + '"},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set a breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testbasictypes.pp","path":"' + ExpandTestApplicationsPath('testbasictypes.pp') + '"},"breakpoints":[{"line":19}]},"type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', 3, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":4}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 4, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Evaluation/expression tests
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":5, "arguments": {"expression": "b"}}');
  CheckReceiveEvaluateResponse(5000, 5, '42', 'Byte');

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":5, "arguments": {"expression": "b+1"}}');
  CheckReceiveEvaluateResponse(5000, 5, '43', '');

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":5, "arguments": {"expression": "2+b"}}');
  CheckReceiveEvaluateResponse(5000, 5, '44', '');

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":7, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 7, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

initialization
  RegisterTest(TDABTestExpressions);
end.

