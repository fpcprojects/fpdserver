unit DebuggingLibrariesTests;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  fpcunit,
  FileUtil,
  fpjson,
  testregistry,
  DABTests;

type

  { TDABTestDebuggingLibraries }

  TDABTestDebuggingLibraries= class(TDABAbstractTestCase)
  protected
    procedure CheckReceiveModuleEvent(
      ATimeout: Cardinal;
      AReason: string;
      AModuleName: string);
    procedure CheckReceiveBreakpointEvent(
      ATimeout: Cardinal;
      AReason: string;
      ABreakpointId: integer;
      AVerified: Boolean);
  published
    procedure TestModuleEvent;
    procedure TestModuleBreakpoints;
  end;

implementation

{ TDABTestDebuggingLibraries }

procedure TDABTestDebuggingLibraries.CheckReceiveModuleEvent(
  ATimeout: Cardinal; AReason: string; AModuleName: string);
var
  JSObject,
  JSBody,
  JSModule: TJSONObject;
begin
  JSObject := ObtainResponse(ATimeout, 'module');
  try
    PerformBasicEventTests(JSObject, 'module');
    JSBody := JSObject.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');
    CheckEquals(AReason, JSBody.Get('reason'), 'Invalid reason for module-event');
    JSModule := JSBody.Get('module', TJSONObject(nil));
    Check(Assigned(JSBody), 'body has no module');
    CheckEquals(AModuleName, JSModule.Get('name'), 'Invalid module name');
  finally
    JSObject.Free;
  end;
end;

procedure TDABTestDebuggingLibraries.CheckReceiveBreakpointEvent(
  ATimeout: Cardinal; AReason: string; ABreakpointId: integer; AVerified: Boolean);
var
  JSObject,
  JSBody,
  JSBreakpoint: TJSONObject;
begin
  JSObject := ObtainResponse(ATimeout, 'breakpoint');
  try
    PerformBasicEventTests(JSObject, 'breakpoint');
    JSBody := JSObject.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');
    CheckEquals(AReason, JSBody.Get('reason'), 'Invalid reason for module-event');
    JSBreakpoint := JSBody.Get('breakpoint', TJSONObject(nil));
    Check(Assigned(JSBody), 'body has no breakpoint');
    CheckEquals(ABreakpointId, JSBreakpoint.Get('id'), 'Invalid breakpoint id');
    CheckEquals(AVerified, JSBreakpoint.Get('verified'), 'Invalid value for verified-flag');
  finally
    JSObject.Free;
  end;
end;

procedure TDABTestDebuggingLibraries.TestModuleEvent;
var
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testlibraryrunner-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' + ExpandTestApplicationsPath('testlibraryrunner') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set a breakpoint before the first library has been loaded
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testlibraryrunner.pp","path":"' + ExpandTestApplicationsPath('testlibraryrunner.pp') + '"},"breakpoints":[{"line":37}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint before the dynamic-modules are loaded
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Step one line, the first library is loaded...
  FDebugListener.SendCommand('{"command":"next","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'next', GetCurrentReqSeq, True);

  CheckReceiveModuleEvent(5000, 'new', {$ifndef windows}'lib'+{$endif}'testlibrary1' + {$ifdef windows}'.dll'{$else}'.so'{$endif});

  CheckReceiveStoppedEvent(5000, ThreadId, 'step', 'Single stepping');

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  // Check event for second library
  {$ifdef windows}
  // On Windows, when the library is bein relocated, the module is unloaded and
  // loaded again...
  CheckReceiveModuleEvent(5000, 'new', 'testlibrary2' + {$ifdef windows}'.dll'{$else}'.so'{$endif});
  CheckReceiveModuleEvent(5000, 'removed', 'testlibrary2' + {$ifdef windows}'.dll'{$else}'.so'{$endif});
  {$endif}
  CheckReceiveModuleEvent(5000, 'new', {$ifndef windows}'lib'+{$endif}'testlibrary2' + {$ifdef windows}'.dll'{$else}'.so'{$endif});

  // Ignore console-output
  IgnoreIncomingEventsOfType(5000, ['output']);

  // Clean-up
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestDebuggingLibraries.TestModuleBreakpoints;
var
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testlibraryrunner-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' + ExpandTestApplicationsPath('testlibraryrunner') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set breakpoints inside both libraries
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testlibrary1.pp","path":"' + ExpandTestApplicationsPath('testlibrary1.pp') + '"},"breakpoints":[{"line":9}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testlibrary2.pp","path":"' + ExpandTestApplicationsPath('testlibrary2.pp') + '"},"breakpoints":[{"line":9}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // We should receive a breakpoint-update for the first module
  CheckReceiveBreakpointEvent(5000, 'changed', 1, True);

  // We should receive a breakpoint-update for the second module
  IgnoreIncomingEventsOfType(5000, ['module']);
  CheckReceiveBreakpointEvent(5000, 'changed', 2, True);

  {$ifdef windows}
  // On Windows, the second module is unloaded and loaded again due to relocation.
  // The breakpoint is unset and set again.
  IgnoreIncomingEventsOfType(5000, ['module']);
  CheckReceiveBreakpointEvent(5000, 'changed', 2, False);
  IgnoreIncomingEventsOfType(5000, ['module']);
  CheckReceiveBreakpointEvent(5000, 'changed', 2, True);
  {$endif}

  // Stop at first breakpoint in library 1
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  // Ignore console-output

  // ToDo: Remove these sleeps. The problem is that the console-output is gathered
  // in another thread. That could delay the output-events in such a way that they
  // are received *after* a breakpoint event. In other words: it looks like console-
  // output is received while the debuggee is not running at all. (At leat on Linux)
  // To fix this there's a need for some kind of synchronisation, probably.
  // Btw: these sleeps reduces the chance that the problem occurs, but it is not
  // always gone.
  sleep(1000);
  IgnoreIncomingEventsOfType(5000, ['output']);

  // Stop at second breakpoint in library 2
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  // Ignore console-output
  sleep(1000);
  IgnoreIncomingEventsOfType(5000, ['output']);

  // Clean-up
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

initialization
  RegisterTest(TDABTestDebuggingLibraries);
end.

