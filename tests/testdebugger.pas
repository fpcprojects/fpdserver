unit TestDebugger;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  syncobjs,
  dcsHandler,
  dcsInOutputProcessor,
  FpDbgDwarf,
  FpDbgDwarfFreePascal,
  lazCollections,
  debugthread;

type

  { TTestDebugListener }

  TTestDebugListenerEventQueue = specialize TLazThreadedQueue<string>;
  TTestDebugListener = class(TObject, IDCSListener)
  private
    FDistributor: TDCSDistributor;
    FDescription: string;
    FListenerId: Integer;
    FInOutputProcessor: TDCSCustomInOutputProcessor;
    FEventQueue: TTestDebugListenerEventQueue;

    function GetListenerId: Integer;
  public
    constructor Create(ADistributor: TDCSDistributor; AnInOutputProcessorClass: TDCSInOutputProcessorClass; ADescription: string);
    destructor Destroy; override;

    procedure InitListener(AListenerId: Integer);
    procedure SendEvent(AnEvent: TDCSEvent);
    function GetOrigin: string;

    property ListenerId: Integer read GetListenerId;

    procedure SendCommand(ACommandStr: string);
    function WaitForEvent(out EventStr: string; Timeout: Cardinal): TWaitResult;
  end;

  { TTestDebugger }

  TTestDebugger = class
  private
    FDistributor: TDCSDistributor;
    FDebugThread: TFpDebugThread;
    function GetDistributor: TDCSDistributor;
  public
    procedure StartDebugger;
    procedure StopDebugger;

    property Distributor: TDCSDistributor read GetDistributor;
  end;


implementation

{ TTestDebugListener }

constructor TTestDebugListener.Create(ADistributor: TDCSDistributor; AnInOutputProcessorClass: TDCSInOutputProcessorClass; ADescription: string);
var
  LisId: Integer;
begin
  FDistributor := ADistributor;
  FDescription := ADescription;

  FEventQueue := TTestDebugListenerEventQueue.create(10);

  LisId := FDistributor.AddListener(Self);
  FInOutputProcessor := AnInOutputProcessorClass.create(LisId, FDistributor);

  FDistributor.SetEventsForListener(Self, reAll);
  FDistributor.SetNotificationEventsForListener(Self, reAll, [ntExecutedCommand, ntInvalidCommand, ntFailedCommand]);
end;

function TTestDebugListener.GetListenerId: Integer;
begin
  Result := FListenerId;
end;

function TTestDebugListener.GetOrigin: string;
begin
  Result := FDescription;
end;

procedure TTestDebugListener.InitListener(AListenerId: Integer);
begin
  FListenerId := AListenerId;
end;

procedure TTestDebugListener.SendEvent(AnEvent: TDCSEvent);
var
  EventStr: string;
begin
  // Some events (such as the event that a new listener (this listener itself)) is
  // registering, will be send before the inoutput-processor is in place
  if Assigned(FInOutputProcessor) then
    begin
    EventStr := FInOutputProcessor.EventToText(AnEvent);
    if FEventQueue.PushItem(EventStr) <> wrSignaled then
      Raise Exception.Create('Problem with the test event-queue');
    //WriteLn('Received from debugger: ' + EventStr);
    end;
end;

procedure TTestDebugListener.SendCommand(ACommandStr: string);
var
  ACommand: TDCSThreadCommand;
begin
  ACommand := FInOutputProcessor.TextToCommand(ACommandStr);
  //WriteLn('Send to debugger: ' + ACommandStr);
  if assigned(ACommand) then
    FDistributor.QueueCommand(ACommand);
end;

destructor TTestDebugListener.Destroy;
begin
  FDistributor.RemoveListener(Self);

  FEventQueue.Free;
  FInOutputProcessor.Free;

  inherited Destroy;
end;

function TTestDebugListener.WaitForEvent(out EventStr: string; Timeout: Cardinal): TWaitResult;
begin
  Result := FEventQueue.PopItemTimeout(EventStr, Timeout);
end;

{ TTestDebugger }

procedure TTestDebugger.StartDebugger;
begin
  FDistributor := TDCSDistributor.Create();

  FDebugThread := TFpDebugThread.Create(FDistributor, TFpServerDbgController);
  try
    FDistributor.AddHandlerThread(FDebugThread);
  except
    FDebugThread.Free;
    raise;
  end;
end;

procedure TTestDebugger.StopDebugger;
begin
  try
    FDebugThread.Terminate;
    FDebugThread.WaitFor;
    FDebugThread.Free;
  finally
    FDistributor.Free;
    FDistributor := nil;
  end;
end;

function TTestDebugger.GetDistributor: TDCSDistributor;
begin
  if not Assigned(FDistributor) then
    raise Exception.Create('The debugger has to be started first');
  Result := FDistributor;
end;


end.

