unit FunctionCallTests;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  fpjson,
  fpcunit,
  testregistry,
  DABTests;

type

  { TDABTestFunctionCalls }

  TDABAbstractTestFunctionCalls = class(TDABAbstractTestCase)
  protected
    procedure CheckFunctionCall(
      ATimeout: Cardinal;
      FunctionExpression: string;
      ResultType: string;
      ResultValue: string);
    procedure CheckFunctionCallFailure(
      ATimeout: Cardinal;
      FunctionExpression: string;
      ExpectedErrorMessage: string);
  end;

  TDABTestFunctionCalls = class(TDABAbstractTestFunctionCalls)
  published
    procedure TestByteFunctionResult;
    procedure TestVariousSimpleReturnTypes;
    procedure TestUnsupportedReturnTypes;
    procedure TestClassReturnTypes;
    procedure TestBreakpointDuringCall;
    procedure TestParameters;
    procedure TestLocalsAfterParameters;
    procedure TestInvoke;
  end;

  { TDABTestProperties }

  TDABTestProperties = class(TDABAbstractTestFunctionCalls)
  protected
  published
    procedure TestGetterProperty;
    procedure TestIndexedProperties;
  end;

implementation

{ TDABTestProperties }

procedure TDABTestProperties.TestGetterProperty;
var
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testinvoke-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testinvokemethod') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set the breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testinvokemethod.pp","path":"' + ExpandTestApplicationsPath('testinvokemethod.pp') + '"},"breakpoints":[{"line":41}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint at line 41
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Evaluate GetProp
  CheckFunctionCall(5000, 'CI.Prop', 'Byte', '42');

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestProperties.TestIndexedProperties;
var
  ThreadId: Integer;
  i: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testinvoke-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testindexedproperties') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set the breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testindexedproperties.pp","path":"' + ExpandTestApplicationsPath('testindexedproperties.pp') + '"},"breakpoints":[{"line":40}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint at line 40
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Evaluate GetProp
  CheckFunctionCall(5000, 'List.Items[i].Prop', 'LongInt', '0');

  for i := 1 to 9 do
    begin
    // Continue the loop
    FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
    CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);
    CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');
    // Stop at the same location in the loop again.

    // Evaluate GetProp
    CheckFunctionCall(5000, 'List.Items[i].Prop', 'LongInt', IntToStr(I*2));
    end;

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestFunctionCalls.TestByteFunctionResult;
var
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testinvoke-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testinvoke') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set the breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testinvoke.pp","path":"' + ExpandTestApplicationsPath('testinvoke.pp') + '"},"breakpoints":[{"line":20}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint at line 20
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Evaluate GetByte
  CheckFunctionCall(5000, 'getbyte', 'Byte', '4');
  // Evaluate GetValue
  CheckFunctionCall(5000, 'GetValue', 'LongInt', '24');
  // Evaluate i, should be 10
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "i"}}');
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '14', 'LongInt');

  // Step one line, at line 21 now
  FDebugListener.SendCommand('{"command":"next","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'next', GetCurrentReqSeq, True);
  CheckReceiveStoppedEvent(5000, ThreadId, 'step', 'Single stepping');

  // Evaluate GetValue
  CheckFunctionCall(5000, 'GetValue', 'LongInt', '34');
  // Evaluate GetByte
  CheckFunctionCall(5000, 'getbyte', 'Byte', '4');
  // Evaluate i, should be 10
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "i"}}');
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '24', 'LongInt');

  // Step one line, at line 22 now
  FDebugListener.SendCommand('{"command":"next","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'next', GetCurrentReqSeq, True);
  CheckReceiveStoppedEvent(5000, ThreadId, 'step', 'Single stepping');

  // Evaluate GetValue
  CheckFunctionCall(5000, 'GetValue', 'LongInt', '14');
  // Evaluate GetByte
  CheckFunctionCall(5000, 'getbyte', 'Byte', '4');
  // Evaluate i, should be 10
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "i"}}');
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '4', 'LongInt');

  // Step into GetByte
  FDebugListener.SendCommand('{"command":"stepIn","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'stepIn', GetCurrentReqSeq, True);
  CheckReceiveStoppedEvent(5000, ThreadId, 'step', 'Single stepping');

  // Evaluate GetValue
  CheckFunctionCall(5000, 'GetValue', 'LongInt', '14');

  // Step through GetByte
  FDebugListener.SendCommand('{"command":"stepIn","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'stepIn', GetCurrentReqSeq, True);
  CheckReceiveStoppedEvent(5000, ThreadId, 'step', 'Single stepping');

  // Evaluate GetValue
  CheckFunctionCall(5000, 'GetValue', 'LongInt', '14');

  // Step through GetByte
  FDebugListener.SendCommand('{"command":"stepIn","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'stepIn', GetCurrentReqSeq, True);
  CheckReceiveStoppedEvent(5000, ThreadId, 'step', 'Single stepping');

  // Evaluate GetValue
  CheckFunctionCall(5000, 'GetValue', 'LongInt', '14');

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABAbstractTestFunctionCalls.CheckFunctionCall(
  ATimeout: Cardinal;
  FunctionExpression: string;
  ResultType: string;
  ResultValue: string);
var
  VariablesReference: Integer;
  JSVariables: TJSONArray;
begin
  // Evaluate expression
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "'+FunctionExpression+'"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(ATimeout, GetCurrentReqSeq, 'function', '', VariablesReference);

  // Call function
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(ATimeout, GetCurrentReqSeq, 1, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('Result', ResultValue, ResultType, VariablesReference, JSVariables.Items[0]);
  finally
    JSVariables.Free;
  end;
end;

procedure TDABTestFunctionCalls.TestVariousSimpleReturnTypes;
var
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testinvoke-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testinvokereturntypes') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set the breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testinvokereturntypes.pp","path":"' + ExpandTestApplicationsPath('testinvokereturntypes.pp') + '"},"breakpoints":[{"line":12}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at breakpoint at line 12
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Evaluate function-results
  {$ifdef UsePasSrc}
  CheckFunctionCall(5000, 'GetByte()', 'Byte', '4');
  {$else}
  CheckFunctionCall(5000, 'GetByte', 'Byte', '4');
  {$endif UsePasSrc}
  CheckFunctionCall(5000, 'GetInteger', 'LongInt', '1234');
  // Disabled, because debugging the Currency type does not work well at all.
  //CheckFunctionCall(5000, 9, 'GetCurrency', 'Currency', '100.20');
  CheckFunctionCall(5000, 'GetPointer', 'Pointer', 'Pointer\(\$0000000000127642\)');
  CheckFunctionCall(5000, 'GetCardinal', 'LongWord', '4294957295');
  CheckFunctionCall(5000, 'GetBooleanFalse', 'Boolean', 'False');
  CheckFunctionCall(5000, 'GetBooleanTrue', 'Boolean', 'True');
  CheckFunctionCall(5000, 'GetChar', 'Char', '''A''');
  // The type-definition of an Enum is flakey
  CheckFunctionCall(5000, 'GetEnum', 'TValueType', 'vaFalse');

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestFunctionCalls.TestUnsupportedReturnTypes;
var
  ThreadId, VariablesReference: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the testinvoke-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testinvokereturntypes') + GetExeExt + '"},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set the breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testinvokereturntypes.pp","path":"' + ExpandTestApplicationsPath('testinvokereturntypes.pp') + '"},"breakpoints":[{"line":12}]},"type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', 3, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":4}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 4, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at breakpoint at line 12
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // String return types are not supported
  // Evaluate expression
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":5, "arguments": {"expression": "GetString"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, 5, 'function', '', VariablesReference);

  // Call function
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":6, "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveBasicResponse(5000, 'variables', 6, False, 'Unable to call function [GetString]. The type of the function result is not supported');

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":31, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 31, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestFunctionCalls.TestClassReturnTypes;
var
  ThreadId, VariablesReference: Integer;
  JSVariables: TJSONArray;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the testinvoke-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testinvokereturntypes') + GetExeExt + '"},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set the breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testinvokereturntypes.pp","path":"' + ExpandTestApplicationsPath('testinvokereturntypes.pp') + '"},"breakpoints":[{"line":12}]},"type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', 3, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":4}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 4, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at breakpoint at line 12
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Evaluate expression
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":5, "arguments": {"expression": "GetClassInstance"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, 5, 'function', '', VariablesReference);

  // Call function
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":6, "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, 6, 1, JSVariables);
  try
    VariablesReference := -1;
    // ToDo: It does not only return the class-type, but the complete definition.
    // That is not what you want...
    CheckVariable('Result', 'TComponent \(\$[0-9A-F]{16}\)', 'TComponent', VariablesReference, JSVariables.Items[0]);
    Check(VariablesReference > 0, 'Variablesreference may not be empty');
  finally
    JSVariables.Free;
  end;

  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":8, "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, 8, 18, JSVariables);
  try
    VariablesReference := 0;
    // This check is not entirely correct, we should check the version of the
    // compiler used to compile the debugee (testinvokereturntypes). We assume
    // here that these versions match...
    // FPC had a bug that generated incorrect accesibility debug information so
    // the order of the class-entries is messed up.
    {$IF FPC_FULLVERSION>=30301}
    CheckVariable('TComponent.ComponentState', '\[\]', 'TComponentState', VariablesReference, JSVariables.Items[0]);
    CheckVariable('TComponent.Tag', '5353', 'Int64', VariablesReference, JSVariables.Items[6]);
    CheckVariable('TObject._vptr$TOBJECT', 'Pointer\(\$[0-9A-F]{16}\)', 'Pointer', VariablesReference, JSVariables.Items[17]);
    {$else}
    CheckVariable('TComponent.FComponentStyle', '\[csInheritable\]', 'TComponentStyle', VariablesReference, JSVariables.Items[0]);
    CheckVariable('TComponent.Tag', '5353', 'Int64', VariablesReference, JSVariables.Items[15]);
    CheckVariable('TObject._vptr$TOBJECT', 'Pointer\(\$[0-9A-F]{16}\)', 'Pointer', VariablesReference, JSVariables.Items[17]);
    {$endif}
  finally
    JSVariables.Free;
  end;

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":31, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 31, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestFunctionCalls.TestBreakpointDuringCall;
var
  ThreadId, VariablesReference: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testinvoke-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testinvoke') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set the breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testinvoke.pp","path":"' + ExpandTestApplicationsPath('testinvoke.pp') + '"},"breakpoints":[{"line":20},{"line":10}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint at line 20
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Evaluate GetValue
  CheckFunctionCall(5000, 'GetValue', 'LongInt', '24');
  // Evaluate i, should be 14
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "i"}}');
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '14', 'LongInt');

  // There is a breakpoint in GetByte. Calling it should fail.
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "GetByte"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, 'function', '', VariablesReference);
  // Call function
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveBasicResponse(5000, 'variables', GetCurrentReqSeq, False, 'The function stopped unexpectedly. (Breakpoint, Exception, etc)');

  // Evaluate i again, should still be 14
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "i"}}');
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '14', 'LongInt');

  // Step one line, at line 21 now
  FDebugListener.SendCommand('{"command":"next","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'next', GetCurrentReqSeq, True);
  CheckReceiveStoppedEvent(5000, ThreadId, 'step', 'Single stepping');

  // Evaluate i again, should now be 24
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "i"}}');
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '24', 'LongInt');

  // Continue twice, should stop at the breakpoint in GetByte twice
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Continue twice, should stop at the breakpoint in GetByte twice
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');


  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestFunctionCalls.TestParameters;
var
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testinvoke-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testinvokeparameters') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set the breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testinvokeparameters.pas","path":"' + ExpandTestApplicationsPath('testinvokeparameters.pas') + '"},"breakpoints":[{"line":16}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at breakpoint at line 12
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Check invalid parameter count
  CheckFunctionCallFailure(5000, 'GetByte()', 'Unable to call function [GetByte]. Not enough parameters supplied.');
  CheckFunctionCallFailure(5000, 'GetByte(1,2)', 'Unable to call function [GetByte]. Too many parameters provided. Expected 1, got 2.');

  // Simple byte-values
  CheckFunctionCall(5000, 'GetByte(15)', 'Byte', '20');
  CheckFunctionCall(5000, 'GetByte(44)', 'Byte', '49');
  CheckFunctionCall(5000, 'GetByte(44)+1', '', '50');
  CheckFunctionCall(5000, 'GetByte(1)*GetByte(2)', '', '42');

  // Class-parameters
  CheckFunctionCall(5000, 'GetTag(C)', 'Int64', '123');

  // Multiple parameters
  CheckFunctionCall(5000, 'SetTag(C,4372)', 'Int64', '123');
  CheckFunctionCall(5000, 'GetTag(C)', 'Int64', '4372');

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABAbstractTestFunctionCalls.CheckFunctionCallFailure(ATimeout: Cardinal; FunctionExpression: string; ExpectedErrorMessage: string);
var
  VariablesReference: Integer;
begin
  // Evaluate expression
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":' +GetNextReqSeq+ ', "arguments": {"expression": "' +FunctionExpression + '"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(ATimeout, GetCurrentReqSeq, 'function', '', VariablesReference);

  // Call function
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveBasicResponse(5000, 'variables', GetCurrentReqSeq, False, ExpectedErrorMessage);
end;

procedure TDABTestFunctionCalls.TestLocalsAfterParameters;

// This test has been added te test reference-counting issues after
// a function-call has been made.

var
  ThreadId, StackFrameId: Integer;
  LocalsVariablesReference, ArgumentsVariablesReference: Integer;
  JSVariables: TJSONArray;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testinvoke-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testinvokeparameters') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set the breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testinvokeparameters.pas","path":"' + ExpandTestApplicationsPath('testinvokeparameters.pas') + '"},"breakpoints":[{"line":16}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at breakpoint at line 12
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  CheckFunctionCall(5000, 'GetTag(C)', 'Int64', '123');

  // Check locals
  StackFrameId := GetCurrentStackFrameId(ThreadId);
  GetLocalsReference(StackFrameId, LocalsVariablesReference, ArgumentsVariablesReference);

  // Class-parameters
  CheckFunctionCall(5000, 'GetTag(C)', 'Int64', '123');

  // Get the procedure-arguments
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(ArgumentsVariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 0, JSVariables);
  JSVariables.Free;

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestFunctionCalls.TestInvoke;
var
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testinvoke-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testinvokemethod') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set the breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testinvokemethod.pp","path":"' + ExpandTestApplicationsPath('testinvokemethod.pp') + '"},"breakpoints":[{"line":41}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint at line 41
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Evaluate GetByte
  CheckFunctionCall(5000, 'CI.GetByte', 'Byte', '55');

  // Evaluate GetVal
  CheckFunctionCall(5000, 'CI.GetVal()', 'Byte', '124');

  // Evaluate GetProp
  CheckFunctionCall(5000, 'CI.GetProp()', 'Byte', '42');


  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

initialization

  RegisterTest(TDABTestFunctionCalls);
  RegisterTest(TDABTestProperties);
end.

