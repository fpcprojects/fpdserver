# FPDebug Server tests

These are unit-tests for the FPDServer. The test-application is basically a DAB-client and a DAB-adapter (server) in one. The tests can send DAB-commands and check the responses and received events.

## Getting started

There is a GUI-based testrunner project. Just open and run it in Lazarus. For the
tests to work, though, the test application have to be compiled.

### Compile the test-applications

Here's a list of compile-commands for applications that can be either 32 or 64
bit. Make sure that testapplications is the current directory first:

     fpc -gw3 helloworld.pp
     fpc -gw3 testparams.pp
     fpc -gw3 simpleproc.pp
     fpc -gw3 teststrings.pp
     fpc -gw3 pascalexception.pp
     fpc -gw3 systemexception.pp
     fpc -gw3 testclass.pp
     fpc -gw3 testarray.pp
     fpc -gw3 testbasictypes.pp
     fpc -gw3 testsetvariable.pp
     fpc -gw3 testinvokemethod.pp
     fpc -gw3 testindexedproperties.pp
     fpc -gw3 testinvoke.pp
     fpc -gw3 testinvokereturntypes.pp
     fpc -gw3 testlibrary1.pp
     fpc -gw3 testlibrary2.pp
     fpc -gw3 testlibraryrunner.pp
