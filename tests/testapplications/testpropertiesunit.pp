unit TestPropertiesUnit;

{$mode objfpc}{$H+}

interface

type
  TMyClassOtherUnit=class
  protected
    FOUProp: Integer;
  public
    constructor Create(P: Integer);
    function GetOUProp: Integer;
  published
    property OUDoubleProp: Integer read GetOUProp;
    property OUSingleProp: Integer read FOUProp;
  end;

  TUnusedMyChildClassOtherUit=class(TMyClassOtherUnit)
  public
    property UselessProp: Integer read FOUProp;
  end;

  TMyChildClassOtherUit=class(TMyClassOtherUnit)
  public
    property OUParentProp: Integer read FOUProp;
  end;

implementation

function TMyClassOtherUnit.GetOUProp: Integer;
begin
  Result := FOUProp*3;
end;

constructor TMyClassOtherUnit.Create(P: Integer);
begin
  FOUProp := P;
end;

end.

