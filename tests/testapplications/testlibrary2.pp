library TestLibrary2;

var
  GlobalVar: string;

function SomeFunction(ANumber: Integer): Integer; export; stdcall;
begin
  SomeFunction := ANumber * 3;
  GlobalVar := 'Bye';
end;

exports
  SomeFunction;
end.