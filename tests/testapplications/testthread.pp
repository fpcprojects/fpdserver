program TestThread;

{$mode objfpc}{$H+}

uses
{$ifdef unix}
  cthreads,
{$endif}
  sysutils,
  Classes;

procedure InThread(AData : Pointer);
var
  i: Integer;
begin
  i := Random(10);
end;

begin
  TThread.ExecuteInThread(@InThread);
  sleep(1000);
end.
