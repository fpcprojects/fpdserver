program TestClass;

{$mode objfpc}{$H+}

type
  TMyChildClass = class
  private
    FField1: string;
  public
    property Field1: string read FField1 write FField1;
  end;

  TMyClass = class
  private
    FIntField: Integer;
    FChild: TMyChildClass;
  public
    constructor Create;
    destructor Destroy; override;
    property Child: TMyChildClass read FChild;
    property IntField: Integer read FIntField write FIntField;
  end;

constructor TMyClass.Create();
begin
  FChild := TMyChildClass.Create;
end;

destructor TMyClass.Destroy;
begin
  FChild.Free;
  inherited Destroy;
end;

var
  ChildInst: TMyChildClass;
  Inst: TMyClass;

begin
  ChildInst := TMyChildClass.Create;
  ChildInst.Field1 := 'Field1';
  ChildInst.Free;

  Inst := TMyClass.Create;
  Inst.IntField := 123;
  Inst.Child.Field1 := 'ChildField1';
  Inst.Free;
end.
