program TestLibraryRunner;

{$mode objfpc}
{$ifdef unix}
uses
  sysutils,
  dynlibs;

const
  SharedLibExt = '.so';
  SharedLibPrefix = 'lib';
{$else}
const
  SharedLibExt = '.dll';
  SharedLibPrefix = '';
{$endif}


type
  TSomeFunctionProc = function (ANumber: Integer): Integer; stdcall;

var
  HandleA: TLibHandle;
  HandleB: TLibHandle;
  SomeFuncA: TSomeFunctionProc;
  SomeFuncB: TSomeFunctionProc;
  Prefix: string;
  i: Integer;
begin
  {$ifdef unix}
  // On unix we need all this stuff, because the OS does not search for libraries
  // in the application path by default.
  Prefix := ConcatPaths([IncludeTrailingPathDelimiter(ExtractFileDir(ParamStr(0))), SharedLibPrefix]);
  {$else}
  Prefix := SharedLibPrefix;
  {$endif}
  HandleA := LoadLibrary(Prefix + 'testlibrary1'+SharedLibExt);
  if HandleA=0 then
    begin
    writeln('Failed to load first library');
    Halt(1);
    end;
  HandleB := LoadLibrary(Prefix + 'testlibrary2'+SharedLibExt);
  if HandleB=0 then
    begin
    writeln('Failed to load second library');
    Halt(2);
    end;
  SomeFuncA := TSomeFunctionProc(GetProcedureAddress(HandleA, 'SomeFunction'));
  if not Assigned(SomeFuncA) then
    begin
    writeln('someFunction in library 1 not found');
    Halt(3);
    end;
  SomeFuncB := TSomeFunctionProc(GetProcedureAddress(HandleB, 'SomeFunction'));
  if not Assigned(SomeFuncB) then
    begin
    writeln('someFunction in library 2 not found');
    Halt(4);
    end;
  i := SomeFuncA(120);
  WriteLn(i);
  i := SomeFuncB(120);
  WriteLn(i);
end.
