program TestIndexedProperties;

{$mode objfpc}{$H+}

uses
  Generics.Collections;

type
  TMyClass=class
  private
    FProp: Integer;
  public
    constructor Create(P: Integer);
    function GetProp: Integer;
  published
    property Prop: Integer read GetProp;
  end;
  TMyClassList = specialize TObjectList<TMyClass>;

function TMyClass.GetProp: Integer;
begin
  Result := FProp*2;
end;

constructor TMyClass.Create(P: Integer);
begin
  FProp := P;
end;

var
  List: TMyClassList;
  i: Integer;
  a: Integer;
begin
  List := TMyClassList.Create(True);
  try
    for I := 0 to 9 do
      begin
      List.Add(TMyClass.Create(i));
      a := List.Items[i].Prop;
      end;
  finally
    List.Free;
  end;
end.

