program SimpleProc;

procedure SecondTestProc(Argument: Integer);
var
  a,b: Integer;
begin
  a := Argument;
  b := a * 2;
  inc(a);
end;

procedure FirstTestProc(Argument: Integer);
begin
  SecondTestProc(20);
end;

begin
  FirstTestProc(10);
end.
