program PascalException;

{$mode objfpc}{$H+}

uses
  sysutils;

procedure ExceptionProcedure();
begin
  raise Exception.Create('This is a Pascal exception');
end;

begin
  ExceptionProcedure();
end.
