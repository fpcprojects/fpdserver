program TestInvokeReturnTypes;

{$mode objfpc}{$H+}

uses
  classes;

procedure SomeProcedure();
var
  i: Integer;
begin
  i := 123;
end;

function GetByte: byte;
begin
  Result := 4;
end;

function GetInteger: Integer;
begin
  Result := 1234;
end;

function GetCurrency: Currency;
begin
  Result := 100.20;
end;

function GetPointer: Pointer;
begin
  Result := pointer($127642);
end;

function GetCardinal: Cardinal;
begin
  Result := 4294957295;
end;

function GetBooleanFalse: Boolean;
begin
  Result := False;
end;

function GetBooleanTrue: Boolean;
begin
  Result := True;
end;

function GetChar: Char;
begin
  Result := 'A';
end;

function GetEnum: TValueType;
begin
  Result := vaFalse;
end;

function GetString: string;
begin
  Result := 'String';
end;

function GetClassInstance: TComponent;
begin
  Result := TComponent.Create(nil);
  Result.Tag := 5353;
end;

begin
  SomeProcedure;
end.
