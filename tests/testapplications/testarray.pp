program TestArray;

{$mode objfpc}{$H+}

procedure StaticArray;
var
  Arr: Array[0..5] of byte;
begin
  Arr[0] := 10;
  Arr[1] := 1;
  Arr[2] := 2;
  Arr[3] := 3;
  Arr[4] := 4;
  Arr[5] := 5;
end;

procedure StaticArrayWithOffset;
var
  Arr: Array[4..9] of char;
begin
  Arr[4] := 'h';
  Arr[5] := 'e';
  Arr[6] := 'l';
  Arr[7] := 'l';
  Arr[8] := 'o';
  Arr[9] := '!';
end;

procedure HugeStaticArrayWithOffset;
var
  Arr: Array[16..1012] of word;
  i: word;
begin
  for i := 16 to 1012 do
    Arr[i] := i;
end;

procedure StaticArrayMultipleDimensions;
var
  Arr: Array[0..5,3..6] of integer;
  i,j: integer;
begin
  for i := 0 to 5 do
    for j := 3 to 6 do
      Arr[i,j] := i*100+j;
end;

procedure DynamicArray;
var
  Arr: Array of byte;
begin
  SetLength(Arr, 5);
  Arr[0] := 40;
  Arr[1] := 30;
  Arr[2] := 20;
  Arr[3] := 10;
  Arr[4] := 0;
end;

procedure DynamicArrayHuge;
var
  Arr: Array of byte;
begin
  SetLength(Arr, 100000);
  Arr[0] := 1;
  Arr[1] := 2;
  Arr[2] := 4;
  Arr[3] := 8;
  Arr[4] := 16;
  Arr[99996] := 32;
  Arr[99997] := 64;
  Arr[99998] := 128;
  Arr[99999] := 255;
end;

procedure DynamicArrayOfString;
var
  Arr: Array of string;
begin
  SetLength(Arr, 3);
  Arr[0] := 'string 0';
  Arr[1] := 'string  1';
  Arr[2] := 'string   2';
end;

begin
  StaticArray;
  StaticArrayWithOffset;
  HugeStaticArrayWithOffset;
  StaticArrayMultipleDimensions;
  DynamicArray;
  DynamicArrayHuge;
  DynamicArrayOfString;
end.