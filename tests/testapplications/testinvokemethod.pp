program TestInvokeMethod;

{$mode objfpc}{$H+}

type
  TMyClass=class
  private
    FVal: Byte;
    FProp: Byte;
  public
    function GetByte: Byte;
    function GetVal: Byte;
    function GetProp: Byte;
  published
    property Prop: Byte read GetProp write FProp;
  end;

function TMyClass.GetByte: Byte;
begin
  Result := 55;
end;

function TMyClass.GetVal: Byte;
begin
  Result := FVal;
end;

function TMyClass.GetProp: Byte;
begin
  Result := FProp;
end;

var
  CI: TMyClass;
  i: byte;
begin
  CI := TMyClass.Create;
  CI.FVal := 124;
  CI.Prop := 42;
  i := CI.GetByte;
end.

