program TestProperties;

{$mode objfpc}{$H+}

uses TestPropertiesUnit, sysutils, classes;

type
  TMyClass=class
  private
    FProp: Integer;
    function GetProp: Integer;
  public
    constructor Create(P: Integer);
  protected
    property DoubleProp: Integer read GetProp;
    property SingleProp: Integer read FProp write FProp;
  end;

  TMyChildClass=class(TMyClass)
  public
    property ParentProp: Integer read FProp;
  end;

  TunusedMyChildClass=class(TMyClass)
  public
    property UnusedParentProp: Integer read FProp;
  end;

  TMyOtherUnitChildClass=class(TMyClassOtherUnit)
  public
    property ParentOtherUnitProp: Integer read FOUProp;
    property ParentOtherUnitDoubleProp: Integer read GetOUProp;
  end;

function TMyClass.GetProp: Integer;
begin
  Result := FProp*2;
end;

constructor TMyClass.Create(P: Integer);
begin
  FProp := P;
end;

var
  MC: TMyClass;
  MCC: TMyChildClass;
  OUMCC: TMyOtherUnitChildClass;
begin
  MC := TMyClass.Create(154);
  WriteLn(MC.DoubleProp);
  WriteLn(MC.SingleProp);
  MC.Free;

  // Give the debugger some time to process all output-events
  sleep(500);

  MC := TMyChildClass.Create(75);
  WriteLn(MC.DoubleProp);
  WriteLn(MC.SingleProp);
  WriteLn(TMyChildClass(MC).ParentProp);
  MC.Free;

  MCC := TMyChildClass.Create(34);
  WriteLn(MCC.DoubleProp);
  WriteLn(MCC.SingleProp);
  WriteLn(MCC.ParentProp);
  MCC.Free;

  OUMCC := TMyOtherUnitChildClass.Create(12);
  WriteLn(OUMCC.OUDoubleProp);
  WriteLn(OUMCC.OUSingleProp);
  WriteLn(OUMCC.ParentOtherUnitProp);
  OUMCC.Free;
end.

