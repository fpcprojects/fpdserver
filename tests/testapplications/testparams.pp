program TestParams;

uses
  SysUtils;

procedure ShowParams;
var
  i: Integer;
begin
  for i := 1 to ParamCount do
    writeln(ParamStr(i));
end;

procedure ShowEnvironment;
var
  i: Integer;
begin
  for i := 1 to GetEnvironmentVariableCount do
    writeln(GetEnvironmentString(i));
end;

procedure ShowWorkDir;
begin
  Writeln(GetCurrentDir);
end;

begin
  // Disabled flushing after each write(ln) on Output
  // This way the output is more predictable so easier to test
  Textrec(Output).FlushFunc:=nil;
  if ParamCount < 1 then
    Writeln('Give at least one parameter');
  Flush(StdOut);
  if ParamStr(1)='-p' then
    ShowParams();
  Flush(StdOut);
  if ParamStr(1)='-e' then
    ShowEnvironment();
  Flush(StdOut);
  if ParamStr(1)='-w' then
    ShowWorkDir();
  Flush(StdOut);
end.
