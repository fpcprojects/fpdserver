program TestStrings;

{$mode objfpc}{$H+}
{$codepage utf8}

{$ifdef unix}
uses
  cwstring;
{$endif unix}

type
  TString1252 = Type String(1252);

var
  String1252: TString1252;
  SimpleString: string;

begin
  SimpleString := 'Hello World';
  String1252 := 'Héllo World';
end.
