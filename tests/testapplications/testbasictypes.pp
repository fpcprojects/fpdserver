program TestBasicTypes;

{$mode objfpc}

var
  int: Integer;
  b: Byte;
  cur: Currency;
  d: double;
  s: single;
  qw: QWord;
  st: string;

begin
  int := 123456;
  b := 42;
  cur := 10.15;
  s := 12.54;
  d := 3.45;
end.
