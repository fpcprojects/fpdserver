program TestSetVariable;

{$OPTIMIZATION REGVAR}

uses
  Classes;

type
  TMyEnum=(meOne, meTwo, meThree);

// Not all types can be tested in one procedure, due to registers being re-used
procedure LocalVariablesA;
var
  i: Integer;
  b: Byte;
  Enum: TMyEnum;
begin
  i := 13442;
  b := 12;
  Enum := meTwo;
  writeln(i);
  writeln(b);
  writeln(Enum);
end;

procedure LocalVariablesB;
var
  c: Char;
  Bool: Boolean;
begin
  c := 'S';
  Bool := True;
  if c <> 'S' then
    // Simple 'writeln(c)' does not work, because the compiler changes the register-allocation
    // in that case. But not the necessary debug-info to handle this.
    writeln('C is not S');
  if not Bool then
    writeln('Bool is false');
end;

// Not all types can be tested in one procedure, due to register pressure
procedure LocalVariablesC;
var
  Obj1: TComponent;
  Obj2: TComponent;
begin
  Obj1 := TComponent.Create(nil);
  Obj1.Tag  := 234;
  Obj2 := TComponent.Create(nil);
  Obj2.Tag := 1234;
  writeln(Obj1.Tag);
  writeln(Obj2.Tag);
end;

procedure LocalVariablesD;
var
  p: Pointer;
  pb: PByte;
  b: byte;
begin
  New(Pb);
  Pb^ := 43;
  b := 13;
  p := @b;
  writeln(Pb^);
  writeln(HexStr(P));
  writeln(Byte(P^));
end;

begin
  LocalVariablesA;
  LocalVariablesB;
  LocalVariablesC;
  LocalVariablesD;
end.
