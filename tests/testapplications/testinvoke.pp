program TestInvoke;

{$mode objfpc}{$H+}

var
  i: integer;

function GetByte: byte;
begin
  Result := 4;
end;

function GetValue: Integer;
begin
  Result := 5*2 + i;
end;

begin
  i := 14;
  i := GetValue;
  i := GetByte;
  i := GetByte;
end.
