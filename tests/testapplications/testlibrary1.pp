library TestLibrary1;

var
  GlobalVar: string;

function SomeFunction(ANumber: Integer): Integer; export; stdcall;
begin
  SomeFunction := ANumber * 2;
  GlobalVar := 'Hello';
end;

exports
  SomeFunction;
end.