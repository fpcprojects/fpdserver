program TestInvokeParameters;

{$mode objfpc}{$H+}

uses
  Classes;

procedure SomeProcedure();
var
  i: Integer;
  C: TComponent;
begin
  i := 123;
  C := TComponent.Create(nil);
  C.Tag := i;
  C.Free;
end;

function GetByte(i: byte): byte;
begin
  Result := i + 5;
end;

function GetTag(AComponent: TComponent): PtrInt;
begin
  Result := AComponent.Tag;
end;

function SetTag(AComponent: TComponent; AValue: Integer): PtrInt;
begin
  Result := AComponent.Tag;
  AComponent.Tag := AValue;
end;

begin
  SomeProcedure;
end.
