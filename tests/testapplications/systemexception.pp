program systemexception;

{$mode objfpc}{$H+}

procedure ExceptionProcedure();
var
  a,b: Integer;
  c: Double;
begin
  a := 0;
  b := 10;
  c := b / a;
end;

begin
  ExceptionProcedure();
end.
