unit DABTests;

{$mode objfpc}{$H+}
{$codepage utf8}

interface

uses
  fpwidestring,
  Classes,
  FileUtil,
  Variants,
  SysUtils,
  syncobjs,
  regexpr,
  Generics.Collections,
  fpcunit,
  testregistry,
  TestDebugger,
  fpjson,
  jsonparser,
  DABInOutputProcessor;

type

   TResponseStack = specialize TStack<TJSONData>;

  { TDABAbstractTestCase }

  TDABAbstractTestCase = class(TTestCase)
  private
    FCurrentReqSeq: Integer;
    FDefaultRequestTimeout: Integer;
    function RetrieveResponse(ATimeout: Cardinal; ADescription: string; APeek: Boolean): TJSONObject;
  protected
    FTestDebugger: TTestDebugger;
    FDebugListener: TTestDebugListener;
    FInOutputProcessor: TDABInOutputProcessor;
    FResponseCache: TResponseStack;
    procedure SetUp; override;
    procedure TearDown; override;

    function GetNextReqSeq: string;
    function GetCurrentReqSeqStr: string; overload;
    function GetCurrentReqSeq: Integer; overload;

    function ExpandTestApplicationsPath(AName: string): string;
    function ObtainResponse(ATimeout: Cardinal; Description: string): TJSONObject;
    function PeekResponse(ATimeout: Cardinal; Description: string): TJSONObject;
    procedure PerformBasicResponseTests(
      AJSONResponse: TJSONObject;
      ACommand: string;
      AReqSeq: Integer;
      ASuccess: Boolean;
      AMessage: string = '');
    procedure PerformBasicEventTests(
      AJSONEvent: TJSONObject;
      AnEvent: string);
    procedure PerformBreakpointTests(
      AJSONBreakpoint: TJSONData;
      ALine: Integer;
      AVerified: Boolean);
    procedure CheckReceiveBasicResponse(
      ATimeout: Cardinal;
      ACommand: string;
      AReqSeq: Integer;
      ASuccess: Boolean;
      AMessage: string = '');
    procedure CheckReceiveEvaluateResponse(
      ATimeout: Cardinal;
      AReqSeq: Integer;
      AResultRegExpr: string;
      AType: string;
      var AVariablesReference: Integer); overload;
    procedure CheckReceiveEvaluateResponse(
      ATimeout: Cardinal;
      AReqSeq: Integer;
      AResultRegExpr: string;
      AType: string); overload;
    procedure CheckReceiveBasicEvent(
      ATimeout: Cardinal;
      AnEvent: string);
    procedure CheckReceiveStoppedEvent(
      ATimeout: Cardinal;
      out AThreadId: Integer;
      AReason: string;
      ADescription: string;
      AText: string = '');
    procedure CheckReceiveVariablesResponse(
      ATimeout: Cardinal;
      AReqSeq: Integer;
      AVariableCount: Integer;
      out JSVariables: TJSONArray);

    procedure CheckReceiveOutputEvent(
      ATimeout: Cardinal;
      ACategory: string;
      AnOutput: string;
      APartialMatch: Boolean = False);

    procedure CheckVariable(
      AName: string;
      AValueRegExpr: string;
      AType: string;
      AJSONData: TJSONData); overload;

    procedure CheckVariable(
      AName: string;
      AValueRegExpr: string;
      AType: string;
      var AVariablesReference: Integer;
      AJSONData: TJSONData); overload;

    procedure CheckReceiveBasicEventIgnoreOthers(
      ATimeout: Cardinal;
      AnEvent: string);

    procedure IgnoreIncomingEventsOfType(
      ATimeout: Cardinal;
      AnEventArr: array of string);

    procedure GetLocalsReference(AStackFrameId: Integer; out ALocalsReference, AnArgmentsReference: Integer);
    function GetCurrentStackFrameId(AThreadId: Integer): Integer;
    // Returns a regular-expression that corresponds to a pointer, as returned
    // by FpDebug.
    function GetPointerRegExpr: string;

  public
    constructor Create; override;
  end;

  { TDABTestRunBasics }

  TDABTestRunBasics = class(TDABAbstractTestCase)
  published
    procedure TestInitialize;
    procedure TestLaunch;
    procedure TestConsole;
    procedure TestArguments;
    procedure TestWorkingDir;
    procedure TestAlternatingArguments;
    procedure TestEnvironment;

    procedure TestBreakpoints;
    procedure TestAddBreakpoint;

    procedure TestEvaluate;
  end;

  { TDABTestEvaluateBasics }

  TDABTestEvaluateBasics = class(TDABAbstractTestCase)
  published
    procedure TestBasicStrings;
  end;

  { TDABTestExceptions }

  TDABTestExceptions = class(TDABAbstractTestCase)
  published
    procedure TestPascalException;
    procedure TestSystemException;
  end;

  { TDABTestEvaluateClassesBasics }

  TDABTestEvaluateClassesBasics = class(TDABAbstractTestCase)
  published
    procedure TestBasicClass;
    procedure TestEvaluateFields;
    procedure TestEvaluateVirtualInstance;
  end;

  { TDABTestThreads }

  TDABTestThreads = class(TDABAbstractTestCase)
  published
    procedure TestCallstack;
  end;

  { TDABTestSetVariable }

  TDABTestSetVariable = class(TDABAbstractTestCase)
  protected
    procedure CheckReceiveSetVariableResponse(AResultRegExpr: string; AType: string); overload;
  published
    procedure TestSetVariableVariousTypes;
  end;

  { TDABTestIndexedAccess }

  TDABTestIndexedAccess = class(TDABAbstractTestCase)
  published
    procedure TestArray;
  end;

  { TDABTestPropertiesDwarfExtension }

  // Tests for the Fpc-specific DWARF extension for properties.
  // Works only with special Fpc-versions and the compiler-option
  // -godwarfproperties
  TDABTestPropertiesDwarfExtension = class(TDABAbstractTestCase)
  published
    procedure TestBasicProperty;
    procedure TestPropertiesAsMembers;
  end;

  { TDABTestClassTypeCast }

  TDABTestClassTypeCast = class(TDABAbstractTestCase)
  published
    procedure TestBasicTypecast;
  end;


implementation

{ TDABTestClassTypeCast }

procedure TDABTestClassTypeCast.TestBasicTypecast;
var
  ThreadId, VariablesReference: Integer;
  JSVariables: TJSONArray;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testsetvariable-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testproperties') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testproperties.pp","path":"' + ExpandTestApplicationsPath('testproperties.pp') + '"},"breakpoints":[{"line":59}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module', 'output']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Basic typecast
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "TMyChildClass(MC)"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, 'TMyChildClass \('+GetPointerRegExpr+'\)', 'TMyChildClass', VariablesReference);
  Check(VariablesReference > 0, 'Variablesreference of MC');

  // Retrieve members
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 5, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('TMyChildClass.ParentProp', '75', 'LongInt', VariablesReference, JSVariables.Items[0]);

    VariablesReference := -1;
    CheckVariable('TMyClass.DoubleProp', 'property \(function\)', 'function', VariablesReference, JSVariables.Items[1]);
    Check(VariablesReference > 0, 'Variablesreference of MC.DoubleProp is empty');

    VariablesReference := 0;
    CheckVariable('TMyClass.SingleProp', '75', 'LongInt', VariablesReference, JSVariables.Items[2]);
    CheckEquals(0, VariablesReference, 'Variablesreference of MC.SingleProp');

    CheckVariable('TMyClass.FProp', '75', 'LongInt', VariablesReference, JSVariables.Items[3]);
    CheckEquals(0, VariablesReference, 'Variablesreference of MC.FProp');
  finally
    JSVariables.Free;
  end;

  // Typecast in expression
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "TMyChildClass(MC).ParentProp"}}');
  VariablesReference := 0;
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '75', 'LongInt', VariablesReference);

  // Test with missing typecast
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "MC.ParentProp"}}');
  CheckReceiveBasicResponse(5000, 'evaluate', GetCurrentReqSeq, False, 'No child named ParentProp');

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  IgnoreIncomingEventsOfType(5000, ['output']);

  // Check application-end
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

{ TDABTestThreads }

procedure TDABTestThreads.TestCallstack;
var
  ThreadId: Integer;
  JSResponse, JSBody: TJSONObject;
  JSArguments: TJSONArray;
  Name: TJSONStringType;
  Line: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testsetvariable-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testthread') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testthread.pp","path":"' + ExpandTestApplicationsPath('testthread.pp') + '"},"breakpoints":[{"line":16}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Get stackframes
  FDebugListener.SendCommand('{"command":"stackTrace","type":"request","arguments":{"threadId":'+IntToStr(ThreadId)+',"startFrame":0, "levels": 5},"seq":'+GetNextReqSeq+'}');
  JSResponse := ObtainResponse(5000, 'stackTrace response');
  try
    PerformBasicResponseTests(JSResponse, 'stackTrace', GetCurrentReqSeq, True);
    JSBody := JSResponse.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');
    JSArguments := JSBody.Get('stackFrames', TJSONArray(nil));
    Check(Assigned(JSArguments), 'response has no stackframes');
    Name := (JSArguments[0] as TJSONObject).Get('name', '');
    CheckEquals('InThread', Name, 'Stack-trace function name');
    Line := (JSArguments[0] as TJSONObject).Get('line', -1);
    CheckEquals(16, Line, 'Stack-trace line number');
  finally
    JSResponse.Free;
  end;

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(10000, 'continue', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Check application-end
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

{ TDABTestPropertiesDwarfExtension }

procedure TDABTestPropertiesDwarfExtension.TestBasicProperty;
var
  ThreadId, VariablesReference: Integer;
  JSVariables: TJSONArray;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testsetvariable-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testproperties') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testproperties.pp","path":"' + ExpandTestApplicationsPath('testproperties.pp') + '"},"breakpoints":[{"line":51}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Retrieve single property
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "MC.SingleProp"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '154', 'LongInt', VariablesReference);
  CheckEquals(0, VariablesReference, 'Variablesreference of MC.SingleProp');

  // Retrieve double property
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "MC.DoubleProp"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, 'property \(function\)', 'function', VariablesReference);
  Check(VariablesReference > 0, 'Variablesreference of MC.DoubleProp is empty');

  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 1, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('Result', '308', 'LongInt', VariablesReference, JSVariables.Items[0]);
  finally
    JSVariables.Free;
  end;

  // Retrieve getter itself
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "MC.GetProp"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, 'function \(this: TMyClass\)\: LongInt AT '+GetPointerRegExpr, 'function', VariablesReference);
  Check(VariablesReference > 0, 'Variablesreference of MC.GetProp is empty');

  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 1, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('Result', '308', 'LongInt', VariablesReference, JSVariables.Items[0]);
  finally
    JSVariables.Free;
  end;

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  IgnoreIncomingEventsOfType(5000, ['output']);

  // Check application-end
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestPropertiesDwarfExtension.TestPropertiesAsMembers;
var
  ThreadId, VariablesReference: Integer;
  JSVariables: TJSONArray;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testsetvariable-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testproperties') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testproperties.pp","path":"' + ExpandTestApplicationsPath('testproperties.pp') + '"},"breakpoints":[{"line":51}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Retrieve class
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "MC"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, 'TMyClass \('+GetPointerRegExpr+'\)', 'TMyClass', VariablesReference);
  Check(VariablesReference > 0, 'Variablesreference of MC');

  // Retrieve members
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 4, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('TMyClass.SingleProp', '154', 'LongInt', VariablesReference, JSVariables.Items[1]);
    CheckEquals(0, VariablesReference, 'Variablesreference of MC.SingleProp');

    CheckVariable('TMyClass.FProp', '154', 'LongInt', VariablesReference, JSVariables.Items[2]);
    CheckEquals(0, VariablesReference, 'Variablesreference of MC.FProp');

    VariablesReference := -1;
    CheckVariable('TMyClass.DoubleProp', 'property \(function\)', 'function', VariablesReference, JSVariables.Items[0]);
    Check(VariablesReference > 0, 'Variablesreference of MC.DoubleProp is empty');
  finally
    JSVariables.Free;
  end;

  // Retrieve value of DoubleProp
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 1, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('Result', '308', 'LongInt', VariablesReference, JSVariables.Items[0]);
  finally
    JSVariables.Free;
  end;

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  IgnoreIncomingEventsOfType(5000, ['output']);

  // Check application-end
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

{ TDABTestIndexedAccess }

procedure TDABTestIndexedAccess.TestArray;
var
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testsetvariable-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testarray') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testarray.pp","path":"' + ExpandTestApplicationsPath('testarray.pp') + '"},"breakpoints":[{"line":15}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint in StaticArray
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Retrieve indexed array-value
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "Arr[0]"}}');
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '10', 'Byte');

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "Arr[5]"}}');
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '5', 'Byte');

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  // Check application-end
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

{ TDABTestSetVariable }

procedure TDABTestSetVariable.TestSetVariableVariousTypes;
var
  ThreadId: Integer;
  VariablesReference, StackFrameId, LocalsVariablesReference, ArgumentsVariablesReference: Integer;
  JSVariables, JSScopes, JSArguments: TJSONArray;
  JSResponse, JSBody: TJSONObject;
  Obj2Addr: string;
  PbValue: string;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testsetvariable-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testsetvariable') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testsetvariable.pas","path":"' + ExpandTestApplicationsPath('testsetvariable.pas') + '"},"breakpoints":[{"line":21}, {"line":33}, {"line":51}, {"line":65}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // ------ LocalVariablesA ------
  // Stop at first breakpoint in LocalVariablesA
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Get the local variables
  StackFrameId := GetCurrentStackFrameId(ThreadId);
  GetLocalsReference(StackFrameId, LocalsVariablesReference, ArgumentsVariablesReference);

  // Set the variables
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 3, JSVariables);
  try
    VariablesReference := 0;
    // First check their initial values
    CheckVariable('i', '13442', 'SmallInt', VariablesReference, JSVariables.Items[0]);
    CheckVariable('b', '12', 'Byte', VariablesReference, JSVariables.Items[1]);
    CheckVariable('Enum', 'meTwo', 'TMyEnum', VariablesReference, JSVariables.Items[2]);
    // Now set them
    FDebugListener.SendCommand('{"command":"setVariable","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+', "name": "i", "value":15}}');
    CheckReceiveSetVariableResponse('15', 'SmallInt');
    FDebugListener.SendCommand('{"command":"setVariable","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+', "name": "b", "value":139}}');
    CheckReceiveSetVariableResponse('139', 'Byte');
    FDebugListener.SendCommand('{"command":"setVariable","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+', "name": "Enum", "value":"meThree"}}');
    CheckReceiveSetVariableResponse('meThree', 'TMyEnum');
  finally
    JSVariables.Free;
  end;

  // Check the new variables value (according to the debugger)
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 3, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('i', '15', 'SmallInt', VariablesReference, JSVariables.Items[0]);
    CheckVariable('b', '139', 'Byte', VariablesReference, JSVariables.Items[1]);
    CheckVariable('Enum', 'meThree', 'TMyEnum', VariablesReference, JSVariables.Items[2]);
  finally
    JSVariables.Free;
  end;

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);
  // The newly set values should be reflected in the output (WriteLn)
  CheckReceiveOutputEvent(5000, 'stdout', '15'+#13#10+'139'+#13#10+'meThree'+#13#10);


  // ------ LocalVariablesB ------
  // Stop at first breakpoint in LocalVariablesB
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Get the local variables
  StackFrameId := GetCurrentStackFrameId(ThreadId);
  GetLocalsReference(StackFrameId, LocalsVariablesReference, ArgumentsVariablesReference);

  // Set the variables
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 2, JSVariables);
  try
    VariablesReference := 0;
    // First check their initial values
    CheckVariable('c', '''S''', 'Char', VariablesReference, JSVariables.Items[0]);
    CheckVariable('Bool', 'True', 'Boolean', VariablesReference, JSVariables.Items[1]);
    // Now set them
    FDebugListener.SendCommand('{"command":"setVariable","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+', "name": "c", "value":"''G''"}}');
    CheckReceiveSetVariableResponse('G', 'Char');
    FDebugListener.SendCommand('{"command":"setVariable","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+', "name": "Bool", "value":"False"}}');
    CheckReceiveSetVariableResponse('False', 'Boolean');
  finally
    JSVariables.Free;
  end;

  // Check the new variables value (according to the debugger)
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 2, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('c', 'G', 'Char', VariablesReference, JSVariables.Items[0]);
    CheckVariable('Bool', 'False', 'Boolean', VariablesReference, JSVariables.Items[1]);
  finally
    JSVariables.Free;
  end;

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);
  // The newly set values should be reflected in the output (readline)
  CheckReceiveOutputEvent(5000, 'stdout', 'C is not S'+#13#10+'Bool is false'+#13#10);


  // ------ LocalVariablesC ------
  // Stop at first breakpoint in LocalVariablesB
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Get the local variables
  StackFrameId := GetCurrentStackFrameId(ThreadId);
  GetLocalsReference(StackFrameId, LocalsVariablesReference, ArgumentsVariablesReference);

  // Set the variables
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 2, JSVariables);
  try
    // First check their initial values
    VariablesReference := -1;
    CheckVariable('Obj1', 'class\(\$[0-9A-F]+\)', 'TComponent', VariablesReference, JSVariables.Items[0]);
    VariablesReference := -1;
    CheckVariable('Obj2', 'class\(\$[0-9A-F]+\)', 'TComponent', VariablesReference, JSVariables.Items[1]);
    // Now 'assign' obj2 to obj1
    Obj2Addr := (JSVariables.Items[1] as TJSONObject).get('value', '');
    Obj2Addr := copy(Obj2Addr,7,length(Obj2Addr)-7);
    FDebugListener.SendCommand('{"command":"setVariable","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+', "name": "Obj1", "value":"'+Obj2Addr+'"}}');
    CheckReceiveSetVariableResponse('class\(\'+Obj2Addr+'\)', 'TComponent');
  finally
    JSVariables.Free;
  end;

  // Check the new variables value (according to the debugger)
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 2, JSVariables);
  try
    VariablesReference := -1;
    CheckVariable('Obj1', 'class\(\'+Obj2Addr+'\)', 'TComponent', VariablesReference, JSVariables.Items[0]);
    VariablesReference := -1;
    CheckVariable('Obj2', 'class\(\'+Obj2Addr+'\)', 'TComponent', VariablesReference, JSVariables.Items[1]);
  finally
    JSVariables.Free;
  end;

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);
  // The newly set values should be reflected in the output (readline)
  // Both obj1 and obj2 should return the same value
  CheckReceiveOutputEvent(5000, 'stdout', '1234'+#13#10+'1234'+#13#10);


  // ------ LocalVariablesD ------
  // Stop at first breakpoint in LocalVariablesB
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Get the local variables
  StackFrameId := GetCurrentStackFrameId(ThreadId);
  GetLocalsReference(StackFrameId, LocalsVariablesReference, ArgumentsVariablesReference);

  // Set the variables
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 3, JSVariables);
  try
    // First check their initial values
    VariablesReference := 0;
    CheckVariable('p', 'Pointer\(\$[0-9A-F]+\)', 'Pointer', VariablesReference, JSVariables.Items[0]);
    CheckVariable('pb', 'PByte\(\$[0-9A-F]+\)', 'PByte', VariablesReference, JSVariables.Items[1]);
    CheckVariable('b', '13', 'Byte', VariablesReference, JSVariables.Items[2]);
    // Now 'assign' p to pb
    PbValue := (JSVariables.Items[1] as TJSONObject).get('value', '');
    PbValue := copy(PbValue,7,length(PbValue)-7);
    FDebugListener.SendCommand('{"command":"setVariable","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+', "name": "p", "value":"'+PbValue+'"}}');
    CheckReceiveSetVariableResponse('Pointer\(\'+PbValue+'\)', 'Pointer');
  finally
    JSVariables.Free;
  end;

  // Check the new variables value (according to the debugger)
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(LocalsVariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 3, JSVariables);
  try
    CheckVariable('p', 'Pointer\(\'+PbValue+'\)', 'Pointer', VariablesReference, JSVariables.Items[0]);
    CheckVariable('pb', 'PByte\(\$[0-9A-F]+\)', 'PByte', VariablesReference, JSVariables.Items[1]);
    CheckVariable('b', '13', 'Byte', VariablesReference, JSVariables.Items[2]);
  finally
    JSVariables.Free;
  end;

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);
  // The newly set values should be reflected in the output (readline)
  // Both obj1 and obj2 should return the same value
  CheckReceiveOutputEvent(5000, 'stdout', '43'+#13#10+Copy(PbValue,2)+#13#10+'43'+#13#10);


  // Check application-end
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

function TDABAbstractTestCase.GetCurrentStackFrameId(AThreadId: Integer): Integer;
var
  JSResponse, JSBody: TJSONObject;
  JSArguments: TJSONArray;
begin
  // Get stackframes
  FDebugListener.SendCommand('{"command":"stackTrace","type":"request","arguments":{"threadId":'+IntToStr(AThreadId)+',"startFrame":0, "levels": 5},"seq":'+GetNextReqSeq+'}');
  JSResponse := ObtainResponse(5000, 'stackTrace response');
  try
    PerformBasicResponseTests(JSResponse, 'stackTrace', GetCurrentReqSeq, True);
    JSBody := JSResponse.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');
    JSArguments := JSBody.Get('stackFrames', TJSONArray(nil));
    Check(Assigned(JSArguments), 'response has no stackframes');
    Result := (JSArguments[0] as TJSONObject).Get('id', -1);
    CheckTrue(ThreadId>0, 'Invalid stackframe-id');
  finally
    JSResponse.Free;
  end;
end;

procedure TDABAbstractTestCase.GetLocalsReference(AStackFrameId: Integer; out ALocalsReference, AnArgmentsReference: Integer);
var
  JSResponse, JSBody, JSVariablesScope: TJSONObject;
  JSScopes: TJSONArray;
begin
  FDebugListener.SendCommand('{"command":"scopes","type":"request","arguments":{"frameId":'+IntToStr(AStackFrameId)+'},"seq":'+GetNextReqSeq+'}');
  JSResponse := ObtainResponse(5000, 'scopes response');
  try
    PerformBasicResponseTests(JSResponse, 'scopes', GetCurrentReqSeq, True);
    JSBody := JSResponse.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');
    JSScopes := JSBody.Get('scopes', TJSONArray(nil));
    Check(Assigned(JSScopes), 'response has no scopes');
    CheckEquals(2, JSScopes.Count, 'invalid amount of scopes returned');
    JSVariablesScope := JSScopes[1] as TJSONObject;
    CheckEquals('Locals', JSVariablesScope.Get('name', ''));
    ALocalsReference := JSVariablesScope.Get('variablesReference', -1);
    Check(AnArgmentsReference > -1, 'Invalid arguments variables-reference');
    JSVariablesScope := JSScopes[0] as TJSONObject;
    CheckEquals('Arguments', JSVariablesScope.Get('name', ''));
    AnArgmentsReference := JSVariablesScope.Get('variablesReference', -1);
    Check(ALocalsReference > -1, 'Invalid locals variables-reference')
  finally
    JSResponse.Free;
  end;
end;

procedure TDABTestSetVariable.CheckReceiveSetVariableResponse(AResultRegExpr: string; AType: string);
var
  JSObject,
  JSBody: TJSONObject;
  RegExpr: TRegExpr;
begin
  JSObject := ObtainResponse(FDefaultRequestTimeout, 'set variable');
  try
    PerformBasicResponseTests(JSObject, 'setVariable', GetCurrentReqSeq, True);
    JSBody := JSObject.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');

    RegExpr := TRegExpr.Create(AResultRegExpr);
    try
      Check(RegExpr.Exec(JSBody.Get('value', '')), 'The evaluation-result [' + JSBody.Get('value', '') + '] does not match the regular expression [' + AResultRegExpr + ']');
    finally
      RegExpr.Free;
    end;

    if AType<>'' then
      CheckEquals(AType, JSBody.Get('type', 'The evaluation-type is invalid'));
  finally
    JSObject.Free;
  end;
end;

{ TDABTestEvaluateClassesBasics }

procedure TDABTestEvaluateClassesBasics.TestBasicClass;
var
  ThreadId: Integer;
  VariablesReference: Integer;
  JSVariables: TJSONArray;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testclass') + GetExeExt + '", "orderVariablesBy" : [{"strategy" : "Defined"}]},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testclass.pp","path":"' + ExpandTestApplicationsPath('testclass.pp') + '"},"breakpoints":[{"line":40}, {"line":47}]},"type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', 3, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":4}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 4, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // At this moment, ChildInst should be nil
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":5, "arguments": {"expression": "ChildInst"}}');
  CheckReceiveEvaluateResponse(5000, 5, 'nil', 'TMyChildClass');

  // Step one line
  FDebugListener.SendCommand('{"command":"next","type":"request","seq":6, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'next', 6, True);
  CheckReceiveStoppedEvent(5000, ThreadId, 'step', 'Single stepping');

  // At this moment, ChildInst should be assigned
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":7, "arguments": {"expression": "ChildInst"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, 7, 'TMyChildClass \('+GetPointerRegExpr+'\)', 'TMyChildClass', VariablesReference);
  Check(VariablesReference > 0, 'Variablesreference may not be empty');

  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":8, "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, 8, 3, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('TObject._vptr$TOBJECT', 'Pointer\('+GetPointerRegExpr+'\)', 'Pointer', VariablesReference, JSVariables.Items[0]);
    // AnsiStrings have additional-information so they return a VariableReference > 0.
    VariablesReference := -1;
    CheckVariable('TMyChildClass.FField1', '''''', 'AnsiString', VariablesReference, JSVariables.Items[1]);
    VariablesReference := -1;
    CheckVariable('TMyChildClass.Field1', '''''', 'AnsiString', VariablesReference, JSVariables.Items[2]);
  finally
    JSVariables.Free;
  end;

  // Step one line
  FDebugListener.SendCommand('{"command":"next","type":"request","seq":9, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'next', 9, True);
  CheckReceiveStoppedEvent(5000, ThreadId, 'step', 'Single stepping');

  // Now FField1 should be set
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":10, "arguments": {"expression": "ChildInst"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, 10, 'TMyChildClass \('+GetPointerRegExpr+'\)', 'TMyChildClass', VariablesReference);
  Check(VariablesReference > 0, 'Variablesreference may not be empty');

  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":11, "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, 11, 3, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('TObject._vptr$TOBJECT', 'Pointer\('+GetPointerRegExpr+'\)', 'Pointer', VariablesReference, JSVariables.Items[0]);
    // AnsiStrings have additional-information so they return a VariableReference > 0.
    VariablesReference := -1;
    CheckVariable('TMyChildClass.FField1', 'Field1', 'AnsiString', VariablesReference, JSVariables.Items[1]);
    VariablesReference := -1;
    CheckVariable('TMyChildClass.Field1', 'Field1', 'AnsiString', VariablesReference, JSVariables.Items[2]);
  finally
    JSVariables.Free;
  end;

  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":12, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 12, True);
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Now Inst should be assigned.
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":13, "arguments": {"expression": "Inst"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, 13, 'TMyClass \('+GetPointerRegExpr+'\)', 'TMyClass', VariablesReference);
  Check(VariablesReference > 0, 'Variablesreference may not be empty');

  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":14, "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, 14, 5, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('TObject._vptr$TOBJECT', 'Pointer\('+GetPointerRegExpr+'\)', 'Pointer', VariablesReference, JSVariables.Items[0]);
    CheckVariable('TMyClass.FIntField', '123', 'LongInt', VariablesReference, JSVariables.Items[1]);
    CheckVariable('TMyClass.FChild', 'TMyChildClass \('+GetPointerRegExpr+'\)', 'TMyChildClass', JSVariables.Items[2]);
    CheckVariable('TMyClass.IntField', '123', 'LongInt', VariablesReference, JSVariables.Items[4]);
    VariablesReference := -1;
    CheckVariable('TMyClass.Child', 'TMyChildClass \('+GetPointerRegExpr+'\)', 'TMyChildClass', VariablesReference, JSVariables.Items[3]);
  finally
    JSVariables.Free;
  end;

  // Finally, check the members of the child-class
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":15, "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, 15, 3, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('TObject._vptr$TOBJECT', 'Pointer\('+GetPointerRegExpr+'\)', 'Pointer', VariablesReference, JSVariables.Items[0]);
    // AnsiStrings have additional-information so they return a VariableReference > 0.
    VariablesReference := -1;
    CheckVariable('TMyChildClass.FField1', 'Field1', 'AnsiString', VariablesReference, JSVariables.Items[1]);
    VariablesReference := -1;
    CheckVariable('TMyChildClass.Field1', 'Field1', 'AnsiString', VariablesReference, JSVariables.Items[2]);
  finally
    JSVariables.Free;
  end;

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":16, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 16, True);
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestEvaluateClassesBasics.TestEvaluateFields;
var
  ThreadId: Integer;
  VariablesReference: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testclass') + GetExeExt + '"},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testclass.pp","path":"' + ExpandTestApplicationsPath('testclass.pp') + '"},"breakpoints":[{"line":47}]},"type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', 3, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":4}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 4, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "Inst.FIntField"}}');
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '123', 'LongInt');

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "Inst.FChild.FField1"}}');
  // AnsiStrings have additional-information so they return a VariableReference > 0.
  VariablesReference:=-1;
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '''ChildField1''', 'AnsiString', VariablesReference);

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":16, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 16, True);
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestEvaluateClassesBasics.TestEvaluateVirtualInstance;
var
  ThreadId, VariablesReference: Integer;
  JSVariables: TJSONArray;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the testsetvariable-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('testproperties') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"testproperties.pp","path":"' + ExpandTestApplicationsPath('testproperties.pp') + '"},"breakpoints":[{"line":59}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module', 'output']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Basic typecast
  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "MC"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, 'TMyChildClass \('+GetPointerRegExpr+'\)', 'TMyClass', VariablesReference);
  Check(VariablesReference > 0, 'Variablesreference of MC');

  // Retrieve members
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 5, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('TMyChildClass.ParentProp', '75', 'LongInt', VariablesReference, JSVariables.Items[0]);

    VariablesReference := -1;
    CheckVariable('TMyClass.DoubleProp', 'property \(function\)', 'function', VariablesReference, JSVariables.Items[1]);
    Check(VariablesReference > 0, 'Variablesreference of MC.DoubleProp is empty');

    VariablesReference := 0;
    CheckVariable('TMyClass.SingleProp', '75', 'LongInt', VariablesReference, JSVariables.Items[2]);
    CheckEquals(0, VariablesReference, 'Variablesreference of MC.SingleProp');

    CheckVariable('TMyClass.FProp', '75', 'LongInt', VariablesReference, JSVariables.Items[3]);
    CheckEquals(0, VariablesReference, 'Variablesreference of MC.FProp');
  finally
    JSVariables.Free;
  end;

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":'+GetNextReqSeq+', "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', GetCurrentReqSeq, True);

  IgnoreIncomingEventsOfType(5000, ['output']);

  // Check application-end
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

{ TDABTestExceptions }

procedure TDABTestExceptions.TestSystemException;
var
  ThreadId: Integer;
  ExpectedMessage: String;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the systemexception-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' + ExpandTestApplicationsPath('systemexception') + GetExeExt + '"},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 3, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at exception
  // Note that the exception-message should be improved... a lot.
  {$ifdef windows}
  ExpectedMessage := 'External: FLT DIVIDE BY ZERO:';
  {$else}
  ExpectedMessage := 'External: Unknown exception code 8:';
  {$endif}
  CheckReceiveStoppedEvent(5000, ThreadId, 'exception', 'Paused on exception', ExpectedMessage);

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":7, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 7, True);

  // Check that the exception is shown (thus processed)
  CheckReceiveOutputEvent(5000, 'stdout', 'Runtime error 208 at ', True);

  CheckReceiveBasicEventIgnoreOthers(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestExceptions.TestPascalException;
var
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the systemexception-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('pascalexception') + GetExeExt + '"},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 3, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at exception
  // Note that the exception-message should be improved... a lot.
  CheckReceiveStoppedEvent(5000, ThreadId, 'exception', 'Paused on exception', 'Exception: This is a Pascal exception');

  // Continue
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":7, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 7, True);

  // Check that the exception is shown (thus processed)
  CheckReceiveOutputEvent(5000, 'stdout', 'An unhandled exception occurred at ', True);

  CheckReceiveBasicEventIgnoreOthers(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

{ TDABTestEvaluateBasics }

procedure TDABTestEvaluateBasics.TestBasicStrings;
var
  ThreadId, VariablesReference: Integer;
  JSVariables: TJSONArray;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(10, 'initialize', GetCurrentReqSeq, True);

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('teststrings') + GetExeExt + '"},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'launch', GetCurrentReqSeq, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set a breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"simpleproc.pp","path":"' + ExpandTestApplicationsPath('teststrings.pp') + '"},"breakpoints":[{"line":21}]},"type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', GetCurrentReqSeq, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":'+GetNextReqSeq+'}');
  CheckReceiveBasicResponse(5000, 'configurationDone', GetCurrentReqSeq, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "SimpleString"}}');
  // AnsiStrings have additional-information so they return a VariableReference > 0.
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '''Hello World''', 'AnsiString', VariablesReference);
  Check(VariablesReference > 0, 'Variablesreference may not be empty');
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 2, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('Dynamic codepage', 'UTF-8', '', VariablesReference, JSVariables.Items[0]);
    CheckVariable('Rawdata', '''Hello World''', '', VariablesReference, JSVariables.Items[1]);
  finally
    JSVariables.Free;
  end;

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":'+GetNextReqSeq+', "arguments": {"expression": "String1252"}}');
  VariablesReference := -1;
  CheckReceiveEvaluateResponse(5000, GetCurrentReqSeq, '''Héllo World''', 'TString1252', VariablesReference);
  Check(VariablesReference > 0, 'Variablesreference may not be empty');
  FDebugListener.SendCommand('{"command":"variables","type":"request","seq":'+GetNextReqSeq+', "arguments": {"variablesReference": '+IntToStr(VariablesReference)+'}}');
  CheckReceiveVariablesResponse(5000, GetCurrentReqSeq, 2, JSVariables);
  try
    VariablesReference := 0;
    CheckVariable('Dynamic codepage', '1252', '', VariablesReference, JSVariables.Items[0]);
    CheckVariable('Rawdata', '''H''#233''llo World''', '', VariablesReference, JSVariables.Items[1]);
  finally
    JSVariables.Free;
  end;

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":7, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 7, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

{ TDABTestRunBasics }

procedure TDABTestRunBasics.TestInitialize;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);
end;

procedure TDABTestRunBasics.TestLaunch;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Test for a non-existing file
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"doesnotexist"},"type":"request","seq":2}');
  CheckReceiveBasicResponse(10, 'launch', 2, False, 'exe_not_found');

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('helloworld') + GetExeExt + '"},"type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'launch', 3, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Nothing to configure
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":4}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 4, True);

  CheckReceiveBasicEventIgnoreOthers(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestRunBasics.TestConsole;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' + ExpandTestApplicationsPath('helloworld') + GetExeExt + '"},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Nothing to configure
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 3, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Check the console-output
  CheckReceiveOutputEvent(5000, 'stdout', 'Hello World' + #13#10);

  // Clean-up
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestRunBasics.TestBreakpoints;
var
  JSResponse,
  JSBody: TJSONObject;
  JSBreakpoints: TJSONArray;
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('simpleproc') + GetExeExt + '"},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set the initial breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"simpleproc.pp","path":"' + ExpandTestApplicationsPath('simpleproc.pp') + '"},"breakpoints":[{"line":9},{"line":14},{"line":30}]},"type":"request","seq":3}');

  JSResponse := ObtainResponse(5000, 'setbreakpoint response');
  try
    PerformBasicResponseTests(JSResponse, 'setBreakpoints', 3, True);
    JSBody := JSResponse.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');
    JSBreakpoints := JSBody.Get('breakpoints', TJSONArray(nil));
    Check(Assigned(JSBreakpoints), 'response has no breakpoints');
    CheckEquals(3, JSBreakpoints.Count, 'invalid amount of breakpoints returned');
    PerformBreakpointTests(JSBreakpoints[0], 9, True);
    PerformBreakpointTests(JSBreakpoints[1], 14, True);
    PerformBreakpointTests(JSBreakpoints[2], 30, False);
  finally
    JSResponse.Free;
  end;

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":4}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 4, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Remove the second breakpoint
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"simpleproc.pp","path":"' + ExpandTestApplicationsPath('simpleproc.pp') + '"},"breakpoints":[{"line":14},{"line":30}]},"type":"request","seq":5}');

  JSResponse := ObtainResponse(5000, 'setbreakpoint response');
  try
    PerformBasicResponseTests(JSResponse, 'setBreakpoints', 5, True);
    JSBody := JSResponse.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');
    JSBreakpoints := JSBody.Get('breakpoints', TJSONArray(nil));
    Check(Assigned(JSBreakpoints), 'response has no breakpoints');
    CheckEquals(2, JSBreakpoints.Count, 'invalid amount of breakpoints returned');
    PerformBreakpointTests(JSBreakpoints[0], 14, True);
    PerformBreakpointTests(JSBreakpoints[1], 30, False);
  finally
    JSResponse.Free;
  end;

  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":6, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 6, True);

  // The application should exit, breakpoint has been removed
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestRunBasics.TestAddBreakpoint;
var
  JSResponse,
  JSBody: TJSONObject;
  JSBreakpoints: TJSONArray;
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('simpleproc') + GetExeExt + '"},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set the initial breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"simpleproc.pp","path":"' + ExpandTestApplicationsPath('simpleproc.pp') + '"},"breakpoints":[{"line":14},{"line":30}]},"type":"request","seq":3}');

  JSResponse := ObtainResponse(5000, 'setbreakpoint response');
  try
    PerformBasicResponseTests(JSResponse, 'setBreakpoints', 3, True);
    JSBody := JSResponse.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');
    JSBreakpoints := JSBody.Get('breakpoints', TJSONArray(nil));
    Check(Assigned(JSBreakpoints), 'response has no breakpoints');
    CheckEquals(2, JSBreakpoints.Count, 'invalid amount of breakpoints returned');
    PerformBreakpointTests(JSBreakpoints[0], 14, True);
    PerformBreakpointTests(JSBreakpoints[1], 30, False);
  finally
    JSResponse.Free;
  end;

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":4}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 4, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  // Add a second breakpoint
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"simpleproc.pp","path":"' + ExpandTestApplicationsPath('simpleproc.pp') + '"},"breakpoints":[{"line":9},{"line":14},{"line":30}]},"type":"request","seq":5}');

  JSResponse := ObtainResponse(5000, 'setbreakpoint response');
  try
    PerformBasicResponseTests(JSResponse, 'setBreakpoints', 5, True);
    JSBody := JSResponse.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');
    JSBreakpoints := JSBody.Get('breakpoints', TJSONArray(nil));
    Check(Assigned(JSBreakpoints), 'response has no breakpoints');
    CheckEquals(3, JSBreakpoints.Count, 'invalid amount of breakpoints returned');
    PerformBreakpointTests(JSBreakpoints[0], 9, True);
    PerformBreakpointTests(JSBreakpoints[1], 14, True);
    PerformBreakpointTests(JSBreakpoints[2], 30, False);
  finally
    JSResponse.Free;
  end;

  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":6, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 6, True);

  // Stop at the second breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":7, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 7, True);

  // Clean-up
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestRunBasics.TestEvaluate;
var
  ThreadId: Integer;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' +ExpandTestApplicationsPath('simpleproc') + GetExeExt + '"},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Set a breakpoints
  FDebugListener.SendCommand('{"command":"setBreakpoints","arguments":{"source":{"name":"simpleproc.pp","path":"' + ExpandTestApplicationsPath('simpleproc.pp') + '"},"breakpoints":[{"line":9}]},"type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'setBreakpoints', 3, True);

  // Configuration done, start debugging...
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":4}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 4, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Stop at first breakpoint
  CheckReceiveStoppedEvent(5000, ThreadId, 'breakpoint', 'Paused on breakpoint');

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":5, "arguments": {"expression": "b"}}');
  CheckReceiveEvaluateResponse(5000, 5, '40', 'SmallInt');

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":6, "arguments": {"expression": "DoesNotExist"}}');
  {$ifdef UsePasSrc}
  CheckReceiveBasicResponse(5000, 'evaluate', 6, False, 'Identifier not found: ''DoesNotExist''');
  {$else}
  CheckReceiveBasicResponse(5000, 'evaluate', 6, False, 'Identifier not found: "DoesNotExist"');
  {$endif}

  FDebugListener.SendCommand('{"command":"evaluate","type":"request","seq":7, "arguments": {"expression": "Arr[3"}}');
  CheckReceiveBasicResponse(5000, 'evaluate', 7, False, 'Expected "," at token "EOF" in file Expression at line 1 column 6');

  // Finish application
  FDebugListener.SendCommand('{"command":"continue","type":"request","seq":8, "arguments": { "threadId": ' + IntToStr(ThreadID) + '} }');
  CheckReceiveBasicResponse(5000, 'continue', 8, True);

  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestRunBasics.TestArguments;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' + ExpandTestApplicationsPath('testparams') + GetExeExt + '","parameters":["-p", "Hello World"]},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Nothing to configure
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 3, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Check the console-output
  CheckReceiveOutputEvent(5000, 'stdout', '-p' + #13#10 + 'Hello World' + #13#10);

  // Clean-up
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestRunBasics.TestWorkingDir;
var
  TmpDir: string;
  TmpDirEscaped: string;
begin
  TmpDir := ExtractFileDir(GetTempDir);
  TmpDirEscaped := StringReplace(TmpDir, '\', '\\', [rfReplaceAll]);

  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' + ExpandTestApplicationsPath('testparams') + GetExeExt + '","workingdirectory":"'+TmpDirEscaped+'","parameters":["-w"]},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Nothing to configure
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 3, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Check the console-output
  CheckReceiveOutputEvent(5000, 'stdout', TmpDir + #13#10);

  // Clean-up
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

procedure TDABTestRunBasics.TestAlternatingArguments;
begin
  // There was a problem with parameters of two different runs
  // that got mixed up.
  TestWorkingDir;
  TestArguments;

end;

procedure TDABTestRunBasics.TestEnvironment;
begin
  FDebugListener.SendCommand('{"command":"initialize","arguments":{"adapterID":"fpDebug"},"type":"request","seq":1}');
  CheckReceiveBasicResponse(10, 'initialize', 1, True);

  // Start debugging the helloworld-application
  FDebugListener.SendCommand('{"command":"launch","arguments":{"program":"' + ExpandTestApplicationsPath('testparams') + GetExeExt + '","parameters":["-e"],"environment":[{"name":"env1","value":"val1"},{"name":"env2","value":"val2"}]},"type":"request","seq":2}');
  CheckReceiveBasicResponse(5000, 'launch', 2, True);
  CheckReceiveBasicEvent(5000, 'initialized');

  // Nothing to configure
  FDebugListener.SendCommand('{"command":"configurationDone","type":"request","seq":3}');
  CheckReceiveBasicResponse(5000, 'configurationDone', 3, True);

  // Ignore startup-related events
  IgnoreIncomingEventsOfType(5000, ['module']);

  // Check the console-output
  CheckReceiveOutputEvent(5000, 'stdout', 'env1=val1' + #13#10 + 'env2=val2' + #13#10);

  // Clean-up
  CheckReceiveBasicEvent(5000, 'terminated');
  CheckReceiveBasicEvent(5000, 'exited');
end;

{ TDABAbstractTestCase }

procedure TDABAbstractTestCase.SetUp;
begin
  FResponseCache.Free;
  FResponseCache := TResponseStack.Create();

  FTestDebugger := TTestDebugger.Create;
  FTestDebugger.StartDebugger;

  FDebugListener := TTestDebugListener.Create(FTestDebugger.Distributor, TDABInOutputProcessor, ClassName);

  // Give the debugger-thread some time to start.
  // It would be better to have some mechanism to detect that the thread is ready, though.
  Sleep(100);
end;

procedure TDABAbstractTestCase.TearDown;
begin
  FResponseCache.Free;
  FDebugListener.Free;

  FTestDebugger.StopDebugger;
  FTestDebugger.Free;
end;

procedure TDABAbstractTestCase.CheckReceiveBasicResponse(
  ATimeout: Cardinal;
  ACommand: string;
  AReqSeq: Integer;
  ASuccess: Boolean;
  AMessage: string = '');
var
  JSObject: TJSONObject;
begin
  JSObject := ObtainResponse(ATimeout, ACommand);
  try
    PerformBasicResponseTests(JSObject, ACommand, AReqSeq, ASuccess, AMessage);
  finally
    JSObject.Free;
  end;
end;

function TDABAbstractTestCase.ObtainResponse(ATimeout: Cardinal; Description: string): TJSONObject;
begin
  Result := RetrieveResponse(ATimeout, Description, False);
end;

function TDABAbstractTestCase.RetrieveResponse(ATimeout: Cardinal; ADescription: string; APeek: Boolean): TJSONObject;

var
  EventStr: string;
  JSData: TJSONData;
  JSResult: TJSONData;
  JSArray: TJSONArray;
  i: Integer;
begin
  JSResult:= nil;

  if FResponseCache.Count >0 then
    begin
    if APeek then
      JSResult := FResponseCache.Peek
    else
      JSResult := FResponseCache.Pop;
    end
  else
    begin
    Check(FDebugListener.WaitForEvent(EventStr, ATimeout) = wrSignaled, 'Timeout while waiting for [' + ADescription + '] event');
    JSData := GetJSON(EventStr);
    try
      Check(Assigned(JSData), 'Did not receive valid JSON-data in ' + ADescription + ' event: [' + EventStr + ']');
      if JSData.JSONType=jtArray then
        begin
        JSArray := JSData as TJSONArray;
        for i := JSArray.Count -1 downto 0 do
          FResponseCache.Push(JSArray.Extract(i));

        if APeek then
          JSResult := FResponseCache.Peek
        else
          JSResult := FResponseCache.Pop;
        end
      else
        begin
        JSResult := JSData as TJSONObject;
        if APeek then
          FResponseCache.Push(JSResult);
        JSData := nil;
        end;
    finally
      JSData.Free;
    end;
    end;

  try
    Check(JSResult.JSONType=jtObject, 'Received JSON in ' + ADescription + ' is not an object');
    Result := JSResult as TJSONObject;
    JSResult := nil;
  finally
    JSResult.Free;
  end;
end;

function TDABAbstractTestCase.PeekResponse(ATimeout: Cardinal; Description: string): TJSONObject;
begin
  Result := RetrieveResponse(ATimeout, Description, True);
end;


procedure TDABAbstractTestCase.PerformBasicResponseTests(
  AJSONResponse: TJSONObject;
  ACommand: string;
  AReqSeq: Integer;
  ASuccess: Boolean;
  AMessage: string);
begin
  CheckEquals('response', AJSONResponse.Get('type'), 'The [' + ACommand + '] response-type is invalid');
  CheckEquals(AReqSeq, AJSONResponse.Get('request_seq'), 'The [' + ACommand + '] command response has an unexpected request-sequence number');
  CheckEquals(ACommand, AJSONResponse.Get('command'), 'The [' + ACommand + '] command response is invalid');
  CheckEquals(ASuccess, AJSONResponse.Get('success'), 'The [' + ACommand + '] command success-flag has an invalid value. Message: [' +AJSONResponse.Get('message') + ']');
  if AMessage<>'' then
    CheckEquals(AMessage, AJSONResponse.Get('message'), 'The [' + ACommand + '] command has an invalid message');
end;

function TDABAbstractTestCase.ExpandTestApplicationsPath(AName: string): string;
begin
  Result := ConcatPaths([ExtractFileDir(ParamStr(0)), 'testapplications', AName]);
  // Escape backslashes
  Result := StringReplace(Result, '\', '\\', [rfReplaceAll]);
end;

procedure TDABAbstractTestCase.CheckReceiveBasicEvent(ATimeout: Cardinal; AnEvent: string);
var
  JSObject: TJSONObject;
begin
  JSObject := ObtainResponse(ATimeout, AnEvent);
  try
    PerformBasicEventTests(JSObject, AnEvent);
  finally
    JSObject.Free;
  end;
end;

procedure TDABAbstractTestCase.PerformBasicEventTests(AJSONEvent: TJSONObject; AnEvent: string);
begin
  CheckEquals('event', AJSONEvent.Get('type'), 'The [' + AnEvent + '] DAB-type is invalid');
  CheckEquals(AnEvent, AJSONEvent.Get('event'), 'The [' + AnEvent + '] event-type is invalid');
end;

procedure TDABAbstractTestCase.CheckReceiveBasicEventIgnoreOthers(ATimeout: Cardinal; AnEvent: string);
var
  JSObject: TJSONObject;
  Found: Boolean;
begin
  Found := False;

  repeat
  // The timeout in in fact incorrect (should be calculated), but that's not
  // really important for a test.
  JSObject := ObtainResponse(ATimeout, AnEvent);
  try
    if (JSObject.Get('type', '') = 'event') and
       (JSObject.Get('event', '') = AnEvent) then
      begin
      PerformBasicEventTests(JSObject, AnEvent);
      Found := True;
      end;
  finally
    JSObject.Free;
  end;

  until Found;
end;

procedure TDABAbstractTestCase.IgnoreIncomingEventsOfType(ATimeout: Cardinal; AnEventArr: array of string);

  function InArr(Value: string; Arr: array of string): Boolean;
  var
    i: Integer;
  begin
    for i := 0 to High(Arr) do
      if Arr[i]=Value then
        Exit(True);
    Exit(False);
  end;

var
  JSObject: TJSONObject;
  Stop: Boolean;
begin
  Stop := False;

  repeat
  // The timeout in in fact incorrect (should be calculated), but that's not
  // really important for a test.
  JSObject := PeekResponse(ATimeout, 'Awaiting events');
  if Assigned(JSObject) and
     (JSObject.Get('type', '') = 'event') and
     (InArr(JSObject.Get('event', ''), AnEventArr)) then
    begin
    PerformBasicEventTests(JSObject, JSObject.Get('event', ''));
    JSObject := ObtainResponse(ATimeout, 'Discarding events');
    JSObject.Free;
    end
  else
    Stop := True;
  until Stop;
end;

procedure TDABAbstractTestCase.CheckReceiveOutputEvent(ATimeout: Cardinal; ACategory: string; AnOutput: string; APartialMatch: Boolean = False);
var
  JSObject,
  JSBody: TJSONObject;
  Output: string;
begin
  JSObject := ObtainResponse(ATimeout, 'output');
  try
    PerformBasicEventTests(JSObject, 'output');

    JSBody := JSObject.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');
    CheckEquals(ACategory, JSBody.Get('category'), 'The output category is invalid');
    Output := JSBody.Get('output');
    if APartialMatch then
      Output := Copy(Output, 1, Length(AnOutput));
    CheckEquals(AnOutput, Output, 'The output is invalid');
  finally
    JSObject.Free;
  end;
end;

procedure TDABAbstractTestCase.PerformBreakpointTests(
  AJSONBreakpoint: TJSONData;
  ALine: Integer;
  AVerified: Boolean);
var
  JSObject: TJSONObject;
begin
  Check(AJSONBreakpoint.JSONType=jtObject, 'the breakpoint should be a json-object');
  JSObject := AJSONBreakpoint as TJSONObject;
  CheckEquals(ALine, JSObject.Get('line'), 'The breakpoint-line is invalid');
  CheckEquals(AVerified, JSObject.Get('verified'), 'The breakpoint-verified flag is invalid');
end;

procedure TDABAbstractTestCase.CheckReceiveStoppedEvent(
  ATimeout: Cardinal;
  out AThreadId: Integer;
  AReason: string;
  ADescription: string;
  AText: string);
var
  JSObject,
  JSBody: TJSONObject;
begin
  JSObject := ObtainResponse(ATimeout, 'stopped');
  try
    PerformBasicEventTests(JSObject, 'stopped');
    JSBody := JSObject.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');
    CheckEquals(AReason, JSBody.Get('reason'), 'The stopped reason invalid');
    CheckEquals(ADescription, JSBody.Get('description'), 'The stop-description is invalid');
    if AText <> '' then
      CheckEquals(AText, JSBody.Get('text'), 'The stop-text is invalid');
    AThreadId := JSBody.Get('threadId');
    CheckNotEquals(0, AThreadId, 'The ThreadId is invalid');
  finally
    JSObject.Free;
  end;
end;

procedure TDABAbstractTestCase.CheckReceiveEvaluateResponse(
  ATimeout: Cardinal;
  AReqSeq: Integer;
  AResultRegExpr: string;
  AType: string;
  var AVariablesReference: Integer);
var
  JSObject,
  JSBody: TJSONObject;
  RegExpr: TRegExpr;
begin
  JSObject := ObtainResponse(ATimeout, 'evaluate');
  try
    PerformBasicResponseTests(JSObject, 'evaluate', AReqSeq, True);
    JSBody := JSObject.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');

    RegExpr := TRegExpr.Create(AResultRegExpr);
    try
      Check(RegExpr.Exec(JSBody.Get('result', '')), 'The evaluation-result [' + JSBody.Get('result', '') + '] does not match the regular expression [' + AResultRegExpr + ']');
    finally
      RegExpr.Free;
    end;

    if AVariablesReference > -1 then
      CheckEquals(AVariablesReference, JSBody.Get('variablesReference'), 'The evaluation-VariablesReference is invalid');
    AVariablesReference := JSBody.Get('variablesReference');

    if AType<>'' then
      CheckEquals(AType, JSBody.Get('type', 'The evaluation-type is invalid'));
  finally
    JSObject.Free;
  end;
end;

procedure TDABAbstractTestCase.CheckReceiveEvaluateResponse(ATimeout: Cardinal; AReqSeq: Integer; AResultRegExpr: string; AType: string);
var
  VariablesReference: Integer;
begin
  VariablesReference := 0;
  CheckReceiveEvaluateResponse(ATimeout, AReqSeq, AResultRegExpr, AType, VariablesReference);
end;

procedure TDABAbstractTestCase.CheckVariable(
  AName: string;
  AValueRegExpr: string;
  AType: string;
  var AVariablesReference: Integer;
  AJSONData: TJSONData);
var
  JSObject: TJSONObject;
  RegExpr: TRegExpr;
begin
  Check(jtObject=AJSONData.JSONType, 'The variable should be an object');
  JSObject := AJSONData as TJSONObject;
  CheckEquals(AName, JSObject.Get('name', ''), 'Variable name');

  if (AValueRegExpr='') then
    CheckEquals(AValueRegExpr, JSObject.Get('value', ''), 'The variable-value')
  else
    begin
    RegExpr := TRegExpr.Create(AValueRegExpr);
    try
      Check(RegExpr.Exec(JSObject.Get('value', '')), 'The variable-value [' + JSObject.Get('value', '') + '] compared to the regular expression [' + AValueRegExpr + ']');
    finally
      RegExpr.Free;
    end;
    CheckEquals(AType, JSObject.Get('type', ''), 'Variable type');
    end;

  if AVariablesReference > -1 then
    CheckEquals(AVariablesReference, JSObject.Get('variablesReference'), 'The evaluation-VariablesReference is invalid');
  AVariablesReference := JSObject.Get('variablesReference');
end;

procedure TDABAbstractTestCase.CheckVariable(
  AName: string;
  AValueRegExpr: string;
  AType: string;
  AJSONData: TJSONData);
var
  VariablesReference: Integer;
begin
  VariablesReference := -1;
  CheckVariable(AName, AValueRegExpr, AType, VariablesReference, AJSONData);
end;

procedure TDABAbstractTestCase.CheckReceiveVariablesResponse(ATimeout: Cardinal; AReqSeq: Integer; AVariableCount: Integer; out JSVariables: TJSONArray);
var
  JSObject, JSBody: TJSONObject;
  JSV: TJSONArray;
begin
  JSObject := ObtainResponse(5000, 'variables');
  try
    PerformBasicResponseTests(JSObject, 'variables', AReqSeq, True);
    JSBody := JSObject.Get('body', TJSONObject(nil));
    Check(Assigned(JSBody), 'response has no body');
    JSV := JSBody.Get('variables', TJSONarray(nil));
    Check(Assigned(JSV), 'response has no variables');
    CheckEquals(AVariableCount, JSV.Count, 'variable-count does not match');
    JSVariables := JSV.Clone as TJSONArray;
  finally
    JSObject.Free;
  end;
end;

function TDABAbstractTestCase.GetCurrentReqSeqStr: string;
begin
  Result := IntToStr(FCurrentReqSeq);
end;

function TDABAbstractTestCase.GetNextReqSeq: string;
begin
  Inc(FCurrentReqSeq);
  Result := GetCurrentReqSeqStr;
end;

function TDABAbstractTestCase.GetCurrentReqSeq: Integer;
begin
  Result := FCurrentReqSeq;
end;

constructor TDABAbstractTestCase.Create;
begin
  inherited Create;
  FDefaultRequestTimeout := 5000;
end;

function TDABAbstractTestCase.GetPointerRegExpr: string;
begin
  {$ifdef cpu64}
  Result := '\$[0-9A-F]{16}'
  {$else}
  Result := '\$[0-9A-F]{8}'
  {$endif}
end;

initialization
  RegisterTest(TDABTestRunBasics);
  RegisterTest(TDABTestEvaluateBasics);
  RegisterTest(TDABTestExceptions);
  RegisterTest(TDABTestIndexedAccess);
  RegisterTest(TDABTestEvaluateClassesBasics);
  RegisterTest(TDABTestSetVariable);
  RegisterTest(TDABTestPropertiesDwarfExtension);
  RegisterTest(TDABTestThreads);
  RegisterTest(TDABTestClassTypeCast);
end.

