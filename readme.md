# FPDebug Server

A stand-alone debug server based on fpDebug. It can be used as a stand-alone console-based debugger, but also supports Microsoft's Debug Adapter Protocol ([DAB](https://microsoft.github.io/debug-adapter-protocol)). With DAB it is possible to provide debug-functionality to third party tools.

FPDebug only suports x86 and x64 platfoms, running Windows, Linux or macOS.

## Getting started

For usage with Visual Studio Code and Lazarus there are plugins and pre-compiled binaries to get you started. For usage with other DAB-clients you have to specify some FPDebug-specific parameters.

### Pre-compiled binaries

Pre-compiled binaries for Windows and Linux are available on [Gitlab](https://gitlab.com/fpcprojects/fpdserver/-/releases)

### Visual Studio Code

Install the [Free Pascal Debugger (cnoc.fpdebug)](https://marketplace.visualstudio.com/items?itemName=CNOC.fpdebug) extension from the Marketplace. It includes the fpdserver-executables already and works out-of-the-box.

### Lazarus

You have to download and install the LazFPDServerDebugger component from the [LazDABDebugger package](https://gitlab.com/fpcprojects/LazDABDebugger) and need the FPDServer binaries to serve as DAB adapter. You can find more information on the LazDABDebugger page.

### Basic usage

If you want you can copy the executable somewhere in your path, so it can be started without providing the full path to it's executable.

The debug-server can be started with:

    fpdserver

Now it is ready to receive incoming listeners on it's default port (9159). It is also possible to provide debug-commands on the console.

### Other DAB-clients

To be able to use the FPDServer DAB-adapter with other DAB-clients, the arguments of a DAB Launch-request can provide the following arguments:

 - program: the name of the executable that has to be debugged (the debuggee)
 - parameters: array of strings with parameters for the debuggee
 - workingdirectory: this will be made the current directory before the debuggee is started
 - environment: environment-variables that are set before running the debuggee
 - forcenewconsolewin: is a boolean, when set it will enforce a console-window for the debuggee. Windows only.
 - OrderVariablesBy: is an array, used to configure the way variables are ordered. Possible strategies are `Defined`, `Visibility`, `InheritenceDepth` and `Alphabetic`.

Here is a full example of a DAB-launch request that works with FPDServer:

    {
        "command": "launch",
        "arguments": {
            "program": "/home/joost/fpdserver/tests/testapplications/testparams",
            "parameters": [
                "-e"
            ],
            "environment": [
                {
                    "name": "env1",
                    "value": "val1"
                },
                {
                    "name": "env2",
                    "value": "val2"
                }
            ],
            "workingdirectory": "/home/joost/fpdserver/tests/testapplications",
            "forcenewconsolewin": true,
            "orderVariablesBy": [
                {
                    "strategy": "InheritenceDepth",
                    "order": "Descending"
                },
                {
                    "strategy": "Alphabetic",
                    "order": "Ascending"

                }
            ]
        },
        "type": "request",
        "seq": <x>
    }

## How to compile

### Prerequisites

To compile FPDServer you need [Free Pascal](https://www.freepascal.org/) version 3.2 or above, [Lazarus](https://www.lazarus-ide.org/) and the [fppkg](https://wiki.lazarus.freepascal.org/fppkg) package-manager.

### Compilation

Start by installing the dependencies [cerialization](https://gitlab.com/fpcprojects/cerialization), [fpcdab](https://gitlab.com/fpcprojects/fpcdab) and dcserver from the central fppkg-repository. This can be done on a command-line with these three commands:

    fppkg install cerialization
    fppkg install dcserver
    fppkg install fpcdab

Now open the fpdserver.lpi project in Lazarus and compile it (ctrl-F9).

## Contributing

Questions, ideas and patches are welcome. You can send them directly to <joost@cnoc.nl>, or discuss them at the fpc-pascal mailinglist.

At this moment the basic functionality works, except for multiple threads. And the display of variables is limited and needs more work.

## Versioning

This package is very new and receives a lot of updates. For the versions available, see the tags on this repository.

## Authors

* **Joost van der Sluis** - *initial work* - <joost@cnoc.nl>

## License

This library is distributed under the GNU General Public License version 2 (see the [COPYING.GPL.txt](COPYING.GPL.txt) file) with the following modification:

- object files and libraries linked into an application may be distributed without source code.