## [0.6.0] - 2022-7-15
### Changed
 - Compiles with Lazarus 2.3.0
 - Improved error-messages on communication/protocol problems
 - The 'virtual class-type' of variables that contain a class is shown now, not the 'defined class-type'
 - Based on the DwarfExt branch of [fpDebug](https://gitlab.com/jvdsluis/lazarus-playground/-/tree/dwarfext/components/fpdebug) with a lot of improvements since the prior version. Like support for CFI and the DWARF-property extension.
### Added
 - Ability to configure how to sort variables
 - Show dynamic codepage and rawdata of strings
 - Support for the DWARF-property extension which is used by Freepascal v3.3.1 and up (-godwarfproperties)
### Fixed
 - Fixed errors within the communication-layers. (Unexpected errors/crashes)
 - Fixed stacktraces for threads other then the main thread
 - Fixed problem on showing local variables for locations without debug-info
 - Fixed the line-information in stacktraces on locations without debug-info

## [0.5.0] - 2022-1-4
### Changed
 - Moved hosting to gitlab.com/fpcprojects/fpdebug
 - Improved feedback on setting breakpoints
 - Improved feedback when the start of the debuggee failed
 - Improved feedback in case of other problems/errors/exceptions
 - The DAB-specific sources are moved to a new package, [fpcdab](https://gitlab.com/fpcprojects/fpcdab)
 - The console-output is now send to the client as 'stdout' and log messages are passed to DAB-clients as 'console'-output.
 - Based on the latest version of [fpDebug](https://gitlab.com/freepascal.org/lazarus/lazarus/-/tree/218ea083c3410134be26dc47ecbf9d8772bab884/components/fpdebug) with a lot of improvements since the prior version.
### Added
 - Ability to set breakpoints while the debugee is running
 - CI/CD scripts to provide pre-compiled binaries for Windows and Linux
 - Provide the visilibity of class-members
 - ForceNewConsoleWin option (Windows only)
 - Ability to debug libraries
### Fixed
 - Removed some memory-leaks
 - Fixed a crash on invalid input