unit DebugInOutputProcessor;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  fpjson,
  FpDbgUtil,
  DebugThreadCommand,
  DbgIntfDebuggerBase,
  dcsHandler,
  dcsInOutputProcessor,
  LazDebuggerIntf,
  debugthread,
  FpDbgClasses,
  typinfo,
  variants,
  jsonparser;

type

  { TJSonInOutputProcessor }

  TJSonInOutputProcessor = class(TDCSCustomInOutputProcessor)
  public
    function TextToCommand(const ACommandText: string): TFpDebugThreadCommand; override;
    function EventToText(AnEvent: TDCSEvent): string; override;
    class function InteractiveInitializationMessage(APort: integer): string;
  end;

implementation

{ TJSonInOutputProcessor }

function TJSonInOutputProcessor.TextToCommand(const ACommandText: string): TFpDebugThreadCommand;
var
  AJSonCommand: TJSONData;
  AJSonProp: TJSONData;
  AJSonUID: TJSONData;
  AnUID: variant;
  ACommandClass: TFpDebugThreadCommandClass;
  s: string;
  i: integer;
  APropCount: integer;
  APropList: PPropList;
  APropName: string;
begin
  result := nil;
  try
    AJSonCommand := GetJSON(ACommandText);
  except
    on E: Exception do
      begin
      FDistributor.SendNotification(FLisId, ntInvalidCommand, NULL, 'Command "%s" is not a valid JSON string: %s', ACommandText, [ACommandText, e.Message]);
      Exit;
      end;
  end;
  if not assigned(AJSonCommand) then
    begin
    FDistributor.SendNotification(FLisId, ntInvalidCommand, NULL, 'Command "%s" is not a valid JSON string.', ACommandText, [ACommandText]);
    exit;
    end;

  try
    if AJSonCommand.JSONType<>jtObject then
      begin
      FDistributor.SendNotification(FLisId, ntInvalidCommand, NULL, 'Command "%s" is not a JSON-object.', ACommandText, [ACommandText]);
      exit;
      end;
    s := TJSONObject(AJSonCommand).Get('command', '');
    if s = '' then
      begin
      FDistributor.SendNotification(FLisId, ntInvalidCommand, NULL, 'Command "%s" does not contain a "command" entry.', ACommandText,[ACommandText]);
      exit;
      end;
    ACommandClass := TFpDebugThreadCommandList.instance.GetCommandByName(s);
    if not assigned(ACommandClass) then
      begin
      FDistributor.SendNotification(FLisId, ntInvalidCommand, NULL, 'Command "%s" does not exist.', s, [S]);
      exit;
      end;

    AJSonUID := TJSONObject(AJSonCommand).find('uid');
    if assigned(AJSonUID) then
      AnUID := AJSonUID.Value
    else
      AnUID := null;

    result := ACommandClass.Create(FLisId, AnUID, FDistributor);
    APropCount := GetPropList(result, APropList);
    try
      for i := 0 to APropCount-1 do
        begin
        APropName := APropList^[i]^.Name;
        AJSonProp := TJSONObject(AJSonCommand).Find(LowerCase(APropName));

        if assigned(AJSonProp) then
          begin
          case APropList^[i]^.PropType^.Kind of
            tkAString, tkString, tkUString:
              SetStrProp(result, APropList^[i], AJSonProp.AsString);
            tkInteger:
              SetOrdProp(result, APropList^[i], AJSonProp.AsInteger);
          end;
          end;
        end;
    finally
      Freemem(APropList);
    end;
  finally
    AJSonCommand.Free;
  end;
end;

function TJSonInOutputProcessor.EventToText(AnEvent: TDCSEvent): string;
var
  JSonEvent: TJSONObject;
  JSonLocationRec: TJSONObject;
  JSonArray: TJSONArray;
  JSonArrayEntry: TJSONObject;
  i: Integer;
  Event: TFpDebugEvent;
  NotificationEvent: TFpDebugNotificationEvent;
begin
  JSonEvent := TJSONObject.Create;
  try
    JSonEvent.Add('type',DCSEventTypeNames[AnEvent.EventType]);

    case AnEvent.EventType of
      dcsetEvent:
        begin
        Event := AnEvent as TFpDebugEvent;
        JSonEvent.Add('eventName',Event.EventName);
        if Event.InstructionPointerRegValue<>0 then
          JSonEvent.Add('instrPointer', FormatAddress(Event.InstructionPointerRegValue));
        end;
      dcsetLog:
        begin
        case (AnEvent as TDCSLogEvent).LogLevel of
          etDebug: JSonEvent.Add('logType','debug');
          etError: JSonEvent.Add('logType','error');
          etInfo: JSonEvent.Add('logType','info');
        end;
        end;
      dcsetNotification:
        begin
        JSonEvent.Add('notificationType',DCSNotificationTypeNames[TDCSNotificationEvent(AnEvent).NotificationType]);
        if TDCSNotificationEvent(AnEvent).Command<>'' then
          JSonEvent.Add('command',TDCSNotificationEvent(AnEvent).Command);
        if TDCSNotificationEvent(AnEvent).Message<>'' then
          JSonEvent.Add('message',TDCSNotificationEvent(AnEvent).Message);
        end;
    end;

    if AnEvent is TFpDebugNotificationEvent then
      begin
      NotificationEvent := AnEvent as TFpDebugNotificationEvent;

      JSonEvent.Add('message',NotificationEvent.Message);

      if NotificationEvent.BreakpointServerIdr<>0 then
        JSonEvent.Add('BreakpointServerIdr', NotificationEvent.BreakpointServerIdr);
      if Event.SendByConnectionIdentifier>0 then
        JSonEvent.Add('connIdentifier', Event.SendByConnectionIdentifier);
      if NotificationEvent.Validity<>ddsUnknown then
        JSonEvent.Add('validity', DebuggerDataStateStr[NotificationEvent.Validity]);
      if NotificationEvent.LocationRec.Address <> 0 then
        begin
        JSonLocationRec := TJSONObject.Create;
        JSonLocationRec.Add('address', FormatAddress(NotificationEvent.LocationRec.Address));
        JSonLocationRec.Add('funcName', NotificationEvent.LocationRec.FuncName);
        JSonLocationRec.Add('srcFile', NotificationEvent.LocationRec.SrcFile);
        JSonLocationRec.Add('srcFullName', NotificationEvent.LocationRec.SrcFullName);
        JSonLocationRec.Add('srcLine', NotificationEvent.LocationRec.SrcLine);
        JSonEvent.Add('locationRec',JSonLocationRec);
        end;
      if not varisnull(Event.AnUID) then
        begin
        if VarIsOrdinal(Event.AnUID) then
          JSonEvent.Add('uid', integer(Event.AnUID))
        else
          JSonEvent.Add('uid', VarToStr(Event.AnUID));
        end;
      if length(NotificationEvent.StackEntryArray)>0 then
        begin
        JSonArray := TJSONArray.Create;
        for i := 0 to high(NotificationEvent.StackEntryArray) do
          begin
          JSonArrayEntry := TJSONObject.Create;
          JSonArrayEntry.Add('address', FormatAddress(NotificationEvent.StackEntryArray[i].AnAddress));
          JSonArrayEntry.Add('frameaddress', FormatAddress(NotificationEvent.StackEntryArray[i].FrameAdress));
          JSonArrayEntry.Add('sourcefile', NotificationEvent.StackEntryArray[i].SourceFile);
          JSonArrayEntry.Add('line', NotificationEvent.StackEntryArray[i].Line);
          JSonArrayEntry.Add('functionname', NotificationEvent.StackEntryArray[i].FunctionName);
          JSonArray.Add(JSonArrayEntry);
          end;
        JSonEvent.Add('callstack', JSonArray);
        end;
      if length(NotificationEvent.DisassemblerEntryArray)>0 then
        begin
        JSonArray := TJSONArray.Create;
        for i := 0 to high(NotificationEvent.DisassemblerEntryArray) do
          begin
          JSonArrayEntry := TJSONObject.Create;
          JSonArrayEntry.Add('address', FormatAddress(NotificationEvent.DisassemblerEntryArray[i].Addr));
          JSonArrayEntry.Add('dump', NotificationEvent.DisassemblerEntryArray[i].Dump);
          JSonArrayEntry.Add('statement', NotificationEvent.DisassemblerEntryArray[i].Statement);
          JSonArrayEntry.Add('srcfilename', NotificationEvent.DisassemblerEntryArray[i].SrcFileName);
          JSonArrayEntry.Add('srcfileline', NotificationEvent.DisassemblerEntryArray[i].SrcFileLine);
          JSonArrayEntry.Add('srcstatementindex', NotificationEvent.DisassemblerEntryArray[i].SrcStatementIndex);
          JSonArrayEntry.Add('srcstatementcount', NotificationEvent.DisassemblerEntryArray[i].SrcStatementCount);
          JSonArrayEntry.Add('functionname', NotificationEvent.DisassemblerEntryArray[i].FuncName);
          JSonArrayEntry.Add('offset', NotificationEvent.DisassemblerEntryArray[i].Offset);
          JSonArray.Add(JSonArrayEntry);
          end;
        JSonEvent.Add('disassembly', JSonArray);
        JSonEvent.Add('startaddress', FormatAddress(NotificationEvent.Addr1));
        JSonEvent.Add('endaddress', FormatAddress(NotificationEvent.Addr2));
        JSonEvent.Add('lastentryendaddress', FormatAddress(NotificationEvent.Addr3));
        end;
      if length(NotificationEvent.WatchEntryArray)>0 then
        begin
        JSonArray := TJSONArray.Create;
        for i := 0 to high(NotificationEvent.WatchEntryArray) do
          begin
          JSonArrayEntry := TJSONObject.Create;
          JSonArrayEntry.Add('name', NotificationEvent.WatchEntryArray[i].Expression);
          JSonArrayEntry.Add('value', NotificationEvent.WatchEntryArray[i].TextValue);
          if Event.EventName='registers' then
            begin
            JSonArrayEntry.Add('numvalue', NotificationEvent.WatchEntryArray[i].NumValue);
            JSonArrayEntry.Add('size', NotificationEvent.WatchEntryArray[i].Size);
            end;
          JSonArray.Add(JSonArrayEntry);
          end;
        JSonEvent.Add(NotificationEvent.EventName, JSonArray);
        end;
      end;
    result := JSonEvent.AsJSON;
  finally
    JSonEvent.Free;
  end;
end;

class function TJSonInOutputProcessor.InteractiveInitializationMessage(APort: integer): string;
var
  JSonMessage: TJSONObject;
begin
  JSonMessage := TJSONObject.Create;
  try
    JSonMessage.Add('welcome', 'FPDebug Server');
    JSonMessage.Add('copyright', 'Joost van der Sluis (2015-2020)');
    if APort>-1 then
      JSonMessage.Add('port', APort);
    result := JSonMessage.AsJSON;
  finally
    JSonMessage.Free;
  end;
end;

end.

